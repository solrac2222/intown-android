package instown.instown.com;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.text.format.Time;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Francisco on 31/5/15.
 */
public class TimeApp {
    static BDInstown BDApp;
    private final Context nContexto;

    public TimeApp(Context c){
        nContexto=c;
    }

    public static void reposo(Activity actividad) {
        BDApp = new BDInstown (actividad);
        try {
            BDApp.abrir();
            Time today = new Time(Time.getCurrentTimezone());
            today.setToNow();
            int dia = today.monthDay;
            int mes = today.month;
            int year = today.year;
            int seconds = today.second;
            int minutes = today.minute;
            int hour = today.hour;
            String hour_now = dia + "/" + mes + "/" + year + " " + hour + ":" + minutes + ":" + seconds;
            String fecha = BDApp.GetTime();

            if(fecha.toString().equals("")){
                BDApp.crear_time(hour_now);
            }else {
                BDApp.update_time(hour_now);
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                java.util.Date fechaActual = dateFormat.parse(hour_now);
                java.util.Date fechaApp = dateFormat.parse(fecha);


                // tomamos la instancia del tipo de calendario
                Calendar calendarInicio = Calendar.getInstance();
                Calendar calendarFinal = Calendar.getInstance();

                // Configramos la fecha del calendatio, tomando los valores del date que
                // generamos en el parse
                calendarInicio.setTime(fechaActual);
                calendarFinal.setTime(fechaApp);

                // obtenemos el valor de las fechas en milisegundos
                long milisegundos1 = calendarInicio.getTimeInMillis();
                long milisegundos2 = calendarFinal.getTimeInMillis();

                // tomamos la diferencia
                long diferenciaMilisegundos = milisegundos2 - milisegundos1;

                // calcular la diferencia en segundos
                long diffSegundos = Math.abs(diferenciaMilisegundos / 1000);
                System.out.println("segundos " + diffSegundos);
                if (diffSegundos > 3600) {
                    Constants.ActualizarCiudad(actividad, "0");
                }

            }

            BDApp.cerrar();
        } catch (Exception e) {

            e.printStackTrace();
        }


    }

}