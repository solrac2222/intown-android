package instown.instown.com;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;
import com.loopj.android.http.RequestParams;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import instown.instown.com.exception.NetworkException;
import instown.instown.com.exception.ParsingException;
import instown.instown.com.exception.ServerException;
import instown.instown.com.exception.TimeOutException;
import instown.instown.com.services.AppAsynchTask;
import io.fabric.sdk.android.Fabric;


public class SignFragment extends ActionBarActivity implements View.OnClickListener {
    LoginButton loginButton;
    CallbackManager callbackManager;
    AccessToken accesstoken;
    ImageView img_fb,img_tw;
    int vacio_agree=0,vacio_email=0;
    EditText ed_usuario,ed_pass,ed_city;
    ImageView woman,man,cir_vacio,cir_vacio2;
    String genero="",envio_mail="false",terminos="",email_valido;
    Button bt_sign;
    final BDInstown logeobd = new BDInstown (SignFragment.this);
    String non_field,id_usuario;
    String respuestaWS=null;
    ProgressDialog dialog;
    TwitterLoginButton loginButtonTw;
    RequestParams params = new RequestParams();
    String emailW,first_name,username,email,last_name,customer,notifications_enabled,state,email_frequency,gender,city;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        TwitterAuthConfig authConfig = new TwitterAuthConfig(Constants.consumerKey,Constants.consumerSecret);
        Fabric.with(this, new Twitter(authConfig));

        setContentView(R.layout.sign_fragment);

        if( Build.VERSION.SDK_INT >= 9){
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        init();
        ortogarClick();

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("siii");
                onBackPressed();
            }
        });


        accesstoken= AccessToken.getCurrentAccessToken();

        loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions("email", "user_likes", "user_friends");

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                String accessToken= loginResult.getAccessToken().getToken();
                System.out.println(accessToken);
                LoginServicioFB(accessToken);
            }

            @Override
            public void onCancel() {
            }

            @Override
            public void onError(FacebookException exception) {
            }
        });

        loginButtonTw = (TwitterLoginButton) findViewById(R.id.twitter_login_button);
        loginButtonTw.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                // Do something with result, which provides a TwitterSession for making API calls

                TwitterSession session = Twitter.getSessionManager().getActiveSession();
                TwitterAuthToken authToken = session.getAuthToken();
                String token = authToken.token;
                String secret = authToken.secret;
                System.out.println("oauth_token "+token);
                System.out.println("oauth_token_secret "+secret);
                LoginServicioTW(token,secret);

            }

            @Override
            public void failure(TwitterException exception) {
                // Do something on failure
            }
        });

    }

    @Override
    public void onResume() {

        super.onResume();
        TimeApp.reposo(SignFragment.this);

    }

    private void ortogarClick() {
        img_fb.setOnClickListener(this);
        img_tw.setOnClickListener(this);
        woman.setOnClickListener(this);
        man.setOnClickListener(this);
        cir_vacio.setOnClickListener(this);
        cir_vacio2.setOnClickListener(this);
        bt_sign.setOnClickListener(this);
    }

    private void init() {
        toolbar = (Toolbar)findViewById(R.id.my_awesome_toolbar);
        img_fb=(ImageView) findViewById(R.id.img_fb);
        img_tw=(ImageView) findViewById(R.id.img_tw);
        ed_usuario=(EditText)findViewById(R.id.ed_usuario);
        ed_pass=(EditText)findViewById(R.id.ed_pass);
        ed_city=(EditText)findViewById(R.id.ed_city);
        woman=(ImageView)findViewById(R.id.woman);
        man=(ImageView)findViewById(R.id.man);
        cir_vacio=(ImageView)findViewById(R.id.cir_vacio);
        cir_vacio2=(ImageView)findViewById(R.id.cir_vacio2);
        bt_sign=(Button)findViewById(R.id.bt_sign);
    }



    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.img_fb:
                loginButton.performClick();
                break;
            case R.id.bt_sign:
                registro();
                break;
            case R.id.cir_vacio:
                if(vacio_agree==0){
                    vacio_agree=1;
                    terminos="si";
                    cir_vacio.setImageResource(R.drawable.cir_lleno);
                }else{
                    vacio_agree=0;
                    terminos="";
                    cir_vacio.setImageResource(R.drawable.cir_vacio);
                }
            break;
            case R.id.cir_vacio2:
                if(vacio_email==0){
                    vacio_email=1;
                    envio_mail="true";
                    cir_vacio2.setImageResource(R.drawable.cir_lleno);
                }else{
                    vacio_email=0;
                    envio_mail="false";
                    cir_vacio2.setImageResource(R.drawable.cir_vacio);
                }
             break;
            case R.id.man:
                genero="0";
                man.setImageResource(R.drawable.maleselected);
                woman.setImageResource(R.drawable.woman);
            break;
            case R.id.woman:
                genero="1";
                woman.setImageResource(R.drawable.felameselected);
                man.setImageResource(R.drawable.man);
            break;

        }
    }

    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }


    private void registro() {
        String usuario=ed_usuario.getText()+"";
        int pasword=ed_pass.length();
        String city=ed_city.getText()+"";


        boolean isCity=true,isEmailEntered=true,isPasswordEntered=true,isGenero=true,isTerminos=true;



        if(city.equals("")){
            isCity=false;
        }

        if(usuario.equals("")){
            isEmailEntered=false;
        }else if (!isEmailValid(ed_usuario.getText()+"")){
            isEmailEntered=false;
        }

        if(pasword<5){
            isPasswordEntered=false;
        }

        if(genero.equals("")){
            isGenero=false;
        }

        if(envio_mail.equals("")){
            isGenero=false;
        }

        if(terminos.equals("")){
            isTerminos=false;
        }




        if (isCity && isEmailEntered && isPasswordEntered && isGenero && isTerminos) {

            //String email,String pass,String gender,String city,String send_emails
            EnviarRegisto(ed_usuario.getText()+"",ed_pass.getText()+"",genero,city,envio_mail);

        }else if (!isEmailEntered) {
            ed_usuario.setError("Email Invalido");
            ed_usuario.requestFocus();
        }else if (!isPasswordEntered) {
            ed_pass.setError("Contraseña muy corta");
            ed_pass.requestFocus();
        }else if (!isCity) {
            ed_city.setError("Digite su ciudad");
            ed_city.requestFocus();
        }else if (!isGenero) {
            new AlertDialog.Builder(this)
                    .setIcon(R.mipmap.ic_launcher)
                    .setTitle(R.string.app_name)
                    .setMessage("Seleccion su genero")
                    .setNegativeButton(android.R.string.ok, null)
                    .show();
        }else if (!isTerminos) {
            new AlertDialog.Builder(this)
                    .setIcon(R.mipmap.ic_launcher)
                    .setTitle(R.string.app_name)
                    .setMessage("Debes aceptar los terminos y condiciones")
                    .setNegativeButton(android.R.string.ok, null)
                    .show();
        }
    }


    //hilo para logeo


    public void  EnviarRegisto(String emailE, String passE, String genderE, String cityE, String send_emailsE){
        dialog = new ProgressDialog(SignFragment.this);
        dialog.setMessage("Please Wait...");
        dialog.show();
        params.put("email", emailE);
        params.put("password", passE);
        params.put("gender", genderE);
        params.put("city", cityE);
        params.put("send_emails", send_emailsE);

        AsyncHttpClient myClient = new AsyncHttpClient();
        PersistentCookieStore cookieStore = new PersistentCookieStore(SignFragment.this);
        myClient.setCookieStore(cookieStore);

        myClient.post(Constants.registration, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                String txtResponse = String.valueOf(response);
                Log.e("", txtResponse);


                 try {
                    JSONObject myObject = new JSONObject(txtResponse);
                    //JSONObject myObject = response;

                    if (!myObject.isNull("non_field_errors")){
                        non_field=myObject.getString("non_field_errors");
                    }

                    if (!myObject.isNull("email")){
                        non_field=myObject.getString("email");
                    }


                    if (!myObject.isNull("id")){
                        id_usuario=myObject.getString("id");
                        first_name=myObject.getString("first_name");
                        username=myObject.getString("username");
                        email=myObject.getString("email");
                        last_name=myObject.getString("last_name");
                        customer=myObject.getString("customer");

                        System.out.println(customer+"...");
                        JSONObject myObjectCustomer = new JSONObject(customer);
                        notifications_enabled=myObjectCustomer.getString("notifications_enabled");
                        state=myObjectCustomer.getString("state");
                        gender=myObjectCustomer.getString("gender");

                        email_frequency=myObjectCustomer.getString("email_frequency");
                        city=myObjectCustomer.getString("city");

                        try {
                            logeobd.abrir();
                            logeobd.borrar();
                            logeobd.crear_entrada("1",id_usuario,username,first_name,last_name,email,email_frequency,notifications_enabled,city,state,gender);
                            logeobd.cerrar();
                            finish();
                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                dialog.hide();

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);


                dialog.hide();
                System.out.println(id_usuario);




            }
        });
    }

    private static String convertStreamToString(InputStream is) {

        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }


    public void  LoginServicioFB(String tkR){
        dialog = new ProgressDialog(SignFragment.this);
        dialog.setMessage("Please Wait...");
        dialog.show();
        params.put("access_token", tkR);
        AsyncHttpClient myClient = new AsyncHttpClient();
        PersistentCookieStore cookieStore = new PersistentCookieStore(SignFragment.this);
        myClient.setCookieStore(cookieStore);

        myClient.post(Constants.loginFB, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                String txtResponse = String.valueOf(response);
                Log.e("", txtResponse);


                try {
                    JSONObject myObject = new JSONObject(txtResponse);
                    //JSONObject myObject = response;

                    if (!myObject.isNull("non_field_errors")){
                        non_field=myObject.getString("non_field_errors");
                    }

                    if (!myObject.isNull("email")){
                        non_field=myObject.getString("email");
                    }


                    if (!myObject.isNull("id")){
                        id_usuario=myObject.getString("id");
                        first_name=myObject.getString("first_name");
                        username=myObject.getString("username");
                        email=myObject.getString("email");
                        last_name=myObject.getString("last_name");
                        customer=myObject.getString("customer");

                        System.out.println(customer+"...");
                        JSONObject myObjectCustomer = new JSONObject(customer);
                        notifications_enabled=myObjectCustomer.getString("notifications_enabled");
                        state=myObjectCustomer.getString("state");
                        gender=myObjectCustomer.getString("gender");

                        email_frequency=myObjectCustomer.getString("email_frequency");
                        city=myObjectCustomer.getString("city");

                        try {
                            logeobd.abrir();
                            logeobd.borrar();
                            logeobd.crear_entrada("1",id_usuario,username,first_name,last_name,email,email_frequency,notifications_enabled,city,state,gender);
                            logeobd.cerrar();
                            finish();
                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                dialog.hide();

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);


                dialog.hide();
                System.out.println(id_usuario);




            }
        });
    }


    public void  LoginServicioTW(String token,String tokenSecret){
        dialog = new ProgressDialog(SignFragment.this);
        dialog.setMessage("Please Wait...");
        dialog.show();
        params.put("oauth_token", token);
        params.put("oauth_token_secret", tokenSecret);

        AsyncHttpClient myClient = new AsyncHttpClient();
        PersistentCookieStore cookieStore = new PersistentCookieStore(SignFragment.this);
        myClient.setCookieStore(cookieStore);

        myClient.post(Constants.loginTW, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                String txtResponse = String.valueOf(response);
                Log.e("", txtResponse);


                try {
                    JSONObject myObject = new JSONObject(txtResponse);
                    //JSONObject myObject = response;


                    if (!myObject.isNull("id")){
                        id_usuario=myObject.getString("id");
                        first_name=myObject.getString("first_name");
                        username=myObject.getString("username");
                        email=myObject.getString("email");
                        last_name=myObject.getString("last_name");
                        customer=myObject.getString("customer");

                        System.out.println(customer+"...");
                        JSONObject myObjectCustomer = new JSONObject(customer);
                        notifications_enabled=myObjectCustomer.getString("notifications_enabled");
                        state=myObjectCustomer.getString("state");
                        gender=myObjectCustomer.getString("gender");

                        email_frequency=myObjectCustomer.getString("email_frequency");
                        city=myObjectCustomer.getString("city");

                        try {
                            logeobd.abrir();
                            logeobd.borrar();
                            logeobd.crear_entrada("1",id_usuario,username,first_name,last_name,email,email_frequency,notifications_enabled,city,state,gender);
                            logeobd.cerrar();
                            finish();
                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                dialog.hide();

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);


                dialog.hide();
                System.out.println(id_usuario);




            }
        });
    }




}