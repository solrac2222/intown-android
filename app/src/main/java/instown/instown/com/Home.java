package instown.instown.com;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.internal.widget.AdapterViewCompat;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import instown.instown.com.adapter.AdapterMenuInternas;


public class Home extends ActionBarActivity implements View.OnClickListener,Animation.AnimationListener {
    private static int selectedFragmentIndex = 0;
    LinearLayout tab_explore,tab_biz,tab_page,tab_calendar,tab_more;
    ImageView subir,lupa;
    static ImageView bajar;
    Animation anim;
    RelativeLayout Rsubir,Rbajar;
    static ArrayList<HashMap<String, String>> ListOpciones = new ArrayList<HashMap<String, String>>();
    static ArrayList<HashMap<String, String>> ListOpcionesStuff = new ArrayList<HashMap<String, String>>();
    int id_usuario;
    static int listo=0;
    Dialog dialog_pop;
    static AdaptadorOpciones adapter_opciones;
    static ListView buscador_list;
    int bandera_opciones=0;
    int ocultar=0;
    ArrayList<HashMap<String, String>> OpcionesArray = new ArrayList<HashMap<String, String>>();
    BDInstown logeoBD;
    public static int abrir=0;
    ArrayList<HashMap<String, String>> MenuArray = new ArrayList<HashMap<String, String>>();
    static FragmentManager fragmentManager;
    //lista opciones
    static String TAG_ID= "id";
    static String TAG_NAME = "name";

    AlertDialog alert = null;
    //menu
    ListView mDrawerList,contenido_home;
    ActionBarDrawerToggle mDrawerToggle;
    AdapterMenuInternas adapter_menu;
    Toolbar toolbar;
    DrawerLayout mDrawerLayout;
    static Activity actividad;
    GPSTracker gps;
    static ImageView ico_explore;
    static ImageView ico_biz;
    static ImageView ico_page;
    static ImageView ico_calendar;
    static ImageView ico_more;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);


        if( Build.VERSION.SDK_INT >= 9){
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        actividad=Home.this;
        logeoBD = new BDInstown (Home.this);
        mDrawerLayout = (DrawerLayout)findViewById(R.id.my_drawer_layout);
        toolbar = (Toolbar)findViewById(R.id.my_awesome_toolbar);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        gps = new GPSTracker(Home.this);

        if (gps.canGetLocation()) {
        }else{
            AlertNoGps();
        }


        mDrawerToggle = new ActionBarDrawerToggle(Home.this,
                mDrawerLayout,
                toolbar,
                R.string.drawer_open,
                R.string.drawer_close){
            public void onDrawerClosed(View v){
                super.onDrawerClosed(v);
                invalidateOptionsMenu();
                syncState();
            }
            public void onDrawerOpened(View v){
                super.onDrawerOpened(v);
                invalidateOptionsMenu();
                syncState();
            }
        };

       // getSupportActionBar().hide();
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
        dataMenu();
        adapter_menu=new AdapterMenuInternas(Home.this,MenuArray);
        mDrawerList.setAdapter(adapter_menu);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //getWindow().setNavigationBarColor(getResources().getColor(Color.BLACK));
            getWindow().setNavigationBarColor(Color.parseColor("#000000"));
        }

        mDrawerLayout.setDrawerListener(mDrawerToggle);


        mDrawerToggle.syncState();

        fragmentManager = getSupportFragmentManager();


        tab_explore=(LinearLayout)findViewById(R.id.tab_explore);
        tab_biz=(LinearLayout)findViewById(R.id.tab_biz);
        tab_page=(LinearLayout)findViewById(R.id.tab_page);
        tab_calendar=(LinearLayout)findViewById(R.id.tab_calendar);
        tab_more=(LinearLayout)findViewById(R.id.tab_more);
        subir=(ImageView)findViewById(R.id.subir);
        lupa=(ImageView)findViewById(R.id.lupa);
        bajar=(ImageView)findViewById(R.id.bajar);
        Rsubir=(RelativeLayout)findViewById(R.id.Rsubir);
        Rbajar=(RelativeLayout)findViewById(R.id.Rbajar);
        buscador_list=(ListView)findViewById(R.id.buscador_list);


        ico_explore=(ImageView)findViewById(R.id.ico_explore);
        ico_biz=(ImageView)findViewById(R.id.ico_biz);
        ico_page=(ImageView)findViewById(R.id.ico_page);
        ico_calendar=(ImageView)findViewById(R.id.ico_calendar);
        ico_more=(ImageView)findViewById(R.id.ico_more);


        ico_explore.setImageResource(R.drawable.explore_on);

        tab_explore.setOnClickListener(this);
        tab_biz.setOnClickListener(this);
        tab_page.setOnClickListener(this);
        tab_calendar.setOnClickListener(this);
        tab_more.setOnClickListener(this);
        subir.setOnClickListener(this);
        bajar.setOnClickListener(this);
        lupa.setOnClickListener(this);

        selectItem(0);
        new LlenarOpciones().execute();

    }

    private void AlertNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Es necesario activar su GPS, ¿Desea activarlo?")
                .setCancelable(false)
                .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        alert = builder.create();
        if(!alert.isShowing()){
            alert.show();
        }

    }

    private void dataMenu() {

        MenuArray.clear();
        HashMap<String, String> mapLogout = new HashMap<String, String>();
        mapLogout.put("id", "1");
        mapLogout.put("name","LOG IN/LOGOUT");
        MenuArray.add(mapLogout);

        HashMap<String, String> mapAcc = new HashMap<String, String>();
        mapAcc.put("id", "2");
        mapAcc.put("name","MY ACCOUNT");
        MenuArray.add(mapAcc);

        HashMap<String, String> mapInto = new HashMap<String, String>();
        mapInto.put("id", "3");
        mapInto.put("name","INTOWNER / VIDEO");
        MenuArray.add(mapInto);

        HashMap<String, String> mapFa = new HashMap<String, String>();
        mapFa.put("id", "4");
        mapFa.put("name","FAQS");
        MenuArray.add(mapFa);

        HashMap<String, String> mapTer= new HashMap<String, String>();
        mapTer.put("id", "5");
        mapTer.put("name","TERMS OF USE");
        MenuArray.add(mapTer);

        HashMap<String, String> mapPri = new HashMap<String, String>();
        mapPri.put("id", "6");
        mapPri.put("name","PRIVACY POLICY");
        MenuArray.add(mapPri);

        HashMap<String, String> mapCont = new HashMap<String, String>();
        mapCont.put("id", "7");
        mapCont.put("name","CONTACT US");
        MenuArray.add(mapCont);


        HashMap<String, String> mapAdver = new HashMap<String, String>();
        mapAdver.put("id", "8");
        mapAdver.put("name","ADVERTISERS");
        MenuArray.add(mapAdver);

    }

    /* The click listner for ListView in the navigation drawer */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            //selectItem(position);
            if(position==0){
                try {
                    logeoBD.abrir();
                    id_usuario=logeoBD.obtener_datos();

                    if(id_usuario>0){
                        new AlertDialog.Builder(Home.this)
                                .setTitle("InTown")
                                .setMessage("¿Desea cerrar sesión?")
                                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which){


                                    }
                                })
                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which){
                                        try {
                                            logeoBD.abrir();
                                            logeoBD.borrar();
                                            finishAffinity();
                                            logeoBD.cerrar();
                                        } catch (Exception e) {
                                            // TODO Auto-generated catch block
                                            e.printStackTrace();
                                        }
                                    }
                                })
                                .show();
                    }else{
                        Intent ventana_login= new Intent(Home.this, LoginPop.class);
                        startActivity(ventana_login);
                    }
                    logeoBD.cerrar();

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


            }else if(position==1){
                try {
                    logeoBD.abrir();
                    id_usuario=logeoBD.obtener_datos();

                    if(id_usuario>0){
                        Intent ventana_login= new Intent(Home.this, MyAccount.class);
                        startActivity(ventana_login);
                    }else{
                        Intent menu = new Intent(Home.this, LoginPop.class);
                        startActivity(menu);
                    }
                    logeoBD.cerrar();

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }else if(position==2){
                Intent ventana_video = new Intent(Home.this, VideoInto.class);
                startActivity(ventana_video);
            }else if(position==3){
                Intent ventana_web = new Intent(Home.this, WebsiteMerchant.class);
                ventana_web.putExtra("pagina", Constants.why_be);
                startActivity(ventana_web);
            }else if(position==4){
                Intent ventana_terms = new Intent(Home.this, WebsiteMerchant.class);
                ventana_terms.putExtra("pagina", Constants.terms);
                startActivity(ventana_terms);
            }else if(position==5){
                Intent ventana_privacy= new Intent(Home.this, WebsiteMerchant.class);
                ventana_privacy.putExtra("pagina", Constants.privacy);
                startActivity(ventana_privacy);
            }else if(position==6){
                String[] to = { "team@intown.com"};
                String[] cc = { "" };
                enviar(to, cc, "Contáctenos", "");
            }else if(position==7){
                Intent ventana_privacy= new Intent(Home.this, Advertisers.class);
                startActivity(ventana_privacy);
            }
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tab_explore:
                lupa.setVisibility(View.VISIBLE);
                selectItem(0);
                break;
            case R.id.tab_biz:
                lupa.setVisibility(View.VISIBLE);
                selectItem(2);
                break;
            case R.id.tab_page:
                try {
                    logeoBD.abrir();
                    id_usuario=logeoBD.obtener_datos();

                    if(id_usuario>0){
                        lupa.setVisibility(View.VISIBLE);
                        selectItem(7);
                    }else{
                        Intent menu = new Intent(Home.this, LoginPop.class);
                        startActivity(menu);
                    }
                    logeoBD.cerrar();

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                break;
            case R.id.tab_calendar:
                lupa.setVisibility(View.INVISIBLE);
                selectItem(4);
                break;
            case R.id.tab_more:
                try {
                    logeoBD.abrir();
                    id_usuario=logeoBD.obtener_datos();

                    if(id_usuario>0){
                        lupa.setVisibility(View.INVISIBLE);
                        selectItem(6);
                    }else{
                        Intent menu = new Intent(Home.this, LoginPop.class);
                        startActivity(menu);
                    }
                    logeoBD.cerrar();

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                break;
            case R.id.bajar:
                ocultar=1;
                anim = AnimationUtils.loadAnimation(Home.this, R.anim.push_bottom_out);
                anim.setAnimationListener(Home.this);
                anim.setFillAfter(true);
                Rbajar.startAnimation(anim);
                Rsubir.setVisibility(View.VISIBLE);
                break;
            case R.id.subir:
                ocultar=0;
                anim = AnimationUtils.loadAnimation(Home.this, R.anim.push_top_in);
                anim.setAnimationListener(Home.this);
                anim.setFillAfter(true);
                Rbajar.startAnimation(anim);
                Rbajar.setVisibility(View.VISIBLE);
                bajar.setVisibility(View.VISIBLE);
                buscador_list.setVisibility(View.VISIBLE);
                Rsubir.setVisibility(View.INVISIBLE);
               // Rsubir.setVisibility(View.GONE);
                break;
            case R.id.lupa:
                pop_search();
               break;
            /*case R.id.tx_my_account:

                try {
                    logeoBD.abrir();
                    int id_usuario=logeoBD.obtener_datos();
                    System.out.println(id_usuario+" id_usuario");
                    if (id_usuario>0){
                        selectItem(7);
                    }else{
                        Intent menu = new Intent(getApplicationContext(), LoginFragment.class);
                        startActivity(menu);
                    }

                    logeoBD.cerrar();

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }



                break;*/

        }
    }


    public static void selectItem(int position) {
        // Create a new fragment and specify the planet to show based on position
        Fragment fragment=null;
        selectedFragmentIndex=position;
        switch (position) {
            case 0:
                if(listo==1){
                    adapter_opciones=new AdaptadorOpciones(actividad,ListOpciones);
                    buscador_list.setAdapter(adapter_opciones);
                }

                HomeFragment.q="";
                fragment = new HomeFragment();
                seleccionarTabs(0);
            break;
            case 2:
                adapter_opciones=new AdaptadorOpciones(actividad,ListOpcionesStuff);
                buscador_list.setAdapter(adapter_opciones);
                bajar.performClick();
                BizFragment.categorie="0";

                fragment = new BizFragment();
                seleccionarTabs(1);
                break;
            case 4:

                adapter_opciones=new AdaptadorOpciones(actividad,ListOpciones);
                buscador_list.setAdapter(adapter_opciones);
                fragment = new CalendarFragment();
                seleccionarTabs(3);
                break;
            case 6:

                adapter_opciones=new AdaptadorOpciones(actividad,ListOpcionesStuff);
                buscador_list.setAdapter(adapter_opciones);
                bajar.performClick();
                MoreFragment.categorie="0";
                fragment = new MoreFragment();
                seleccionarTabs(4);
                break;
            case 7:

                adapter_opciones=new AdaptadorOpciones(actividad,ListOpciones);
                buscador_list.setAdapter(adapter_opciones);
                bajar.performClick();
                PageFragment.categorie="0";
                fragment = new PageFragment();
                seleccionarTabs(2);
                break;
            case 8:
                adapter_opciones=new AdaptadorOpciones(actividad,ListOpciones);
                buscador_list.setAdapter(adapter_opciones);
                fragment = new MyPageFragment();
                break;
        }

        fragmentManager.beginTransaction()
                .replace(R.id.content_frame, fragment)
                .commit();



       /* FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.content_frame, fragment)
                .commit();
        getFragmentManager().executePendingTransactions();*/


    }

    private static void seleccionarTabs(int posi) {
        switch (posi){
            case 0:
                ico_explore.setImageResource(R.drawable.explore_on);
                ico_biz.setImageResource(R.drawable.biz);
                ico_page.setImageResource(R.drawable.page);
                ico_calendar.setImageResource(R.drawable.calendar);
                ico_more.setImageResource(R.drawable.more);
                break;
            case 1:
                ico_explore.setImageResource(R.drawable.explore);
                ico_biz.setImageResource(R.drawable.biz_on);
                ico_page.setImageResource(R.drawable.page);
                ico_calendar.setImageResource(R.drawable.calendar);
                ico_more.setImageResource(R.drawable.more);
                break;
            case 2:
                ico_explore.setImageResource(R.drawable.explore);
                ico_biz.setImageResource(R.drawable.biz);
                ico_page.setImageResource(R.drawable.page_on);
                ico_calendar.setImageResource(R.drawable.calendar);
                ico_more.setImageResource(R.drawable.more);
                break;
            case 3:
                ico_explore.setImageResource(R.drawable.explore);
                ico_biz.setImageResource(R.drawable.biz);
                ico_page.setImageResource(R.drawable.page);
                ico_calendar.setImageResource(R.drawable.calendar_on);
                ico_more.setImageResource(R.drawable.more);
                break;
            case 4:
                ico_explore.setImageResource(R.drawable.explore);
                ico_biz.setImageResource(R.drawable.biz);
                ico_page.setImageResource(R.drawable.page);
                ico_calendar.setImageResource(R.drawable.calendar);
                ico_more.setImageResource(R.drawable.more_on);
                break;
        }

    }


    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {
        if(ocultar==1){
            bajar.setVisibility(View.GONE);
            buscador_list.setVisibility(View.GONE);
            Rbajar.setVisibility(View.GONE);
        }
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }


    private class LlenarOpciones extends AsyncTask<Void, Integer, Boolean> {
        private ProgressDialog dialog;
        @Override
        protected Boolean doInBackground(Void... params) {

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(Constants.categories);
            try {
                logeoBD.abrir();
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);


                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);

                HttpEntity entity = response.getEntity();
                InputStream instream = entity.getContent();
                String result = Constants.convertStreamToString(instream);
                System.out.println("resultados "+result);

                JSONArray resultado= null;
                JSONObject myObject = new JSONObject(result);

                HashMap<String, String> mapAll = new HashMap<String, String>();
                mapAll.put(TAG_ID, "0");
                mapAll.put(TAG_NAME,"VIEW ALL");
                ListOpcionesStuff.add(mapAll);


                resultado = myObject.getJSONArray("results");

                for(int i = 0; i < resultado.length(); i++){
                    JSONObject c = resultado.getJSONObject(i);
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put(TAG_ID, c.getString("id"));
                    map.put(TAG_NAME,c.getString("name"));
                    ListOpciones.add(map);
                    ListOpcionesStuff.add(map);

                }

                HashMap<String, String> mapRelleno = new HashMap<String, String>();
                mapRelleno.put(TAG_ID, "0");
                mapRelleno.put(TAG_NAME, " ");
                ListOpcionesStuff.add(mapRelleno);
                ListOpciones.add(mapRelleno);



                logeoBD.cerrar();
            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
            } catch (IOException e) {
                // TODO Auto-generated catch block
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }


            return true;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            int progreso = values[0].intValue();
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }



        @Override
        protected void onPostExecute(Boolean result) {
            listo=1;
            adapter_opciones=new AdaptadorOpciones(Home.this,ListOpciones);
            buscador_list.setAdapter(adapter_opciones);

            buscador_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Context context = view.getContext();
                    bajar.performClick();
                    HashMap<String, String> song = new HashMap<String, String>();
                    song = ListOpciones.get(position);
                    if(selectedFragmentIndex==6){

                        HashMap<String, String> songS = new HashMap<String, String>();
                        songS = ListOpcionesStuff.get(position);

                        if(songS.get(TAG_ID).toString().equals("0")){
                            MoreFragment.categorie="0";
                        }

                        MoreFragment.categorie=song.get(TAG_ID);
                        MoreFragment.MiTareaStuff();
                    }else if(selectedFragmentIndex==2){
                        HashMap<String, String> songS = new HashMap<String, String>();
                        songS = ListOpcionesStuff.get(position);
                        BizFragment.categorie=songS.get(TAG_ID);
                        BizFragment.MiTareaBiz();
                        if(songS.get(TAG_ID).toString().equals("0")){
                            BizFragment.foodT.setText("ALL - BUSINESSES");
                        }else{
                            BizFragment.foodT.setText(songS.get(TAG_NAME).toUpperCase()+" - BUSINESSES");
                        }


                    }else if(selectedFragmentIndex==7){
                        PageFragment.categorie=song.get(TAG_ID);
                        PageFragment.MiTareaPage();
                    }else{
                        Intent VentanaCategorias = new Intent(Home.this, ExploreFragment.class);
                        VentanaCategorias.putExtra("subcategoria", song.get(TAG_ID));
                        VentanaCategorias.putExtra("ciudad", HomeFragment.city_select);
                        VentanaCategorias.putExtra("nombre", song.get(TAG_NAME));
                        startActivity(VentanaCategorias);
                    }

                }
            });

        }

    }

    private void enviar(String[] to, String[] cc,String asunto, String mensaje) {
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setData(Uri.parse("mailto:"));
        //String[] to = direccionesEmail;
        //String[] cc = copias;
        emailIntent.putExtra(Intent.EXTRA_EMAIL, to);
        emailIntent.putExtra(Intent.EXTRA_CC, cc);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, asunto);
        emailIntent.putExtra(Intent.EXTRA_TEXT, mensaje);
        emailIntent.setType("message/rfc822");
        startActivity(Intent.createChooser(emailIntent, "Email "));
    }

    private void pop_search() {
        dialog_pop = new Dialog(Home.this);
        dialog_pop.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_pop.setContentView(R.layout.buscador);

        final EditText ed_buscador=(EditText)dialog_pop.findViewById(R.id.ed_buscador);
        Button bt_search=(Button) dialog_pop.findViewById(R.id.bt_search);
        Button bt_close=(Button) dialog_pop.findViewById(R.id.bt_close);

        bt_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_pop.hide();
            }
        });


        bt_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* if(ed_buscador.getText().toString().equals("")){
                    ed_buscador.setError("No puede estar vacio");
                    ed_buscador.requestFocus();
                    ed_buscador.setSelected(true);
                }else{*/
                    if(selectedFragmentIndex==0){
                        HomeFragment.q=ed_buscador.getText().toString();
                        new HomeFragment.HomeData().execute();
                    }else if(selectedFragmentIndex==2){
                        BizFragment.q=ed_buscador.getText().toString();
                        BizFragment.MiTareaBiz();
                    }else if(selectedFragmentIndex==7){
                        PageFragment.q=ed_buscador.getText().toString();
                        PageFragment.MiTareaPage();
                    }
                    dialog_pop.hide();
               // }

            }
        });

        dialog_pop.show();
    }
}
