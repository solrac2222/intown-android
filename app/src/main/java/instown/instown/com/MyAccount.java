package instown.instown.com;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


public class MyAccount extends ActionBarActivity {
    BDInstown logeoBD;
    int id_usuario;
    ListView buscador_list;
    AdaptadorOpciones adapter_opciones;
    ArrayList<HashMap<String, String>> ListOpciones = new ArrayList<HashMap<String, String>>();
    static String TAG_ID= "id";
    static String TAG_NAME = "name";
    Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_account);

        if( Build.VERSION.SDK_INT >= 9){
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        toolbar = (Toolbar)findViewById(R.id.my_awesome_toolbar);


        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });



        buscador_list=(ListView)findViewById(R.id.buscador_list);

        HashMap<String, String> map = new HashMap<String, String>();
        map.put(TAG_ID, "1");
        map.put(TAG_NAME, "CONTACT INFO");
        ListOpciones.add(map);

        HashMap<String, String> mapC = new HashMap<String, String>();
        mapC.put(TAG_ID, "2");
        mapC.put(TAG_NAME,"CHANGE PASSWORD");
        ListOpciones.add(mapC);

        HashMap<String, String> mapE = new HashMap<String, String>();
        mapE.put(TAG_ID, "3");
        mapE.put(TAG_NAME,"EMAIL UPDATES");
        ListOpciones.add(mapE);

        HashMap<String, String> mapN = new HashMap<String, String>();
        mapN.put(TAG_ID, "4");
        mapN.put(TAG_NAME,"NOTIFICATIONS");
        ListOpciones.add(mapN);

        adapter_opciones=new AdaptadorOpciones(MyAccount.this,ListOpciones);
        buscador_list.setAdapter(adapter_opciones);

        buscador_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Context context = view.getContext();

                if(position==0){
                    Intent ventana_login= new Intent(MyAccount.this, UpdateInfo.class);
                    startActivity(ventana_login);
                }else if(position==1){
                    Intent ventana_login= new Intent(MyAccount.this, ChangePassword.class);
                    startActivity(ventana_login);
                }else if(position==2){
                    Intent ventana_login= new Intent(MyAccount.this, UpdateEmails.class);
                    startActivity(ventana_login);
                }else if(position==3){
                    Intent ventana_login= new Intent(MyAccount.this, UpdateNotifications.class);
                    startActivity(ventana_login);
                }

                /*Intent VentanaCategorias = new Intent(Home.this, ExploreFragment.class);
                VentanaCategorias.putExtra("subcategoria", song.get(TAG_ID));
                VentanaCategorias.putExtra("nombre", song.get(TAG_NAME));
                startActivity(VentanaCategorias);*/

                }


        });


    }



    @Override
    public void onResume() {

        super.onResume();
        TimeApp.reposo(MyAccount.this);

    }

}