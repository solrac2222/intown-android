package instown.instown.com;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.VideoView;

/**
 * Created by Francisco on 30/6/15.
 */
public class Prueba extends Activity {

    VideoView vv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.video);

        vv = (VideoView)this.findViewById(R.id.videoView);

        String uri = "android.resource://" + getPackageName() + "/" + R.raw.merchantvideo;
        vv.setVideoURI(Uri.parse(uri));

        vv.start();

    }



}
