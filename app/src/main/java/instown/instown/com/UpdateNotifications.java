package instown.instown.com;

import android.app.ProgressDialog;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;


public class UpdateNotifications extends ActionBarActivity implements View.OnClickListener{
    ImageView cir_vacio,cir_vacio_two;
    Button bt_save;
    Toolbar toolbar;
    String notifications_enabled;
    ProgressDialog dialog;
    RequestParams params = new RequestParams();
    BDInstown logeoBD;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.update_notifications);

        if( Build.VERSION.SDK_INT >= 9){
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        toolbar = (Toolbar)findViewById(R.id.my_awesome_toolbar);


        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        logeoBD = new BDInstown (UpdateNotifications.this);
        try {
            logeoBD.abrir();
            Cursor Cusuarios=logeoBD.getUser();
            for(Cusuarios.moveToFirst();!Cusuarios.isAfterLast();Cusuarios.moveToNext()){
                notifications_enabled=Cusuarios.getString(Cusuarios.getColumnIndex(BDInstown.NOTIFICATIONS));
            }
            logeoBD.cerrar();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        init();
        events();


    }

    private void init() {
        cir_vacio=(ImageView)findViewById(R.id.cir_vacio);
        cir_vacio_two=(ImageView)findViewById(R.id.cir_vacio_two);
        bt_save=(Button)findViewById(R.id.bt_save);

    }

    private void events() {
        cir_vacio.setOnClickListener(this);
        cir_vacio_two.setOnClickListener(this);
        bt_save.setOnClickListener(this);


        if(notifications_enabled.toString().equals("true")){
            cir_vacio.setImageResource(R.drawable.cir_lleno);
        }else if(notifications_enabled.toString().equals("false")){
            cir_vacio_two.setImageResource(R.drawable.cir_lleno);
        }
    }


    @Override
    public void onResume() {

        super.onResume();
        TimeApp.reposo(UpdateNotifications.this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.cir_vacio:
                cir_vacio.setImageResource(R.drawable.cir_lleno);
                cir_vacio_two.setImageResource(R.drawable.cir_vacio);
                notifications_enabled="true";
                break;
            case R.id.cir_vacio_two:
                cir_vacio.setImageResource(R.drawable.cir_vacio);
                cir_vacio_two.setImageResource(R.drawable.cir_lleno);
                notifications_enabled="false";
                break;
            case R.id.bt_save:
                UpdateInfoUser();
                break;
        }
    }

    public void  UpdateInfoUser(){
        dialog = new ProgressDialog(UpdateNotifications.this);
        dialog.setMessage("Please Wait...");
        dialog.show();
        params.put("notifications_enabled",notifications_enabled);


        AsyncHttpClient myClient = new AsyncHttpClient();
        PersistentCookieStore cookieStore = new PersistentCookieStore(UpdateNotifications.this);
        myClient.setCookieStore(cookieStore);

        myClient.put(Constants.user, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                String txtResponse = String.valueOf(response);
                Log.e("", txtResponse);


                Toast toast1 =Toast.makeText(getApplicationContext(),"Datos actualizados", Toast.LENGTH_SHORT);
                toast1.setGravity(Gravity.CENTER, 0, 0);
                toast1.show();
                try {
                    logeoBD.abrir();
                    logeoBD.update_notifications(notifications_enabled);
                    logeoBD.cerrar();

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }



                dialog.hide();

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                dialog.hide();
            }
        });
    }






}