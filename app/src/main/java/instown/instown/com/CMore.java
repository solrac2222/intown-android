package instown.instown.com;

class CMore {
    String id;
    String name;
    String descri_small;
    String photoId;
    String expire;

    CMore(String id, String name, String descri_small, String photoId, String expire) {
        this.name = name;
        this.id = id;
        this.descri_small = descri_small;
        this.photoId = photoId;
        this.expire = expire;
    }
}