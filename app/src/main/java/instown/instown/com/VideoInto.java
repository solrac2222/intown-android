package instown.instown.com;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.view.Window;
import android.widget.VideoView;

/**
 * Created by Francisco on 30/6/15.
 */
public class VideoInto extends Activity {

    VideoView vv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.video);

        vv = (VideoView)this.findViewById(R.id.videoView);

        String uri = "android.resource://" + getPackageName() + "/" + R.raw.video_home;
        vv.setVideoURI(Uri.parse(uri));

        vv.start();

    }

    @Override
    public void onResume() {

        super.onResume();
        TimeApp.reposo(VideoInto.this);

    }


}
