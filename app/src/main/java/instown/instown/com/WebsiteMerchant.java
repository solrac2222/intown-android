package instown.instown.com;

/**
 * Created by Francisco on 29/6/15.
 */

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static java.lang.System.currentTimeMillis;

public class WebsiteMerchant extends ActionBarActivity {
    WebView web;
    String pagina="";
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.website);
        toolbar = (Toolbar)findViewById(R.id.my_awesome_toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("siii");
                onBackPressed();
            }
        });


        web = (WebView) this.findViewById(R.id.web);

        // Provide a WebViewClient for your WebView
        web.setWebViewClient(new MyWebViewClient());
        web.getSettings().setJavaScriptEnabled(true);
        web.getSettings().setLoadWithOverviewMode(true);
        web.getSettings().setUseWideViewPort(true);


        web.getSettings().setBuiltInZoomControls(true);
        web.setInitialScale(1);

        if (!getIntent().getStringExtra("pagina").equals(null)) {
            pagina=getIntent().getStringExtra("pagina");


           // Dictionary<String, String> headers = new Dictionary<String,String>();
            //headers.Add ("CustomHeaderName", "CustomHeaderValue");

            //[requestObj addValue:@"TRUE" forHTTPHeaderField:@"X-INTOWN-MOBILE"];
            Map<String,String> extraHeaders = new HashMap<String, String>();
            extraHeaders.put("X-INTOWN-MOBILE","TRUE");



            Map<String, String> params = new HashMap<String, String>();
            params.put("X-INTOWN-MOBILE", "TRUE");


            web.loadUrl(pagina,params);
        }



    }


    @Override
    protected void onStart()
    {
        super.onStart();

    }

    @Override
    protected void onStop()
    {
        super.onStop();
    }


    @Override
    public void onResume() {

        super.onResume();
        TimeApp.reposo(WebsiteMerchant.this);

    }

    @Override
    public void onBackPressed() {
        finish();
    }



    private class MyWebViewClient extends WebViewClient {

        private long loadTime; // Web page loading time

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {

            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);

            // Save start time
            this.loadTime = currentTimeMillis();
            // Show a toast
            Toast.makeText(getApplicationContext(),"Cargando..",Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            // Calculate load time
            this.loadTime = currentTimeMillis() - this.loadTime;
            // Convert milliseconds to date format
            String time = new SimpleDateFormat("mm:ss:SSS", Locale.getDefault())
                    .format(new Date(this.loadTime));
            // Show a toast


        }
    }

    WebViewClient wvc = new WebViewClient() {
        @Override
        public WebResourceResponse shouldInterceptRequest(WebView view, String url) {

            try {
                DefaultHttpClient client = new DefaultHttpClient();
                HttpGet httpGet = new HttpGet(url);
                httpGet.setHeader("MY-CUSTOM-HEADER", "header value");
                //httpGet.setHeader(HttpHeaders.USER_AGENT, "custom user-agent");
                HttpResponse httpReponse = client.execute(httpGet);

                Header contentType = httpReponse.getEntity().getContentType();
                Header encoding = httpReponse.getEntity().getContentEncoding();
                InputStream responseInputStream = httpReponse.getEntity().getContent();

                String contentTypeValue = null;
                String encodingValue = null;
                if (contentType != null) {
                    contentTypeValue = contentType.getValue();
                }
                if (encoding != null) {
                    encodingValue = encoding.getValue();
                }
                return new WebResourceResponse(contentTypeValue, encodingValue, responseInputStream);
            } catch (ClientProtocolException e) {
                //return null to tell WebView we failed to fetch it WebView should try again.
                return null;
            } catch (IOException e) {
                //return null to tell WebView we failed to fetch it WebView should try again.
                return null;
            }
        }
    };

}