package instown.instown.com;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;
import com.loopj.android.http.RequestParams;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import instown.instown.com.services.MyAppWebViewClient;
import io.fabric.sdk.android.Fabric;


public class InfoFragment extends ActionBarActivity implements View.OnClickListener {
    String tipo="",links,image,share_url,post,business_name,id,titulo,details,address_1,id_coordenadas,location_mapa,city;
    TextView tx_south_two,tx_name,tx_numero,tx_visit;
    WebView web;
    private File mapImageFile = null;
    double latitude=0,longitude=0;
    int disponibles=0;
    Toolbar toolbar;
    ImageView fb_ico,tw_ico,p_ico,image_check;
    ImageView img_mapa,fb_ico_b,tw_ico_b,p_ico_b,tw_inst_b,tw_video_b,yelpicon,opentable_icon,tripadvisoricon,delivery_icon;
    String mostrar_mapa="si",website,facebook,twitter,pinterest,card_details,id_merchant,banner,subcategoria,nombre;
    RelativeLayout cont_incentive;
    public static Button bt_see_more;
    Button bt_remind;
    BDInstown logeoBD;
    int id_usuario,int_mapa=0;
    Dialog dialog_pop;
    String respuestaWS=null;
    String detalles="",video="",instagram,yelp,opentable,tripadvisor,delivery;
    String direccion_total="",phone="",store_hours="N/A";
    TextView tx_phone,tx_hours,tx_titulo,tx_direccion,tx_take;
    double latitudI,longitudI;
    int vacio=0,num_location=0;
    String user_status,has_reminder,can_redeem,has_redeem,can_remind,can_want,has_want;
    JSONArray myObjectLocations;

    //logeo fb
    CallbackManager callbackManager;
    AccessToken accesstoken;
    ShareDialog shareDialog=null;
    ProgressDialog dialog;
    RequestParams params = new RequestParams();

    GPSTracker gps;
    static String Latitude="0";
    static String Longitude="0";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        TwitterAuthConfig authConfig = new TwitterAuthConfig(Constants.consumerKey,Constants.consumerSecret);
        Fabric.with(this, new Twitter(authConfig));
        setContentView(R.layout.info_fragment);

        if( Build.VERSION.SDK_INT >= 9){
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        logeoBD = new BDInstown (this);
        gps = new GPSTracker(InfoFragment.this);
        init();
        events();

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("siii");
                onBackPressed();
            }
        });

        if (!getIntent().getStringExtra("post").equals(null)) {
            post=getIntent().getStringExtra("post");

            try {
                logeoBD.abrir();
                id_usuario=logeoBD.obtener_datos();

                if(id_usuario>0){
                    MyPost();
                }else{
                    new MiTareaPost().execute();
                }
                logeoBD.cerrar();

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }



        }




    }

    @Override
    public void onResume() {

        super.onResume();
        TimeApp.reposo(InfoFragment.this);

    }

    private void init() {
        img_mapa=(ImageView)findViewById(R.id.img_mapa);
        tx_south_two=(TextView)findViewById(R.id.tx_south_two);
        tx_name=(TextView)findViewById(R.id.tx_titulo);
        tx_visit=(TextView)findViewById(R.id.tx_visit);
        tx_direccion=(TextView)findViewById(R.id.tx_direccion);
        tx_phone=(TextView)findViewById(R.id.tx_phone);
        tx_hours=(TextView)findViewById(R.id.tx_hours);
        tx_take=(TextView)findViewById(R.id.tx_take);
        tx_numero=(TextView)findViewById(R.id.tx_numero);
        web=(WebView)findViewById(R.id.web);
        toolbar = (Toolbar)findViewById(R.id.my_awesome_toolbar);
        shareDialog = new ShareDialog(this);
        fb_ico = (ImageView)findViewById(R.id.fb_ico);
        tw_ico = (ImageView)findViewById(R.id.tw_ico);
        p_ico = (ImageView)findViewById(R.id.p_ico);
        fb_ico_b = (ImageView)findViewById(R.id.fb_ico_b);
        tw_ico_b = (ImageView)findViewById(R.id.tw_ico_b);
        p_ico_b = (ImageView)findViewById(R.id.p_ico_b);
        tw_inst_b= (ImageView)findViewById(R.id.tw_inst_b);
        tw_video_b= (ImageView)findViewById(R.id.tw_video_b);
        yelpicon= (ImageView)findViewById(R.id.yelpicon);
        opentable_icon= (ImageView)findViewById(R.id.opentable_icon);
        tripadvisoricon= (ImageView)findViewById(R.id.tripadvisoricon);
        delivery_icon= (ImageView)findViewById(R.id.delivery_icon);
        image_check= (ImageView)findViewById(R.id.image_check);
        cont_incentive= (RelativeLayout)findViewById(R.id.cont_incentive);
        bt_see_more = (Button)findViewById(R.id.bt_see_more);
        bt_remind = (Button)findViewById(R.id.bt_remind);

        if(mostrar_mapa=="si"){
            tx_take.setOnClickListener(this);
        }else{
            tx_take.setVisibility(View.INVISIBLE);
        }

    }

    private void events() {
        fb_ico.setOnClickListener(this);
        tw_ico.setOnClickListener(this);
        p_ico.setOnClickListener(this);
        cont_incentive.setOnClickListener(this);
        fb_ico_b.setOnClickListener(this);
        tw_ico_b.setOnClickListener(this);
        p_ico_b.setOnClickListener(this);
        tw_inst_b.setOnClickListener(this);
        tw_video_b.setOnClickListener(this);
        yelpicon.setOnClickListener(this);
        opentable_icon.setOnClickListener(this);
        tripadvisoricon.setOnClickListener(this);
        delivery_icon.setOnClickListener(this);
        tx_hours.setOnClickListener(this);
        tx_phone.setOnClickListener(this);
        image_check.setOnClickListener(this);
        tx_visit.setOnClickListener(this);


        tx_south_two.setOnClickListener(this);
        bt_see_more.setOnClickListener(this);
        bt_remind.setOnClickListener(this);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tx_phone:
                Uri numero = Uri.parse( "tel:" + phone);
                Intent intentCall = new Intent(Intent.ACTION_CALL, numero);
                startActivity(intentCall);
                break;
            case R.id.image_check:
                try {
                    logeoBD.abrir();
                    id_usuario=logeoBD.obtener_datos();
                    if(id_usuario>0){
                        if(image_check.getTag().toString().equals("no_follow")){
                            BizFollow(InfoFragment.this,id_merchant);
                        }else{
                            BizUnFollow(InfoFragment.this,id_merchant);
                        }
                    }else{
                        Intent menu = new Intent(InfoFragment.this, LoginPop.class);
                        startActivity(menu);
                    }
                    logeoBD.cerrar();
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                break;
            case R.id.bt_see_more:
                Intent ventana_post= new Intent(this, InfoFragmentDetails.class);
                ventana_post.putExtra("banner",banner);
                ventana_post.putExtra("website",website);
                ventana_post.putExtra("facebook",facebook);
                ventana_post.putExtra("twitter",twitter);
                ventana_post.putExtra("pinterest",pinterest);
                ventana_post.putExtra("id_merchant",id_merchant);
                ventana_post.putExtra("business_name",business_name);
                ventana_post.putExtra("card_details",card_details);
                startActivity(ventana_post);
                break;
            case R.id.tx_visit:
                change_locations();

                break;
            case R.id.bt_remind:

                try {
                    logeoBD.abrir();
                    id_usuario=logeoBD.obtener_datos();

                    if(id_usuario>0){

                        if(can_remind.toString().equals("true")){
                            FavoritoInfo(InfoFragment.this, id, dialog_pop);
                        }else if(can_want.toString().equals("true")){
                            FavoritoInfo(InfoFragment.this, id, dialog_pop);
                        }else if(can_redeem.toString().equals("true")){
                            new AlertDialog.Builder(InfoFragment.this)
                                    .setTitle("InTown")
                                    .setMessage("Please SHOW the gift card at time of \"purchase\" via your mobile BEFORE you press redeem so participating business can confirm and validate.\n\nREQUIRED!\n\nNo screenshots will be accepted\n\nUse FLIP to see additional details\n\n")
                                    .setNegativeButton("Save for Later", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {


                                        }
                                    })
                                    .setPositiveButton("REDEEM NOW!", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            FavoritoInfo(InfoFragment.this, id, dialog_pop);

                                        }
                                    })
                                            .show();
                        }


                    }else{
                        Intent menu = new Intent(InfoFragment.this, LoginPop.class);
                        startActivity(menu);
                    }
                    logeoBD.cerrar();

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                break;
            case R.id.tx_south_two:
                Uri uriWeb = Uri.parse(website);
                Intent intentWeb = new Intent(Intent.ACTION_VIEW, uriWeb);
                startActivity(intentWeb);
                break;
            case R.id.fb_ico:
                ShareLinkContent linkContent = new ShareLinkContent.Builder()
                        .setContentTitle(Constants.NAME_APP)
                        .setContentUrl(Uri.parse(share_url))
                        .build();

                shareDialog.show(linkContent);

                break;
            case R.id.tw_ico:
                TweetComposer.Builder builder = new TweetComposer.Builder(InfoFragment.this)
                        .text(share_url);
                builder.show();
                break;
            case R.id.p_ico:
                String mediaUrl = image;
                String description = business_name;
                String url = String.format(
                        "https://www.pinterest.com/pin/create/button/?url=%s&media=%s&description=%s",
                        urlEncode(share_url), urlEncode(mediaUrl), description);
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                filterByPackageName(InfoFragment.this, intent, "com.pinterest");
                startActivity(intent);

                break;
            case R.id.fb_ico_b:
                if (facebook != null && !facebook.isEmpty()) {
                    abrir_url(facebook);
                }else{
                    Toast toast2 =Toast.makeText(InfoFragment.this,"No Disponible", Toast.LENGTH_SHORT);
                    toast2.show();
                }
                break;
            case R.id.tw_ico_b:
                if (twitter != null && !twitter.isEmpty()) {
                    abrir_url(twitter);
                }else{
                    Toast toast2 =Toast.makeText(InfoFragment.this,"No Disponible", Toast.LENGTH_SHORT);
                    toast2.show();
                }
                break;
            case R.id.p_ico_b:
                if (pinterest != null && !pinterest.isEmpty()) {
                    abrir_url(pinterest);
                }else{
                    Toast toast2 =Toast.makeText(InfoFragment.this,"No Disponible", Toast.LENGTH_SHORT);
                    toast2.show();
                }
                break;
            case R.id.tw_video_b:
                if (video != null && !video.isEmpty()) {
                    abrir_url(video);
                }else{
                    Toast toast2 =Toast.makeText(InfoFragment.this,"No Disponible", Toast.LENGTH_SHORT);
                    toast2.show();
                }
                break;
            case R.id.yelpicon:
                if (yelp != null && !yelp.isEmpty()) {
                    abrir_url(yelp);
                }else{
                    Toast toast2 =Toast.makeText(InfoFragment.this,"No Disponible", Toast.LENGTH_SHORT);
                    toast2.show();
                }
                break;


            case R.id.delivery_icon:
                if (delivery != null && !delivery.isEmpty()) {
                    abrir_url(delivery);
                }else{
                    Toast toast2 =Toast.makeText(InfoFragment.this,"No Disponible", Toast.LENGTH_SHORT);
                    toast2.show();
                }
                break;
            case R.id.tripadvisoricon:
                if (tripadvisor != null && !tripadvisor.isEmpty()) {
                    abrir_url(tripadvisor);
                }else{
                    Toast toast2 =Toast.makeText(InfoFragment.this,"No Disponible", Toast.LENGTH_SHORT);
                    toast2.show();
                }
                break;
            case R.id.opentable_icon:
                if (opentable != null && !opentable.isEmpty()) {
                    abrir_url(opentable);
                }else{
                    Toast toast2 =Toast.makeText(InfoFragment.this,"No Disponible", Toast.LENGTH_SHORT);
                    toast2.show();
                }
                break;
            case R.id.tw_inst_b:
                if (instagram != null && !instagram.isEmpty()) {
                    abrir_url(instagram);
                }else{
                    Toast toast2 =Toast.makeText(InfoFragment.this,"No Disponible", Toast.LENGTH_SHORT);
                    toast2.show();
                }
                break;
            case R.id.tx_hours:
                new AlertDialog.Builder(InfoFragment.this)
                        .setTitle("InTown")
                        .setMessage(store_hours)
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which){


                            }
                        })
                        .show();
                break;
            case R.id.tx_take:
                if(int_mapa==0){
                    Toast toast2 =Toast.makeText(InfoFragment.this,"No Disponible", Toast.LENGTH_SHORT);
                    toast2.show();
                }else{
                    gps = new GPSTracker(InfoFragment.this);

                    if(gps.canGetLocation()){
                        latitudI = gps.getLatitude();
                        longitudI = gps.getLongitude();
                        String uri = "http://maps.google.com/maps?saddr="+latitudI+","+longitudI+"&daddr="+latitude+","+longitude;
                        Intent intentM = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                        intentM.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                        startActivity(intentM);
                    }else{
                        gps.showSettingsAlert();
                    }


                }

        }

    }

    private void change_locations() {
            AlertDialog.Builder builderSingle = new AlertDialog.Builder(InfoFragment.this);
            builderSingle.setTitle("Select a location");
            final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(InfoFragment.this,
                    android.R.layout.simple_list_item_1);


            for(int j = 0; j < myObjectLocations.length(); j++){
                JSONObject d = null;
                try {
                    d = myObjectLocations.getJSONObject(j);
                    arrayAdapter.add(d.getString("address_1"));

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }



            builderSingle.setNegativeButton("Cancelar",
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

            builderSingle.setAdapter(arrayAdapter,
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            rellenar_info(which);
                        }
                    });
            builderSingle.show();
    }

    private void abrir_url(String url) {
        Uri uriWeb = Uri.parse(url);
        Intent intentWeb = new Intent(Intent.ACTION_VIEW, uriWeb);
        startActivity(intentWeb);
    }

    private void pop_info() {
        dialog_pop = new Dialog(this);
        dialog_pop.requestWindowFeature(Window.FEATURE_NO_TITLE);


        if(can_redeem.toString().equals("true")){
            dialog_pop.setContentView(R.layout.pop_info_redeem);
        }else{
            dialog_pop.setContentView(R.layout.pop_info);
            Button bt_show=(Button) dialog_pop.findViewById(R.id.bt_show);
            TextView textView4=(TextView) dialog_pop.findViewById(R.id.textView4);
            textView4.setText(detalles);
            bt_show.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent VentanaCategorias = new Intent(InfoFragment.this, ExploreFragment.class);
                    VentanaCategorias.putExtra("subcategoria", subcategoria);
                    VentanaCategorias.putExtra("nombre", nombre);
                    startActivity(VentanaCategorias);
                    dialog_pop.hide();
                }
            });
        }


        Button bt_close=(Button) dialog_pop.findViewById(R.id.bt_close);
        bt_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_pop.hide();
            }
        });

        dialog_pop.show();
    }

    public static String urlEncode(String s) {
        try {
            return URLEncoder.encode(s, "UTF-8");
        }
        catch (UnsupportedEncodingException e) {
            Log.wtf("", "UTF-8 should always be supported", e);
            return "";
        }
    }

    public static void filterByPackageName(Context context, Intent intent, String prefix) {
        List<ResolveInfo> matches = context.getPackageManager().queryIntentActivities(intent, 0);
        for (ResolveInfo info : matches) {
            if (info.activityInfo.packageName.toLowerCase().startsWith(prefix)) {
                intent.setPackage(info.activityInfo.packageName);
                return;
            }
        }
    }

    private class MiTareaPost extends AsyncTask<Void, Integer, Boolean> {
        private ProgressDialog dialog;

        @Override
        protected Boolean doInBackground(Void... params) {

            HttpClient httpclient = new DefaultHttpClient();
            HttpGet httppost = new HttpGet(Constants.post+post);
            System.out.println(Constants.post + post);
            try {
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);

                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);

                HttpEntity entity = response.getEntity();
                InputStream instream = entity.getContent();
                String result = Constants.convertStreamToString(instream);
                System.out.println("result "+result);

                JSONArray resultado= null;
                JSONObject myObject = new JSONObject(result);

                id=myObject.getString("id");
                String merchant=myObject.getString("merchant");
                String category=myObject.getString("category");

                JSONObject myObjectMerchant  = new JSONObject(merchant);

                if(category.toString().equals("null")){
                    subcategoria="";
                    nombre="";
                }else{
                    JSONObject myObjectCategory  = new JSONObject(category);
                    subcategoria=myObjectCategory.getString("id");
                    nombre=myObjectCategory.getString("name");
                }


                tipo=myObject.getString("type");
                business_name=myObjectMerchant.getString("business_name");
                id_merchant=myObjectMerchant.getString("id");
                banner=myObjectMerchant.getString("banner");
                image=myObjectMerchant.getString("image");
                id_coordenadas=myObjectMerchant.getString("id");
                titulo=myObject.getString("title");
                details=myObject.getString("details");
                card_details=myObject.getString("card_details");
                user_status=myObject.getString("user_status");
                JSONObject myObjectUserStatus = new JSONObject(user_status);
                has_reminder=myObjectUserStatus.getString("has_reminder");
                can_redeem=myObjectUserStatus.getString("can_redeem");
                has_redeem=myObjectUserStatus.getString("has_redeem");
                can_remind=myObjectUserStatus.getString("can_remind");
                can_want=myObjectUserStatus.getString("can_want");
                has_want=myObjectUserStatus.getString("has_want");





                System.out.println(myObject.getString("user_status"));
                links=myObjectMerchant.getString("links");
                JSONArray myObjectLinks  = new JSONArray(links);
                for(int i=0;i<myObjectLinks.length();i++){
                    JSONObject cLink = myObjectLinks.getJSONObject(i);
                    if(cLink.getString("type").toString().equals("0")){
                        website=cLink.getString("url").toString();
                    }else if(cLink.getString("type").toString().equals("1")){
                        facebook=cLink.getString("url").toString();
                    }else if(cLink.getString("type").toString().equals("2")){
                        twitter=cLink.getString("url").toString();
                    }else if(cLink.getString("type").toString().equals("4")){
                        pinterest=cLink.getString("url").toString();
                    }else if(cLink.getString("type").toString().equals("5")){
                        instagram=cLink.getString("url").toString();
                    }else if(cLink.getString("type").toString().equals("6")){
                        video=cLink.getString("url").toString();
                    }else if(cLink.getString("type").toString().equals("6")){
                        yelp=cLink.getString("url").toString();
                    }else if(cLink.getString("type").toString().equals("8")){
                        opentable=cLink.getString("url").toString();
                    }else if(cLink.getString("type").toString().equals("9")){
                        tripadvisor=cLink.getString("url").toString();
                    }else if(cLink.getString("type").toString().equals("10")){
                        delivery=cLink.getString("url").toString();
                    }
                }


                share_url=myObject.getString("share_url");
                String locations=myObject.getString("locations");
                myObjectLocations  = new JSONArray(locations);



                disponibles=myObjectLocations.length();

                if(myObjectLocations.length()!=0){
                    JSONObject c = myObjectLocations.getJSONObject(0);
                    address_1=c.getString("address_1");
                    city=c.getString("city");
                }



                num_location=myObjectLocations.length();
                System.out.println("num_location "+num_location);



                for(int j = 0; j < myObjectLocations.length(); j++){
                    JSONObject d = myObjectLocations.getJSONObject(j);

                    if(j==0){
                        String[] location1 = d.getString("location").split("\\(");
                        String[] location2 = location1[1].split("\\)");
                        String[] location3 = location2[0].split(" ");
                        System.out.println("locaio "+d.getString("location"));
                        latitude  = Double.parseDouble(location3[1]);
                        longitude = Double.parseDouble(location3[0]);
                        int_mapa=1;
                        direccion_total=d.getString("address_1")+"\n"+d.getString("city")+", "+d.getString("state")+" "+d.getString("zip_code");
                        phone=d.getString("phone");
                        store_hours=d.getString("store_hours");
                        loadClinicMapImage(latitude, longitude);
                    }
                }

                if(myObjectLocations.length()==0){
                    mostrar_mapa="no";
                }


            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
            } catch (IOException e) {
                // TODO Auto-generated catch block
            } catch (JSONException e) {
                e.printStackTrace();
            }


            return true;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            int progreso = values[0].intValue();

        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(InfoFragment.this);
            dialog.setMessage("Cargando Datos..");
            dialog.show();
        }



        @Override
        protected void onPostExecute(Boolean result) {
            dialog.hide();
            tx_name.setText(business_name);
            tx_visit.setText(num_location+" Locations");
            tx_direccion.setText(direccion_total);
            tx_phone.setText(phone);


            if(num_location==0){
                tx_visit.setVisibility(View.GONE);
                img_mapa.setVisibility(View.GONE);
                tx_take.setVisibility(View.GONE);
            }else if(num_location==1){
                tx_visit.setVisibility(View.GONE);
            }


            if(store_hours.toString().equals("N/A")) {
                tx_hours.setVisibility(View.INVISIBLE);
            }

            if(phone.toString().equals("null")) {
                tx_phone.setVisibility(View.INVISIBLE);
            }




            if(can_remind.toString().equals("true")){
                bt_remind.setText("Remind Me!");
            }else if(can_want.toString().equals("true")){
                bt_remind.setText("I WANT THIS");
                bt_remind.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                bt_remind.setTextColor(Color.parseColor("#ffffff"));
                bt_remind.setBackgroundResource(R.drawable.border_rodado);
            }else if(can_redeem.toString().equals("true")){
                bt_remind.setText("REDEEM");
                bt_remind.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                bt_remind.setTextColor(Color.parseColor("#ffffff"));
                bt_remind.setBackgroundResource(R.drawable.border_rodado);
            }else{
                bt_remind.setVisibility(View.INVISIBLE);
            }



            if (facebook != null && !facebook.isEmpty()) {
                fb_ico_b.setVisibility(View.VISIBLE);
            }

            if (twitter != null && !twitter.isEmpty()) {
                tw_ico_b.setVisibility(View.VISIBLE);
            }

            if (pinterest != null && !pinterest.isEmpty()) {
                p_ico_b.setVisibility(View.VISIBLE);
            }

            if (instagram != null && !instagram.isEmpty()) {
                tw_inst_b.setVisibility(View.VISIBLE);
            }

            if (video != null && !video.isEmpty()) {
                tw_video_b.setVisibility(View.VISIBLE);
            }

            if (yelp != null && !yelp.isEmpty()) {
                yelpicon.setVisibility(View.VISIBLE);
            }

            if (opentable != null && !opentable.isEmpty()) {
                opentable_icon.setVisibility(View.VISIBLE);
            }

            if (tripadvisor != null && !tripadvisor.isEmpty()) {
                tripadvisoricon.setVisibility(View.VISIBLE);
            }

            if (delivery != null && !delivery.isEmpty()) {
                delivery_icon.setVisibility(View.VISIBLE);
            }



            FollowWS(id_merchant);

          //  web.loadData(details, "text/html", null);
            System.out.println("http://www.intown.com/post/" + post + "/content/");


            web.getSettings().setLoadWithOverviewMode(true);
            web.getSettings().setUseWideViewPort(true);
            WebSettings webSettings = web.getSettings();
            webSettings.setJavaScriptEnabled(true);



            CookieSyncManager.createInstance(InfoFragment.this);
            CookieManager.getInstance().setAcceptCookie(true);
            CookieSyncManager.getInstance().startSync();
            CookieSyncManager.getInstance().sync();

            web.setWebViewClient(new MyAppWebViewClient());
            web.loadUrl("http://www.intown.com/post/" + post + "/content/");










            new MiTareaCoordenadas().execute();
        }
    }



    public void MyPost(){
        dialog = new ProgressDialog(InfoFragment.this);
        dialog.setMessage("Please Wait...");
        dialog.show();
        AsyncHttpClient myClient = new AsyncHttpClient();

        PersistentCookieStore cookieStore = new PersistentCookieStore(InfoFragment.this);
        myClient.setCookieStore(cookieStore);

        myClient.get(Constants.post+post, null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                String txtResponse = String.valueOf(response);
                System.out.println(txtResponse);

                try {
                    JSONArray resultado= null;
                    JSONObject myObject = new JSONObject(txtResponse);

                    id=myObject.getString("id");
                    String merchant=myObject.getString("merchant");
                    String category=myObject.getString("category");

                    JSONObject myObjectMerchant  = new JSONObject(merchant);
                    if(category.toString().equals("null")){
                        subcategoria="";
                        nombre="";
                    }else{
                        JSONObject myObjectCategory  = new JSONObject(category);
                        subcategoria=myObjectCategory.getString("id");
                        nombre=myObjectCategory.getString("name");
                    }

                    tipo=myObject.getString("type");
                    business_name=myObjectMerchant.getString("business_name");
                    id_merchant=myObjectMerchant.getString("id");
                    banner=myObjectMerchant.getString("banner");
                    image=myObjectMerchant.getString("image");
                    id_coordenadas=myObjectMerchant.getString("id");
                    titulo=myObject.getString("title");
                    details=myObject.getString("details");
                    card_details=myObject.getString("card_details");
                    user_status=myObject.getString("user_status");
                    JSONObject myObjectUserStatus = new JSONObject(user_status);
                    has_reminder=myObjectUserStatus.getString("has_reminder");
                    can_redeem=myObjectUserStatus.getString("can_redeem");
                    has_redeem=myObjectUserStatus.getString("has_redeem");
                    can_remind=myObjectUserStatus.getString("can_remind");
                    can_want=myObjectUserStatus.getString("can_want");
                    has_want=myObjectUserStatus.getString("has_want");

                    //System.out.println(myObject.getString("user_status"));
                    links=myObjectMerchant.getString("links");
                    JSONArray myObjectLinks  = new JSONArray(links);
                    for(int i=0;i<myObjectLinks.length();i++){
                        JSONObject cLink = myObjectLinks.getJSONObject(i);
                        if(cLink.getString("type").toString().equals("0")){
                            website=cLink.getString("url").toString();
                        }else if(cLink.getString("type").toString().equals("1")){
                            facebook=cLink.getString("url").toString();
                        }else if(cLink.getString("type").toString().equals("2")){
                            twitter=cLink.getString("url").toString();
                        }else if(cLink.getString("type").toString().equals("4")){
                            pinterest=cLink.getString("url").toString();
                        }else if(cLink.getString("type").toString().equals("5")){
                            instagram=cLink.getString("url").toString();
                        }else if(cLink.getString("type").toString().equals("6")){
                            video=cLink.getString("url").toString();
                        }else if(cLink.getString("type").toString().equals("6")){
                            yelp=cLink.getString("url").toString();
                        }else if(cLink.getString("type").toString().equals("8")){
                            opentable=cLink.getString("url").toString();
                        }else if(cLink.getString("type").toString().equals("9")){
                            tripadvisor=cLink.getString("url").toString();
                        }else if(cLink.getString("type").toString().equals("10")){
                            delivery=cLink.getString("url").toString();
                        }
                    }


                    share_url=myObject.getString("share_url");
                    String locations=myObject.getString("locations");
                    myObjectLocations  = new JSONArray(locations);

                    disponibles=myObjectLocations.length();

                    if(myObjectLocations.length()!=0){
                        JSONObject c = myObjectLocations.getJSONObject(0);
                        address_1=c.getString("address_1");
                        city=c.getString("city");
                    }



                    num_location=myObjectLocations.length();
                    System.out.println("num_location "+num_location);
                    if(num_location==0){
                        tx_visit.setVisibility(View.GONE);
                        img_mapa.setVisibility(View.GONE);
                        tx_take.setVisibility(View.GONE);
                    }else if(num_location==1){
                        tx_visit.setVisibility(View.GONE);
                    }


                    for(int j = 0; j < myObjectLocations.length(); j++){
                        JSONObject d = myObjectLocations.getJSONObject(j);

                        if(j==0){
                            String[] location1 = d.getString("location").split("\\(");
                            String[] location2 = location1[1].split("\\)");
                            String[] location3 = location2[0].split(" ");
                            System.out.println("locaio "+d.getString("location"));
                            latitude  = Double.parseDouble(location3[1]);
                            longitude = Double.parseDouble(location3[0]);
                            int_mapa=1;
                            direccion_total=d.getString("address_1")+"\n"+d.getString("city")+", "+d.getString("state")+" "+d.getString("zip_code");
                            phone=d.getString("phone");
                            store_hours=d.getString("store_hours");
                            loadClinicMapImage(latitude, longitude);
                        }
                    }

                    if(myObjectLocations.length()==0){
                        mostrar_mapa="no";
                    }



                    //RECUPERAR DATOS

                    tx_name.setText(business_name);
                    tx_visit.setText(num_location+" Locations");
                    tx_direccion.setText(direccion_total);
                    tx_phone.setText(phone);

                    if(store_hours.toString().equals("N/A")) {
                        tx_hours.setVisibility(View.INVISIBLE);
                    }

                    if(phone.toString().equals("null")) {
                        tx_phone.setVisibility(View.INVISIBLE);
                    }




                    if(can_remind.toString().equals("true")){
                        bt_remind.setText("Remind Me!");
                    }else if(can_want.toString().equals("true")){
                        bt_remind.setText("I WANT THIS");
                        bt_remind.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                        bt_remind.setTextColor(Color.parseColor("#ffffff"));
                        bt_remind.setBackgroundResource(R.drawable.border_rodado);
                    }else if(can_redeem.toString().equals("true")){
                        bt_remind.setText("REDEEM");
                        bt_remind.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                        bt_remind.setTextColor(Color.parseColor("#ffffff"));
                        bt_remind.setBackgroundResource(R.drawable.border_rodado);
                    }else{
                        bt_remind.setVisibility(View.INVISIBLE);
                    }



                    if (facebook != null && !facebook.isEmpty()) {
                        fb_ico_b.setVisibility(View.VISIBLE);
                    }

                    if (twitter != null && !twitter.isEmpty()) {
                        tw_ico_b.setVisibility(View.VISIBLE);
                    }

                    if (pinterest != null && !pinterest.isEmpty()) {
                        p_ico_b.setVisibility(View.VISIBLE);
                    }

                    if (instagram != null && !instagram.isEmpty()) {
                        tw_inst_b.setVisibility(View.VISIBLE);
                    }

                    if (video != null && !video.isEmpty()) {
                        tw_video_b.setVisibility(View.VISIBLE);
                    }

                    if (yelp != null && !yelp.isEmpty()) {
                        yelpicon.setVisibility(View.VISIBLE);
                    }

                    if (opentable != null && !opentable.isEmpty()) {
                        opentable_icon.setVisibility(View.VISIBLE);
                    }

                    if (tripadvisor != null && !tripadvisor.isEmpty()) {
                        tripadvisoricon.setVisibility(View.VISIBLE);
                    }

                    if (delivery != null && !delivery.isEmpty()) {
                        delivery_icon.setVisibility(View.VISIBLE);
                    }

                    dialog.hide();

                    FollowWS(id_merchant);

                    //  web.loadData(details, "text/html", null);
                    System.out.println("http://www.intown.com/post/"+post+"/content/");
                    web.loadUrl("http://www.intown.com/post/"+post+"/content/");

                    web.getSettings().setLoadWithOverviewMode(true);
                    web.getSettings().setUseWideViewPort(true);
                    WebSettings webSettings = web.getSettings();
                    webSettings.setJavaScriptEnabled(true);
                    //web.setWebViewClient(new WebViewClient());
                    web.setWebViewClient(new MyAppWebViewClient());

                    new MiTareaCoordenadas().execute();





                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }



            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                dialog.hide();

            }
        });
    }




    public void FollowWS(String idM){
        dialog = new ProgressDialog(InfoFragment.this);
        dialog.setMessage("Please Wait...");
        dialog.show();
        AsyncHttpClient myClient = new AsyncHttpClient();

        try {
            logeoBD.abrir();
            id_usuario=logeoBD.obtener_datos();
            if(id_usuario>0){
                PersistentCookieStore cookieStore = new PersistentCookieStore(InfoFragment.this);
                myClient.setCookieStore(cookieStore);
            }
            logeoBD.cerrar();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        System.out.println(Constants.posts + idM);
        myClient.get(Constants.posts + idM, null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                String txtResponse = String.valueOf(response);
                System.out.println(txtResponse);

                try {
                    JSONObject c = new JSONObject(txtResponse);
                    tx_numero.setText(c.getString("incentive"));

                    if (!c.getString("has_incentive").toString().equals("true")) {
                        tx_numero.setTextColor(Color.parseColor("#000000"));
                    }


                    if (c.getString("is_following") == "false") {
                        image_check.setImageResource(R.drawable.checkoff);
                        image_check.setTag("no_follow");
                    } else {
                        image_check.setImageResource(R.drawable.checkon);
                        image_check.setTag("follow");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                dialog.hide();


            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                dialog.hide();

            }
        });
    }



    private class MiTareaCoordenadas extends AsyncTask<Void, Integer, Boolean> {
        @Override
        protected Boolean doInBackground(Void... params) {

            HttpClient httpclient = new DefaultHttpClient();
            HttpGet httppost = new HttpGet(Constants.location+id_coordenadas);
            System.out.println(Constants.location+id_coordenadas);
            try {
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);

                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);

                HttpEntity entity = response.getEntity();
                InputStream instream = entity.getContent();
                String result = Constants.convertStreamToString(instream);
                System.out.println("result "+result);

                JSONArray resultado= null;
                JSONObject myObject = new JSONObject(result);
                if (!myObject.isNull("detail")){
                    location_mapa=myObject.getString("detail");
                }else{
                    location_mapa=myObject.getString("location");
                }





            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
            } catch (IOException e) {
                // TODO Auto-generated catch block
            } catch (JSONException e) {
                e.printStackTrace();
            }


            return true;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {


        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }



        @Override
        protected void onPostExecute(Boolean result) {

            System.out.println(location_mapa);
            if(!location_mapa.equals("Not found.")){

                String[] location1 = location_mapa.split("\\(");
                String[] location2 = location1[1].split("\\)");
                String[] location3 = location2[0].split(" ");

                System.out.println(location3[0]+"lll"+location3[1]);

                latitude  = Double.parseDouble(location3[1]);
                longitude = Double.parseDouble(location3[0]);

                //loadClinicMapImage(latitude, longitude);


            }
           //POINT (-79.8269520000000057 32.8328952999999970)


           // POINT (-95.3686169999999720 29.7531190000000016)
            //System.out.println(location_mapa);
            //String[] location1 = location.split("POINT \\(");
            //System.out.println(location1[0]);




           // img_mapa.setImageBitmap(bitmap);

        }
    }

    public void  FavoritoInfo(Activity activity,String id_info,Dialog dialogR){
        final Dialog dialogE=dialogR;

        dialog = new ProgressDialog(activity);
        dialog.setMessage("Please Wait...");
        dialog.show();
        AsyncHttpClient myClient = new AsyncHttpClient();
        PersistentCookieStore cookieStore = new PersistentCookieStore(activity);
        myClient.setCookieStore(cookieStore);

        String urlFinal="";

        if(can_remind.toString().equals("true")){
            urlFinal=String.format(Constants.posts_remind, id_info);
        }else if(can_want.toString().equals("true")){
            urlFinal=String.format(Constants.posts_want, id_info);
        }else if(can_redeem.toString().equals("true")){
            urlFinal = String.format(Constants.posts_redeem, id_info);
        }
        System.out.println(urlFinal);

        myClient.post(urlFinal, null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                String txtResponse = String.valueOf(response);
                Log.e("", txtResponse);


                try {
                    JSONObject myObject = new JSONObject(txtResponse);
                    //JSONObject myObject = response;


                    if (!myObject.isNull("detail")) {
                        detalles = myObject.getString("detail");
                        pop_info();
                    }
                    if(can_redeem.toString().equals("true")){
                        pop_info();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
//                dialogE.hide();
                dialog.hide();

                // Toast toast1 = Toast.makeText(getApplicationContext(), detalles, Toast.LENGTH_SHORT);
                //toast1.show();
                detalles = "";
                bt_remind.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                System.out.println(statusCode);
                Log.d("THROW", throwable.toString());
                dialog.dismiss();
            }

        });
    }

    private void openMap() {
        if(latitude!=0){
            String uri = "geo:" + latitude + "," + longitude+"?z="+17;
            System.out.println(uri);
            Intent openMapIntent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri));
            startActivity(openMapIntent);
        }


    }

    public static Bitmap fetchGoogleMapThumbnail(final double latitude,final double longitude,final int zoom,final int width,final int height) {
        // Query
        final String url = "http://maps.google.com/maps/api/staticmap?center=" + latitude + "," + longitude + "&zoom=" + zoom + "&size=" + width + "x" + height + "&markers=color:red%7Clabel:C%7C" + latitude + "," + longitude + "&scale=2&&sensor=false";

        Bitmap bitmap = null;
        HttpClient httpClient = new DefaultHttpClient();
        HttpGet httpRequest = new HttpGet(url);

        InputStream inputStream = null;

        try {
            inputStream = httpClient.execute(httpRequest).getEntity().getContent();
            bitmap = BitmapFactory.decodeStream(inputStream);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }

                if (httpClient != null) {
                    httpClient.getConnectionManager().shutdown();
                }
            }
            catch (Exception e) {
                // Ignored
            }

            // Marked for GC
            httpClient = null;
            httpRequest = null;
            inputStream = null;
        }

        return bitmap;
    }


    public void  BizFollow(final Activity activity, String id_info){
        dialog = new ProgressDialog(activity);
        dialog.setMessage("Please Wait...");
        dialog.show();
        AsyncHttpClient myClient = new AsyncHttpClient();
        PersistentCookieStore cookieStore = new PersistentCookieStore(activity);
        myClient.setCookieStore(cookieStore);

        myClient.post(Constants.seguir_posts+id_info+"/follow/", null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                String txtResponse = String.valueOf(response);
                Log.e("", txtResponse);


                image_check.setImageResource(R.drawable.checkon);
                image_check.setTag("follow");
                pop_mensaje();
                dialog.hide();
                detalles="";

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                dialog.hide();

            }
        });
    }

    public void  BizUnFollow(final Activity activity, String id_info){
        dialog = new ProgressDialog(activity);
        dialog.setMessage("Please Wait...");
        dialog.show();
        AsyncHttpClient myClient = new AsyncHttpClient();
        PersistentCookieStore cookieStore = new PersistentCookieStore(activity);
        myClient.setCookieStore(cookieStore);

        myClient.post(Constants.seguir_posts+id_info+"/unfollow/", null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                String txtResponse = String.valueOf(response);
                Log.e("", txtResponse);


                image_check.setImageResource(R.drawable.checkoff);
                image_check.setTag("no_follow");
                dialog.hide();

                detalles="";

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                dialog.hide();

            }
        });
    }


    private void pop_mensaje() {
        dialog_pop = new Dialog(InfoFragment.this);
        dialog_pop.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_pop.setContentView(R.layout.pop_biz_interno);
        String titulo="Thanks for \"Cheking Us out!\"";
        String description="Our events and happenings will now post on \"My Page\" so you can stay In The Know!";

        Button bt_close=(Button) dialog_pop.findViewById(R.id.bt_close);
        TextView textView3=(TextView) dialog_pop.findViewById(R.id.textView3);
        TextView textView4=(TextView) dialog_pop.findViewById(R.id.textView4);
        textView4.setText(description);
        textView3.setText(titulo);


        bt_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_pop.hide();
            }
        });

        dialog_pop.show();
    }



    private void loadClinicMapImage(final double latitude, final double longitude){
        // Define the asynchronous task
        AsyncTask<Bitmap, String, Bitmap> loadClinicMapImageTask = new AsyncTask<Bitmap, String, Bitmap>() {
            private ProgressDialog pd = null;

            @Override
            protected void onPreExecute() {

            }

            @Override
            protected Bitmap doInBackground(Bitmap... params) {
                Bitmap bitmap = null;


                bitmap = fetchGoogleMapThumbnail(latitude, longitude, 15, 120, 120);

                // Write Image File
                        /*if (! ImageDownloader.writeImageFile(bitmap, mapImageFile, Bitmap.CompressFormat.PNG)) {
                            bitmap = null;
                        }*/
                return bitmap;
            }

            @Override
            protected void onPostExecute(Bitmap bitmap) {

                if (bitmap == null) {
                    img_mapa.setVisibility(View.INVISIBLE);
                } else {
                    img_mapa.setImageBitmap(bitmap);
                    img_mapa.setVisibility(View.VISIBLE);
                }
            }
        };

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            loadClinicMapImageTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            loadClinicMapImageTask.execute();
        }
    }


    private void rellenar_info(int posicion) {
        JSONObject d = null;
        try {
            d = myObjectLocations.getJSONObject(posicion);
            address_1=d.getString("address_1");
            city=d.getString("city");

            String[] location1 = d.getString("location").split("\\(");
            String[] location2 = location1[1].split("\\)");
            String[] location3 = location2[0].split(" ");
            System.out.println("locaio "+d.getString("location"));
            latitude  = Double.parseDouble(location3[1]);
            longitude = Double.parseDouble(location3[0]);
            int_mapa=1;
            direccion_total=d.getString("address_1")+"\n"+d.getString("city")+", "+d.getString("state")+" "+d.getString("zip_code");
            phone=d.getString("phone");
            store_hours=d.getString("store_hours");
            loadClinicMapImage(latitude, longitude);


            tx_direccion.setText(direccion_total);
            tx_phone.setText(phone);
            tx_hours.setText(store_hours);

            if(store_hours.toString().equals("N/A")) {
                tx_hours.setVisibility(View.INVISIBLE);
            }

            if(phone.toString().equals("null")) {
                tx_phone.setVisibility(View.INVISIBLE);
            }



        } catch (JSONException e) {
            e.printStackTrace();
        }




    }
}