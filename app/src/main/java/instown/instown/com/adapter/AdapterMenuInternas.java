package instown.instown.com.adapter;

/**
 * Created by Francisco on 6/7/15.
 */

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Build;
import android.os.StrictMode;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import instown.instown.com.R;


public class AdapterMenuInternas extends BaseAdapter {

    private Activity activity;
    private static LayoutInflater inflater=null;
    private ArrayList<HashMap<String, String>> data;

    public AdapterMenuInternas(Activity a, ArrayList<HashMap<String, String>> menuArray) {
        activity = a;
        data=menuArray;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }



    public View getView(int position, View convertView, ViewGroup parent) {
        View vi=convertView;

        Display display = activity.getWindowManager().getDefaultDisplay();

        int height = display.getHeight();


            if(position==7){
                vi = inflater.inflate(R.layout.menu_items_two, null);

            }else{
                vi = inflater.inflate(R.layout.menu_items, null);

            }




        if( Build.VERSION.SDK_INT >= 9){
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }


        TextView tx_titulo= (TextView)vi.findViewById(R.id.tx_title);
        HashMap<String, String> song = new HashMap<String, String>();
        song = data.get(position);

        String nombre=song.get("name");
        tx_titulo.setText(nombre);


        return vi;
    }


    public static boolean isTablet(Context context) {
        boolean xlarge = ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == 4);
        boolean large = ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE);
        return (xlarge || large);
    }


}