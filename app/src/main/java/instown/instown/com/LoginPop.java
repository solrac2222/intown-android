package instown.instown.com;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;
import com.loopj.android.http.RequestParams;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.CookieStore;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import instown.instown.com.exception.NetworkException;
import instown.instown.com.exception.ParsingException;
import instown.instown.com.exception.ServerException;
import instown.instown.com.exception.TimeOutException;
import instown.instown.com.services.AppAsynchTask;
import io.fabric.sdk.android.Fabric;


public class LoginPop extends ActionBarActivity implements View.OnClickListener {
    ImageView img_fb,img_tw;
    Button bt_log,bt_registro;
    String respuestaWS=null;
    LoginButton loginButton;
    TwitterLoginButton loginButtonTw;
    CallbackManager callbackManager;
    AccessToken accesstoken;
    String non_field,id_usuario;
    TextView tx_forgot;
    EditText ed_usuario,ed_pass;
    final BDInstown logeobd = new BDInstown (LoginPop.this);
    Toolbar toolbar;
    ProgressDialog dialog;
    RequestParams params = new RequestParams();
    String emailW,first_name,username,email,last_name,customer,notifications_enabled,state,email_frequency,gender,city;
    Dialog dialog_pop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        TwitterAuthConfig authConfig = new TwitterAuthConfig(Constants.consumerKey,Constants.consumerSecret);
        Fabric.with(this, new Twitter(authConfig));


        setContentView(R.layout.login_fragment);

        init();
        ortogarClick();

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("siii");
                onBackPressed();
            }
        });


        accesstoken= AccessToken.getCurrentAccessToken();

        loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions("email", "user_likes", "user_friends");

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                String accessToken= loginResult.getAccessToken().getToken();
                System.out.println(accessToken);
                LoginServicioFB(accessToken);

            }

            @Override
            public void onCancel() {
            }

            @Override
            public void onError(FacebookException exception) {
            }
        });


        loginButtonTw = (TwitterLoginButton) findViewById(R.id.twitter_login_button);
        loginButtonTw.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                // Do something with result, which provides a TwitterSession for making API calls

                TwitterSession session = Twitter.getSessionManager().getActiveSession();
                TwitterAuthToken authToken = session.getAuthToken();
                String token = authToken.token;
                String secret = authToken.secret;
                System.out.println("oauth_token "+token);
                System.out.println("oauth_token_secret "+secret);
                LoginServicioTW(token,secret);

            }

            @Override
            public void failure(TwitterException exception) {
                // Do something on failure
            }
        });


        bt_log=(Button)findViewById(R.id.bt_log);
        bt_log.setOnClickListener(this);
        bt_registro=(Button)findViewById(R.id.bt_registro);
        bt_registro.setOnClickListener(this);


    }


    @Override
    public void onResume() {

        super.onResume();
        TimeApp.reposo(LoginPop.this);

    }


    private void ortogarClick() {
        img_fb.setOnClickListener(this);
        img_tw.setOnClickListener(this);
        tx_forgot.setOnClickListener(this);
    }


    private void init() {
        toolbar = (Toolbar)findViewById(R.id.my_awesome_toolbar);
        tx_forgot = (TextView)findViewById(R.id.tx_forgot);
        img_fb=(ImageView) findViewById(R.id.img_fb);
        img_tw=(ImageView) findViewById(R.id.img_tw);
        ed_usuario=(EditText)findViewById(R.id.ed_usuario);
        ed_pass=(EditText)findViewById(R.id.ed_pass);
    }

    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }


    private void login() {
        String usuario=ed_usuario.getText()+"";
        int pasword=ed_pass.length();




        boolean isEmailEntered=true,isPasswordEntered=true;


        if(usuario.equals("")){
            isEmailEntered=false;
        }else if (!isEmailValid(ed_usuario.getText()+"")){
            isEmailEntered=false;
        }

        if(pasword<5){
            isPasswordEntered=false;
        }


        if (isEmailEntered && isPasswordEntered) {

            //String email,String pass,String gender,String city,String send_emails
            LoginServicio(ed_usuario.getText()+"",ed_pass.getText()+"");

        }else if (!isEmailEntered) {
            ed_usuario.setError("Email Invalido");
            ed_usuario.requestFocus();
        }else if (!isPasswordEntered) {
            ed_pass.setError("Contraseña muy corta");
            ed_pass.requestFocus();
        }
    }

    public void  LoginServicio(String email, String pass){
        dialog = new ProgressDialog(LoginPop.this);
        dialog.setMessage("Please Wait...");
        dialog.show();

        params.put("email", email);
        params.put("password", pass);

        System.out.println(email);
        System.out.println(pass);


        AsyncHttpClient myClient = new AsyncHttpClient();
        PersistentCookieStore cookieStore = new PersistentCookieStore(LoginPop.this);
        myClient.setCookieStore(cookieStore);

        myClient.post(Constants.login, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                String txtResponse = String.valueOf(response);
                Log.e("", txtResponse);

                try {
                    JSONObject myObject = response;

                    if (!myObject.isNull("id")){
                        id_usuario=myObject.getString("id");
                        first_name=myObject.getString("first_name");
                        username=myObject.getString("username");
                        emailW=myObject.getString("email");
                        last_name=myObject.getString("last_name");
                        customer=myObject.getString("customer");
                        JSONObject myObjectCustomer = new JSONObject(customer);
                        notifications_enabled=myObjectCustomer.getString("notifications_enabled");
                        state=myObjectCustomer.getString("state");
                        gender=myObjectCustomer.getString("gender");
                        email_frequency=myObjectCustomer.getString("email_frequency");
                        city=myObjectCustomer.getString("city");


                        try {
                            logeobd.abrir();
                            logeobd.borrar();
                            logeobd.crear_entrada("1",id_usuario,username,first_name,last_name,emailW,email_frequency,notifications_enabled,city,state,gender);
                            logeobd.cerrar();
                            finish();
                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                System.out.println(statusCode);
                System.out.println(errorResponse);
                Log.d("THROW", throwable.toString());
                dialog.dismiss();

                new AlertDialog.Builder(LoginPop.this)
                        .setTitle("InTown")
                        .setMessage("Invalid username or password.")
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which){


                            }
                        })
                        .show();
            }
        });
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        loginButtonTw.onActivityResult(requestCode, resultCode, data);
    }


    public void  LoginServicioFB(String tkR){
        dialog = new ProgressDialog(LoginPop.this);
        dialog.setMessage("Please Wait...");
        dialog.show();
        params.put("access_token", tkR);
        AsyncHttpClient myClient = new AsyncHttpClient();
        PersistentCookieStore cookieStore = new PersistentCookieStore(LoginPop.this);
        myClient.setCookieStore(cookieStore);

        myClient.post(Constants.loginFB, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                String txtResponse = String.valueOf(response);
                Log.e("", txtResponse);


                try {
                    JSONObject myObject = new JSONObject(txtResponse);
                    //JSONObject myObject = response;

                    if (!myObject.isNull("non_field_errors")){
                        non_field=myObject.getString("non_field_errors");
                    }

                    if (!myObject.isNull("email")){
                        non_field=myObject.getString("email");
                    }


                    if (!myObject.isNull("id")){
                        id_usuario=myObject.getString("id");
                        first_name=myObject.getString("first_name");
                        username=myObject.getString("username");
                        email=myObject.getString("email");
                        last_name=myObject.getString("last_name");
                        customer=myObject.getString("customer");

                        System.out.println(customer+"...");
                        JSONObject myObjectCustomer = new JSONObject(customer);
                        notifications_enabled=myObjectCustomer.getString("notifications_enabled");
                        state=myObjectCustomer.getString("state");
                        gender=myObjectCustomer.getString("gender");

                        email_frequency=myObjectCustomer.getString("email_frequency");
                        city=myObjectCustomer.getString("city");

                       try {
                            logeobd.abrir();
                            logeobd.borrar();
                            logeobd.crear_entrada("1",id_usuario,username,first_name,last_name,email,email_frequency,notifications_enabled,city,state,gender);
                            logeobd.cerrar();
                            finish();
                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                dialog.hide();

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);


                dialog.hide();
                System.out.println(id_usuario);




            }
        });
    }


    public void  LoginServicioTW(String token,String tokenSecret){
        dialog = new ProgressDialog(LoginPop.this);
        dialog.setMessage("Please Wait...");
        dialog.show();
        params.put("oauth_token", token);
        params.put("oauth_token_secret", tokenSecret);

        AsyncHttpClient myClient = new AsyncHttpClient();
        PersistentCookieStore cookieStore = new PersistentCookieStore(LoginPop.this);
        myClient.setCookieStore(cookieStore);

        myClient.post(Constants.loginTW, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                String txtResponse = String.valueOf(response);
                Log.e("", txtResponse);


                try {
                    JSONObject myObject = new JSONObject(txtResponse);
                    //JSONObject myObject = response;


                    if (!myObject.isNull("id")){
                        id_usuario=myObject.getString("id");
                        first_name=myObject.getString("first_name");
                        username=myObject.getString("username");
                        email=myObject.getString("email");
                        last_name=myObject.getString("last_name");
                        customer=myObject.getString("customer");

                        System.out.println(customer+"...");
                        JSONObject myObjectCustomer = new JSONObject(customer);
                        notifications_enabled=myObjectCustomer.getString("notifications_enabled");
                        state=myObjectCustomer.getString("state");
                        gender=myObjectCustomer.getString("gender");

                        email_frequency=myObjectCustomer.getString("email_frequency");
                        city=myObjectCustomer.getString("city");

                        try {
                            logeobd.abrir();
                            logeobd.borrar();
                            logeobd.crear_entrada("1",id_usuario,username,first_name,last_name,email,email_frequency,notifications_enabled,city,state,gender);
                            logeobd.cerrar();
                            finish();
                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                dialog.hide();

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);


                dialog.hide();
                System.out.println(id_usuario);




            }
        });
    }



    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.img_fb:
                loginButton.performClick();
            break;
            case R.id.img_tw:
                loginButtonTw.performClick();
                break;
            case R.id.bt_log:
                login();
             break;
            case R.id.tx_forgot:
                pop_info();
                break;
            case R.id.bt_registro:
                Intent registro = new Intent(getApplicationContext(), SignFragment.class);
                finish();
                startActivity(registro);
                break;
        }
    }

    private void pop_info() {
        dialog_pop = new Dialog(this);
        dialog_pop.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_pop.setContentView(R.layout.forget_pass);



        final EditText ed_email=(EditText) dialog_pop.findViewById(R.id.ed_email);
        Button bt_send=(Button) dialog_pop.findViewById(R.id.bt_send);
        Button bt_close=(Button) dialog_pop.findViewById(R.id.bt_close);

        bt_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ed_email.getText().toString().equals("")) {
                    ed_email.setError("Email Invalido");
                    ed_email.requestFocus();
                }else{
                    passSend(ed_email.getText().toString());
                }

            }
        });



        bt_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_pop.hide();
            }
        });

        dialog_pop.show();
    }

    private void passSend(String s) {


        dialog = new ProgressDialog(LoginPop.this);
        dialog.setMessage("Please Wait...");
        dialog.show();

        params.put("email", s);
        AsyncHttpClient myClient = new AsyncHttpClient();
        PersistentCookieStore cookieStore = new PersistentCookieStore(LoginPop.this);
        myClient.setCookieStore(cookieStore);

        myClient.post(Constants.pass_reset, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                String txtResponse = String.valueOf(response);
                Log.e("", txtResponse);

                try {
                    JSONObject myObject = response;

                    if (!myObject.isNull("detail")){
                        new AlertDialog.Builder(LoginPop.this)
                                .setTitle("InTown")
                                .setMessage(myObject.getString("detail"))
                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which){
                                        dialog.dismiss();

                                    }
                                })
                                .show();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                System.out.println(statusCode);
                System.out.println(errorResponse);
                Log.d("THROW", throwable.toString());
                dialog.dismiss();



                new AlertDialog.Builder(LoginPop.this)
                        .setTitle("InTown")
                        .setMessage("The e-mail address is not assigned to any user account")
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which){


                            }
                        })
                        .show();
            }
        });


    }
}