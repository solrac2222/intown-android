package instown.instown.com;

/**
 * Created by Francisco on 29/6/15.
 */
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;


import static java.lang.System.currentTimeMillis;
import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class Website extends Activity{
    WebView web;
    String pagina="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.website);

        web = (WebView) this.findViewById(R.id.web);

        // Provide a WebViewClient for your WebView
        web.setWebViewClient(new MyWebViewClient());
        web.getSettings().setJavaScriptEnabled(true);
        web.getSettings().setLoadWithOverviewMode(true);
        web.getSettings().setUseWideViewPort(true);


        web.getSettings().setBuiltInZoomControls(true);
        web.setInitialScale(1);

        if (!getIntent().getStringExtra("pagina").equals(null)) {
            pagina=getIntent().getStringExtra("pagina");
            web.loadUrl(pagina);
        }





    }


    @Override
    protected void onStart()
    {
        super.onStart();

    }

    @Override
    protected void onStop()
    {
        super.onStop();
    }

    @Override
    public void onResume() {

        super.onResume();
        TimeApp.reposo(Website.this);

    }


    @Override
    public void onBackPressed() {
        finish();
    }



    private class MyWebViewClient extends WebViewClient {

        private long loadTime; // Web page loading time

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {

            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);

            // Save start time
            this.loadTime = currentTimeMillis();
            // Show a toast
            Toast.makeText(getApplicationContext(),"Cargando..",Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            // Calculate load time
            this.loadTime = currentTimeMillis() - this.loadTime;
            // Convert milliseconds to date format
            String time = new SimpleDateFormat("mm:ss:SSS", Locale.getDefault())
                    .format(new Date(this.loadTime));
            // Show a toast


        }
    }




}