package instown.instown.com;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import instown.instown.com.exception.NetworkException;
import instown.instown.com.exception.ParsingException;
import instown.instown.com.exception.ServerException;
import instown.instown.com.exception.TimeOutException;
import instown.instown.com.services.AppAsynchTask;


public class BizDetalle extends ActionBarActivity implements View.OnClickListener {
    ImageView banner,img_mapa,fb_ico,tw_ico,p_ico,tw_inst,tw_video;
    TextView tx_phone,tx_hours,tx_titulo,tx_numero,tx_direccion,tx_visit,tx_south,tx_take;
    String inst="",video="",instagram,yelp,bannerT,business_name,address_1,location,id,links,website,facebook,twitter,pinterest;
    double latitude=0,longitude=0;
    String respuestaWS=null,title,image,incentive,has_incentive,mostrar_mapa="si";
    int vacio=0,num_location=0,int_mapa=0;
    RelativeLayout contenedor;
    Toolbar toolbar;
    double latitudI,longitudI;
    GPSTracker gps;
    RelativeLayout cont_incentive;
    final static String TAG_ID = "id";
    final static String TAG_NOMBRE = "nombre";
    final static String TAG_IMAGEN = "imagen";
    final static String TAG_DESCRIPCION = "descripcion";
    final static String TAG_NAME= "name";
    ArrayList<HashMap<String, String>> HomeArray = new ArrayList<HashMap<String, String>>();
    ExploreAdapter explore_adapter;
    GridView gridview_home;
    String direccion_total="",phone="",store_hours="N/A";
    ImageView fb_ico_b,tw_ico_b,p_ico_b,tw_inst_b,tw_video_b,yelpicon,opentable_icon,tripadvisoricon,delivery_icon;
    String opentable,tripadvisor,delivery;
    JSONArray myObjectLocations  = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.biz_detalle);


        init();
        if (!getIntent().getStringExtra("id").equals(null)) {
            id=getIntent().getStringExtra("id");
            bannerT=getIntent().getStringExtra("banner");
            business_name=getIntent().getStringExtra("business_name");
            address_1=getIntent().getStringExtra("address_1");
            location=getIntent().getStringExtra("location");
            bannerT=getIntent().getStringExtra("banner");



            try {
                myObjectLocations = new JSONArray(location);
                num_location=myObjectLocations.length();


                if(num_location==0){
                    tx_visit.setVisibility(View.GONE);
                    img_mapa.setVisibility(View.GONE);
                    tx_take.setVisibility(View.GONE);
                }else if(num_location==1){
                    tx_visit.setVisibility(View.GONE);
                }

                if(myObjectLocations.length()==0){
                    mostrar_mapa="no";
                }


                for(int j = 0; j < myObjectLocations.length(); j++){
                    JSONObject d = myObjectLocations.getJSONObject(j);

                    if(j==0){
                        String[] location1 = d.getString("location").split("\\(");
                        String[] location2 = location1[1].split("\\)");
                        String[] location3 = location2[0].split(" ");
                        System.out.println("locaio "+d.getString("location"));
                        latitude  = Double.parseDouble(location3[1]);
                        longitude = Double.parseDouble(location3[0]);
                        int_mapa=1;
                        direccion_total=d.getString("address_1")+"\n"+d.getString("city")+", "+d.getString("state")+" "+d.getString("zip_code");
                        phone=d.getString("phone");
                        store_hours=d.getString("store_hours");
                        loadClinicMapImage(latitude, longitude);
                    }
                }

                if(myObjectLocations.length()==0){
                    mostrar_mapa="no";
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }



            has_incentive=getIntent().getStringExtra("has_incentive");
            incentive=getIntent().getStringExtra("incentive");
            links=getIntent().getStringExtra("links");
            JSONArray myObjectLinks  = null;
            try {
                myObjectLinks = new JSONArray(links);
                for(int i=0;i<myObjectLinks.length();i++){
                    JSONObject cLink = myObjectLinks.getJSONObject(i);

                    if(cLink.getString("type").toString().equals("0")){
                        website=cLink.getString("url").toString();
                    }else if(cLink.getString("type").toString().equals("1")){
                        facebook=cLink.getString("url").toString();
                    }else if(cLink.getString("type").toString().equals("2")){
                        twitter=cLink.getString("url").toString();
                    }else if(cLink.getString("type").toString().equals("4")){
                        pinterest=cLink.getString("url").toString();
                    }else if(cLink.getString("type").toString().equals("5")){
                        instagram=cLink.getString("url").toString();
                    }else if(cLink.getString("type").toString().equals("6")){
                        video=cLink.getString("url").toString();
                    }else if(cLink.getString("type").toString().equals("6")){
                        yelp=cLink.getString("url").toString();
                    }else if(cLink.getString("type").toString().equals("8")){
                        opentable=cLink.getString("url").toString();
                    }else if(cLink.getString("type").toString().equals("9")){
                        tripadvisor=cLink.getString("url").toString();
                    }else if(cLink.getString("type").toString().equals("10")){
                        delivery=cLink.getString("url").toString();
                    }



                }

            } catch (JSONException e) {
                e.printStackTrace();
            }






        }

        events();
        toolbar = (Toolbar)findViewById(R.id.my_awesome_toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("siii");
                onBackPressed();
            }
        });




        new BizInfo(BizDetalle.this).execute();

    }

    @Override
    public void onResume() {

        super.onResume();
        TimeApp.reposo(BizDetalle.this);

    }


    private void init() {
        banner=(ImageView)findViewById(R.id.banner);
        img_mapa=(ImageView)findViewById(R.id.img_mapa);
        tx_titulo=(TextView)findViewById(R.id.tx_titulo);
        tx_phone=(TextView)findViewById(R.id.tx_phone);
        tx_hours=(TextView)findViewById(R.id.tx_hours);
        tx_take=(TextView)findViewById(R.id.tx_take);
        tx_visit=(TextView)findViewById(R.id.tx_visit);
        tx_direccion=(TextView)findViewById(R.id.tx_direccion);
        tx_south=(TextView)findViewById(R.id.tx_south_two);
        cont_incentive= (RelativeLayout)findViewById(R.id.cont_incentive);
        tx_numero=(TextView)findViewById(R.id.tx_numero);
        fb_ico=(ImageView)findViewById(R.id.fb_ico);
        tw_ico=(ImageView)findViewById(R.id.tw_ico);
        p_ico=(ImageView)findViewById(R.id.p_ico);
        gridview_home=(GridView)findViewById(R.id.gridview_home);
        fb_ico_b = (ImageView)findViewById(R.id.fb_ico_b);
        tw_ico_b = (ImageView)findViewById(R.id.tw_ico_b);
        p_ico_b = (ImageView)findViewById(R.id.p_ico_b);
        tw_inst_b= (ImageView)findViewById(R.id.tw_inst_b);
        tw_video_b= (ImageView)findViewById(R.id.tw_video_b);
        yelpicon= (ImageView)findViewById(R.id.yelpicon);
        opentable_icon= (ImageView)findViewById(R.id.opentable_icon);
        tripadvisoricon= (ImageView)findViewById(R.id.tripadvisoricon);
        delivery_icon= (ImageView)findViewById(R.id.delivery_icon);
    }

    private void events() {
        tx_titulo.setText(business_name);
        tx_direccion.setText(direccion_total);
        tx_visit.setOnClickListener(this);
        tx_phone.setText(phone);

        if(store_hours.toString().equals("N/A")){
            tx_hours.setVisibility(View.INVISIBLE);
        }

        if(phone.toString().equals("null")){
            tx_phone.setVisibility(View.INVISIBLE);
        }


        if (facebook != null && !facebook.isEmpty()) {
            fb_ico_b.setVisibility(View.VISIBLE);
        }

        if (twitter != null && !twitter.isEmpty()) {
            tw_ico_b.setVisibility(View.VISIBLE);
        }

        if (pinterest != null && !pinterest.isEmpty()) {
            p_ico_b.setVisibility(View.VISIBLE);
        }

        if (instagram != null && !instagram.isEmpty()) {
            tw_inst_b.setVisibility(View.VISIBLE);
        }

        if (video != null && !video.isEmpty()) {
            tw_video_b.setVisibility(View.VISIBLE);
        }

        if (yelp != null && !yelp.isEmpty()) {
            yelpicon.setVisibility(View.VISIBLE);
        }

        if (opentable != null && !opentable.isEmpty()) {
            opentable_icon.setVisibility(View.VISIBLE);
        }

        if (tripadvisor != null && !tripadvisor.isEmpty()) {
            tripadvisoricon.setVisibility(View.VISIBLE);
        }

        if (delivery != null && !delivery.isEmpty()) {
            delivery_icon.setVisibility(View.VISIBLE);
        }

        tx_visit.setText(num_location+" Locations");
        tx_south.setOnClickListener(this);
        tx_hours.setOnClickListener(this);
        fb_ico_b.setOnClickListener(this);
        tw_ico_b.setOnClickListener(this);
        p_ico_b.setOnClickListener(this);
        tx_phone.setOnClickListener(this);
        tw_inst_b.setOnClickListener(this);
        tw_video_b.setOnClickListener(this);
        yelpicon.setOnClickListener(this);
        opentable_icon.setOnClickListener(this);
        tripadvisoricon.setOnClickListener(this);
        delivery_icon.setOnClickListener(this);

        if(mostrar_mapa=="si"){
            tx_take.setOnClickListener(this);
        }else{
            tx_take.setVisibility(View.INVISIBLE);
        }


        tx_numero.setText(incentive);

        if(!has_incentive.toString().equals("true")){
            tx_numero.setTextColor(Color.parseColor("#000000"));
        }


        if(!banner.equals(null)){

            Picasso.with(BizDetalle.this)
                    .load(bannerT)
                    .skipMemoryCache()
                    .into(banner);
        }



    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tx_phone:
                Uri numero = Uri.parse( "tel:" + phone);
                Intent intentCall = new Intent(Intent.ACTION_CALL, numero);
                startActivity(intentCall);
                break;
            case R.id.tx_visit:
                change_locations();
                break;
            case R.id.tx_south:
                if (website != null && !website.isEmpty()) {
                    abrir_url(website);
                }else{
                    Toast toast2 =Toast.makeText(BizDetalle.this,"No Disponible", Toast.LENGTH_SHORT);
                    toast2.show();
                }
                break;
            case R.id.fb_ico_b:
                if (facebook != null && !facebook.isEmpty()) {
                    abrir_url(facebook);
                }else{
                    Toast toast2 =Toast.makeText(BizDetalle.this,"No Disponible", Toast.LENGTH_SHORT);
                    toast2.show();
                }
                break;
            case R.id.tw_ico_b:
                if (twitter != null && !twitter.isEmpty()) {
                    abrir_url(twitter);
                }else{
                    Toast toast2 =Toast.makeText(BizDetalle.this,"No Disponible", Toast.LENGTH_SHORT);
                    toast2.show();
                }
                break;
            case R.id.p_ico_b:
                if (pinterest != null && !pinterest.isEmpty()) {
                    abrir_url(pinterest);
                }else{
                    Toast toast2 =Toast.makeText(BizDetalle.this,"No Disponible", Toast.LENGTH_SHORT);
                    toast2.show();
                }
                break;
            case R.id.tw_video_b:
                if (video != null && !video.isEmpty()) {
                    abrir_url(video);
                }else{
                    Toast toast2 =Toast.makeText(BizDetalle.this,"No Disponible", Toast.LENGTH_SHORT);
                    toast2.show();
                }
                break;
            case R.id.yelpicon:
                if (yelp != null && !yelp.isEmpty()) {
                    abrir_url(yelp);
                }else{
                    Toast toast2 =Toast.makeText(BizDetalle.this,"No Disponible", Toast.LENGTH_SHORT);
                    toast2.show();
                }
                break;


            case R.id.delivery_icon:
                if (delivery != null && !delivery.isEmpty()) {
                    abrir_url(delivery);
                }else{
                    Toast toast2 =Toast.makeText(BizDetalle.this,"No Disponible", Toast.LENGTH_SHORT);
                    toast2.show();
                }
                break;
            case R.id.tripadvisoricon:
                if (tripadvisor != null && !tripadvisor.isEmpty()) {
                    abrir_url(tripadvisor);
                }else{
                    Toast toast2 =Toast.makeText(BizDetalle.this,"No Disponible", Toast.LENGTH_SHORT);
                    toast2.show();
                }
                break;
            case R.id.opentable_icon:
                if (opentable != null && !opentable.isEmpty()) {
                    abrir_url(opentable);
                }else{
                    Toast toast2 =Toast.makeText(BizDetalle.this,"No Disponible", Toast.LENGTH_SHORT);
                    toast2.show();
                }
                break;
            case R.id.tw_inst_b:
                if (instagram != null && !instagram.isEmpty()) {
                    abrir_url(instagram);
                }else{
                    Toast toast2 =Toast.makeText(BizDetalle.this,"No Disponible", Toast.LENGTH_SHORT);
                    toast2.show();
                }
                break;

            case R.id.tx_take:
                if(int_mapa==0){
                    Toast toast2 =Toast.makeText(BizDetalle.this,"No Disponible", Toast.LENGTH_SHORT);
                    toast2.show();
                }else{
                    gps = new GPSTracker(BizDetalle.this);

                    if(gps.canGetLocation()){
                        latitudI = gps.getLatitude();
                        longitudI = gps.getLongitude();
                        String uri = "http://maps.google.com/maps?saddr="+latitudI+","+longitudI+"&daddr="+latitude+","+longitude;
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                        intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                        startActivity(intent);
                    }else{
                        gps.showSettingsAlert();
                    }


                }
            case R.id.tx_hours:
                new AlertDialog.Builder(BizDetalle.this)
                        .setTitle("InTown")
                        .setMessage(store_hours)
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which){


                            }
                        })
                        .show();
                break;
        }
    }

    private void abrir_url(String url) {
        Uri uriWeb = Uri.parse(url);
        Intent intentWeb = new Intent(Intent.ACTION_VIEW, uriWeb);
        startActivity(intentWeb);
    }

    public class BizInfo extends AppAsynchTask<Void, String, String> {
        Activity actividad;


        public BizInfo(Activity activity) {
            super(activity);
            // TODO Auto-generated constructor stub
            actividad=activity;
        }



        @Override
        protected String customDoInBackground(Void... params)
                throws NetworkException, ServerException, ParsingException,
                TimeOutException, IOException, JSONException {

            HttpClient httpclient = new DefaultHttpClient();
            HttpGet httppost = new HttpGet(Constants.posts+id+"/posts/");
            System.out.println(Constants.posts+id+"/posts/");
            try {
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);

                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);

                HttpEntity entity = response.getEntity();
                InputStream instream = entity.getContent();
                String result = Constants.convertStreamToString(instream);

                JSONArray resultado = new JSONArray(result);
                vacio=resultado.length();

                System.out.println(resultado);


                for(int i = 0; i < resultado.length(); i++){
                    JSONObject c = resultado.getJSONObject(i);
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put(TAG_ID, c.getString("id"));
                    map.put(TAG_NOMBRE,business_name);
                    map.put(TAG_DESCRIPCION,c.getString("title"));
                    map.put(TAG_IMAGEN,c.getString("image"));
                    HomeArray.add(map);
                }

                respuestaWS="si";


            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
            } catch (IOException e) {
                // TODO Auto-generated catch block
            }catch (JSONException e) {
                e.printStackTrace();
            }

            return respuestaWS;

        }

        @Override
        protected void customOnPostExecute(String result){

            explore_adapter=new ExploreAdapter(BizDetalle.this, HomeArray);
            gridview_home.setAdapter(explore_adapter);

            gridview_home.setOnItemClickListener(new AdapterView.OnItemClickListener(){
                public void onItemClick(AdapterView<?> parent, View v, int position, long ids)
                {


                    HashMap<String, String> song = new HashMap<String, String>();
                    song = HomeArray.get(position);

                    Intent ventana_post= new Intent(BizDetalle.this, InfoFragment.class);
                    new Bundle();
                    ventana_post.putExtra("post",song.get(TAG_ID));
                    startActivity(ventana_post);
                }

            });
            System.out.println("size "+gridview_home.getHeight());
            //setGridViewHeightBasedOnChildren(gridview_home,2);


        if(vacio==0){
            //img_biz.setVisibility(View.GONE);
            //titulo2.setVisibility(View.GONE);
            //descripcion.setVisibility(View.GONE);
        }else{
            /*contenedor.setVisibility(View.VISIBLE);
            Picasso.with(BizDetalle.this)
                    .load(image)
                    .skipMemoryCache()
                    .into(img_biz);

            titulo2.setText(business_name);
            descripcion.setText(title);*/


        }


        }


    }


    public void setGridViewHeightBasedOnChildren(GridView gridView, int columns) {
        ListAdapter listAdapter = gridView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        int items = listAdapter.getCount();
        int rows = 0;

        View listItem = listAdapter.getView(0, null, gridView);
        listItem.measure(0, 0);
        totalHeight = listItem.getMeasuredHeight();

        float x = 1;
        if( items > columns ){
            x = items/columns;
            rows = (int) (x + 1);
            totalHeight *= rows;
        }

        ViewGroup.LayoutParams params = gridView.getLayoutParams();
        params.height = totalHeight;
        gridView.setLayoutParams(params);

    }


    private void loadClinicMapImage(final double latitude, final double longitude){
        // Define the asynchronous task
        AsyncTask<Bitmap, String, Bitmap> loadClinicMapImageTask = new AsyncTask<Bitmap, String, Bitmap>() {
            private ProgressDialog pd = null;

            @Override
            protected void onPreExecute() {

            }

            @Override
            protected Bitmap doInBackground(Bitmap... params) {
                Bitmap bitmap = null;


                bitmap = fetchGoogleMapThumbnail(latitude, longitude, 15, 120, 120);

                // Write Image File
                        /*if (! ImageDownloader.writeImageFile(bitmap, mapImageFile, Bitmap.CompressFormat.PNG)) {
                            bitmap = null;
                        }*/
                return bitmap;
            }

            @Override
            protected void onPostExecute(Bitmap bitmap) {

                if (bitmap == null) {
                    img_mapa.setVisibility(View.INVISIBLE);
                } else {
                    img_mapa.setImageBitmap(bitmap);
                    img_mapa.setVisibility(View.VISIBLE);
                }
            }
        };

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            loadClinicMapImageTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            loadClinicMapImageTask.execute();
        }
    }

    private void openMap() {
        if(latitude!=0){
            String uri = "geo:" + latitude + "," + longitude+"?z="+17;
            System.out.println(uri);
            Intent openMapIntent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri));
            startActivity(openMapIntent);
        }


    }

    public static Bitmap fetchGoogleMapThumbnail(final double latitude,final double longitude,final int zoom,final int width,final int height) {
        // Query
        final String url = "http://maps.google.com/maps/api/staticmap?center=" + latitude + "," + longitude + "&zoom=" + zoom + "&size=" + width + "x" + height + "&markers=color:red%7Clabel:C%7C" + latitude + "," + longitude + "&scale=2&&sensor=false";

        Bitmap bitmap = null;
        HttpClient httpClient = new DefaultHttpClient();
        HttpGet httpRequest = new HttpGet(url);

        InputStream inputStream = null;

        try {
            inputStream = httpClient.execute(httpRequest).getEntity().getContent();
            bitmap = BitmapFactory.decodeStream(inputStream);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }

                if (httpClient != null) {
                    httpClient.getConnectionManager().shutdown();
                }
            }
            catch (Exception e) {
                // Ignored
            }

            // Marked for GC
            httpClient = null;
            httpRequest = null;
            inputStream = null;
        }

        return bitmap;
    }

    private void change_locations() {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(BizDetalle.this);
        builderSingle.setTitle("Select a location");
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(BizDetalle.this,
                android.R.layout.simple_list_item_1);


        for(int j = 0; j < myObjectLocations.length(); j++){
            JSONObject d = null;
            try {
                d = myObjectLocations.getJSONObject(j);
                arrayAdapter.add(d.getString("address_1"));

            } catch (JSONException e) {
                e.printStackTrace();
            }


        }



        builderSingle.setNegativeButton("Cancelar",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        builderSingle.setAdapter(arrayAdapter,
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        rellenar_info(which);
                    }
                });
        builderSingle.show();
    }


    private void rellenar_info(int posicion) {
        JSONObject d = null;
        try {
            d = myObjectLocations.getJSONObject(posicion);
            address_1=d.getString("address_1");

            String[] location1 = d.getString("location").split("\\(");
            String[] location2 = location1[1].split("\\)");
            String[] location3 = location2[0].split(" ");
            System.out.println("locaio "+d.getString("location"));
            latitude  = Double.parseDouble(location3[1]);
            longitude = Double.parseDouble(location3[0]);
            int_mapa=1;
            direccion_total=d.getString("address_1")+"\n"+d.getString("city")+", "+d.getString("state")+" "+d.getString("zip_code");
            phone=d.getString("phone");
            store_hours=d.getString("store_hours");
            loadClinicMapImage(latitude, longitude);


            tx_direccion.setText(direccion_total);
            tx_phone.setText(phone);
            tx_hours.setText(store_hours);

            if(store_hours.toString().equals("N/A")) {
                tx_hours.setVisibility(View.INVISIBLE);
            }

            if(phone.toString().equals("null")) {
                tx_phone.setVisibility(View.INVISIBLE);
            }



        } catch (JSONException e) {
            e.printStackTrace();
        }




    }
}