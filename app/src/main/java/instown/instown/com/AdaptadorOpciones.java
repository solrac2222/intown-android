package instown.instown.com;

/**
 * Created by Francisco on 27/4/15.
 */

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.StrictMode;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

public class AdaptadorOpciones extends BaseAdapter {

    private Activity activity;
    private ArrayList<HashMap<String, String>> data;
    private static LayoutInflater inflater=null;


    public AdaptadorOpciones(Activity a, ArrayList<HashMap<String, String>> d) {
        activity = a;
        data=d;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View vi=convertView;

        Display display = activity.getWindowManager().getDefaultDisplay();

        int height = display.getHeight();
        vi = inflater.inflate(R.layout.list_opciones, null);

        if( Build.VERSION.SDK_INT >= 9){
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        TextView food = (TextView)vi.findViewById(R.id.food);

        HashMap<String, String> song = new HashMap<String, String>();
        song = data.get(position);

        //final String id=song.get(Mapa.TAG_ID);
        food.setText(song.get(Home.TAG_NAME));

        return vi;
    }

}