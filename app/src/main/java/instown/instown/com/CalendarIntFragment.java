package instown.instown.com;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


public class CalendarIntFragment extends ActionBarActivity implements View.OnClickListener {
    private List<CCalendar> list_calendar = new ArrayList<>();
    private RecyclerView rvp;
    String fecha,ciudad,fechaTwo;
    Toolbar toolbar;
    BDInstown logeoBD;
    AlertDialog.Builder builderSingle,builderSingleMiles;
    ArrayAdapter<String> arrayAdapter,arrayAdapterMiles;
    String city_select="",miles="6",name_select="",respuestaWS=null;
    TextView tx_selector,tx_miles;
    ProgressDialog dialog;
    final static String TAG_ID = "id";
    final static String TAG_NOMBRE = "nombre";
    final static String TAG_IMAGEN = "imagen";
    final static String TAG_DESCRIPCION = "descripcion";
    final static String TAG_NAME= "name";
    ArrayList<HashMap<String, String>> OpcionesArray = new ArrayList<HashMap<String, String>>();
    GPSTracker gps;
    static String Latitude="0";
    static String Longitude="0";
    static int pastVisiblesItems;
    static int visibleItemCount;
    static int totalItemCount;
    static RVAdapterCalendar adapter;
    static LinearLayoutManager llm;
    static boolean loadingMore = false;
    int latest=0,previous=0,total_resultado,total_mostrado=0;
    static RelativeLayout content_load;
    int sumarDias=0;
    TextView tx_sorry;
    String[] valuesCiudad = new String[4];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calendar_int_fragment);

        if( Build.VERSION.SDK_INT >= 9){
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        logeoBD = new BDInstown (this);
        gps = new GPSTracker(this);
        toolbar = (Toolbar)findViewById(R.id.my_awesome_toolbar);

        arrayAdapter=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1);
        arrayAdapterMiles=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1);
        content_load=(RelativeLayout)findViewById(R.id.content_load);
        tx_selector=(TextView)findViewById(R.id.tx_selector);
        tx_selector.setOnClickListener(this);
        tx_miles=(TextView) findViewById(R.id.tx_miles);
        tx_miles.setOnClickListener(this);

        if (!getIntent().getStringExtra("fecha").equals(null)) {
            fecha=getIntent().getStringExtra("fecha");
            fechaTwo=getIntent().getStringExtra("fecha");
            city_select=getIntent().getStringExtra("ciudad");
            miles=getIntent().getStringExtra("miles");
            if(miles.toString().equals("6")){
                tx_miles.setText("30 miles");
            }else if(miles.toString().equals("5")){
                tx_miles.setText("20 miles");
            }else if(miles.toString().equals("4")){
                tx_miles.setText("15 miles");
            }else if(miles.toString().equals("3")){
                tx_miles.setText("10 miles");
            }else if(miles.toString().equals("2")){
                tx_miles.setText("5 miles");
            }else if(miles.toString().equals("1")){
                tx_miles.setText("2 miles");
            }else if(miles.toString().equals("0")){
                tx_miles.setText("1 mile");
            }
            Latitude=getIntent().getStringExtra("latitude");
            Longitude=getIntent().getStringExtra("longitude");
            MiTareaBiz(this);
        }


        try {
            logeoBD.abrir();
            try {
                logeoBD.abrir();
                int veces=0;

                Cursor cT =logeoBD.getCity();
                for(cT.moveToFirst();!cT.isAfterLast();cT.moveToNext()){
                    veces++;
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put(TAG_ID,  cT.getString(cT.getColumnIndex("id")));
                    map.put(TAG_NAME,cT.getString(cT.getColumnIndex("name")));
                    if(city_select.toString().equals( cT.getString(cT.getColumnIndex("id")).toString())){
                        tx_selector.setText(cT.getString(cT.getColumnIndex("name")));
                    }

                    if(city_select.toString().equals("0")){
                        tx_selector.setText("Your current location");
                    }

                    OpcionesArray.add(map);
                    arrayAdapter.add(cT.getString(cT.getColumnIndex("name")));

                }


                //inicio
                builderSingle= new AlertDialog.Builder(this);
                builderSingle.setTitle("City List");
                builderSingle.setNegativeButton("Cancelar",
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                builderSingle.setAdapter(arrayAdapter,
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                fecha=fechaTwo;
                                sumarDias=0;
                                if(which==0){
                                    if (gps.canGetLocation()) {
                                        Latitude=gps.getLatitude()+"";
                                        Longitude=gps.getLongitude()+"";
                                        city_select="0";
                                    }
                                    HashMap<String, String> song = new HashMap<String, String>();
                                    song = OpcionesArray.get(which);
                                    tx_selector.setText(song.get(TAG_NAME));
                                    Constants.ActualizarCiudad(CalendarIntFragment.this, city_select);

                                }else{
                                    Latitude="0";
                                    Longitude="0";
                                    HashMap<String, String> song = new HashMap<String, String>();
                                    song = OpcionesArray.get(which);
                                    tx_selector.setText(song.get(TAG_NAME));
                                    city_select=song.get(TAG_ID);
                                    Constants.ActualizarCiudad(CalendarIntFragment.this, city_select);

                                }
                                MiTareaBiz(CalendarIntFragment.this);

                            }
                        });

                //miles
                arrayAdapterMiles.add("1 Mile");
                arrayAdapterMiles.add("2 Miles");
                arrayAdapterMiles.add("5 Miles");
                arrayAdapterMiles.add("10 Miles");
                arrayAdapterMiles.add("15 Miles");
                arrayAdapterMiles.add("20 Miles");
                arrayAdapterMiles.add("30 Miles");



                builderSingleMiles= new AlertDialog.Builder(this);
                builderSingleMiles.setTitle("Miles List");
                builderSingleMiles.setNegativeButton("Cancelar",
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                builderSingleMiles.setAdapter(arrayAdapterMiles,
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                tx_miles.setText(arrayAdapterMiles.getItem(which));
                                miles=which+"";

                                MiTareaBiz(CalendarIntFragment.this);


                            }
                        });




                logeoBD.cerrar();

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            logeoBD.cerrar();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        rvp=(RecyclerView)findViewById(R.id.rvp);
        tx_sorry= (TextView)findViewById(R.id.tx_sorry);
        llm = new LinearLayoutManager(this);
        rvp.setLayoutManager(llm);




    }

    @Override
    public void onResume() {

        super.onResume();
        TimeApp.reposo(CalendarIntFragment.this);

    }


    public void  MiTareaBiz(final Activity activity){
        dialog = new ProgressDialog(activity);
        dialog.setMessage("Please Wait...");
        dialog.show();
        list_calendar.clear();
        AsyncHttpClient myClient = new AsyncHttpClient();
        PersistentCookieStore cookieStore = new PersistentCookieStore(activity);
        myClient.setCookieStore(cookieStore);


        System.out.println(String.format(Constants.caledario_events,fecha,city_select,miles,Latitude,Longitude));
        myClient.get(String.format(Constants.caledario_events,fecha,city_select,miles,Latitude,Longitude), null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                String txtResponse = String.valueOf(response);
                System.out.println(txtResponse);


                try {
                    JSONObject myObject = new JSONObject(txtResponse);


                    // next=myObject.getString("next");
                    JSONArray myObjectitems  = new JSONArray(myObject.getString("results"));
                    list_calendar.add(new CCalendar("0",fecha,"","","",""));

                    for(int i = 0; i < myObjectitems.length(); i++){
                        JSONObject c = myObjectitems.getJSONObject(i);

                        String merchant=c.getString("merchant");
                        String fecha=c.getString("start_date");

                        JSONObject myObjectMerchant  = new JSONObject(merchant);
                        String business_name=myObjectMerchant.getString("business_name");


                        String user_status=c.getString("user_status");
                        JSONObject myObjectUser  = new JSONObject(user_status);
                        String has_reminder=myObjectUser.getString("has_reminder");

                        list_calendar.add(new CCalendar(c.getString("id"),c.getString("title"),business_name,c.getString("image"),SwitchDate(fecha),has_reminder));
                    }
                    if(list_calendar.size()==1){
                        tx_sorry.setVisibility(View.VISIBLE);
                    }else{
                        tx_sorry.setVisibility(View.GONE);
                    }
                    adapter = new RVAdapterCalendar(CalendarIntFragment.this,list_calendar);
                    rvp.setAdapter(adapter);

                    rvp.setOnScrollListener(new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                            visibleItemCount = llm.getChildCount();
                            totalItemCount = llm.getItemCount();
                            pastVisiblesItems = llm.findFirstVisibleItemPosition();

                            if (!loadingMore) {
                                if ( (visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                                        loadingMore = true;

                                        //sumar dia
                                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                                        String dateInString=fecha;
                                        try {

                                            Date date = formatter.parse(dateInString);
                                            fecha=sumarRestarDiasFecha(date,1);
                                            //System.out.println(formatter.format(date));

                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }
                                        MiTareaBizReload();
                                        content_load.setVisibility(View.VISIBLE);
                                }
                            }
                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                dialog.hide();


            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                dialog.hide();

            }
        });
    }


    public void  MiTareaBizReload(){
        AsyncHttpClient myClient = new AsyncHttpClient();
        PersistentCookieStore cookieStore = new PersistentCookieStore(CalendarIntFragment.this);
        myClient.setCookieStore(cookieStore);


        System.out.println(String.format(Constants.caledario_events,fecha,city_select,miles,Latitude,Longitude));
        myClient.get(String.format(Constants.caledario_events,fecha,city_select,miles,Latitude,Longitude), null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                String txtResponse = String.valueOf(response);
                System.out.println(txtResponse);


                try {
                    JSONObject myObject = new JSONObject(txtResponse);


                    // next=myObject.getString("next");
                    JSONArray myObjectitems  = new JSONArray(myObject.getString("results"));
                    list_calendar.add(new CCalendar("0",fecha,"","","",""));
                    for(int i = 0; i < myObjectitems.length(); i++){
                        JSONObject c = myObjectitems.getJSONObject(i);

                        String merchant=c.getString("merchant");
                        String fecha=c.getString("start_date");

                        JSONObject myObjectMerchant  = new JSONObject(merchant);
                        String business_name=myObjectMerchant.getString("business_name");


                        String user_status=c.getString("user_status");
                        JSONObject myObjectUser  = new JSONObject(user_status);
                        String has_reminder=myObjectUser.getString("has_reminder");

                        list_calendar.add(new CCalendar(c.getString("id"),c.getString("title"),business_name,c.getString("image"),SwitchDate(fecha),has_reminder));
                    }



                    loadingMore = false;
                    content_load.setVisibility(View.GONE);
                    adapter.notifyDataSetChanged();



                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                dialog.hide();


            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                dialog.hide();

            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tx_selector:
                builderSingle.show();
                break;
            case R.id.tx_miles:
                builderSingleMiles.show();
                break;
        }
    }

    public static String SwitchDate(String month) {
        String[] datetime = month.toString().split("T");
        String date=datetime[1];
        String[] separedDate = date.toString().split(":");
        String hora=separedDate[0]+":"+separedDate[1]+":00";

        //String s = "12:18:00";
        DateFormat f1 = new SimpleDateFormat("HH:mm:ss"); //HH for hour of the day (0 - 23)
        Date d = null;
        try {
            d = f1.parse(hora);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        DateFormat f2 = new SimpleDateFormat("h:mma");
        // "12:18am"
        return f2.format(d).toLowerCase();
    }

    public static String SwitchDateTop(String dateS) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(dateS);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        String[] datetime = dateS.toString().split("-");
        String day=datetime[2];
        String mo=num_mes(datetime[1]);



        String dayL = (String) android.text.format.DateFormat.format("EEEE", convertedDate);//Thursday
        String stringMonth = (String) android.text.format.DateFormat.format("MMM", convertedDate); //Jun




        // String dayL=new SimpleDateFormat("DD").format(datetime[2]);
        ///*String mo=new SimpleDateFormat("MMM").format(datetime[1]);


        String finalS=dayL+", "+stringMonth+"."+day;
        return finalS;
    }

    public static String num_mes(String mes) {
        String mes_select="";
        switch (mes){
            case "01":
                mes_select="January";
                break;
            case "02":
                mes_select="February";
                break;
            case "03":
                mes_select="March";
                break;
            case "04":
                mes_select="April";
                break;
            case "05":
                mes_select="May";
                break;
            case "06":
                mes_select="June";
                break;
            case "07":
                mes_select="July";
                break;
            case "08":
                mes_select="August";
                break;
            case "09":
                mes_select="September";
                break;
            case "10":
                mes_select="October";
                break;
            case "11":
                mes_select="November";
                break;
            case "12":
                mes_select="December";
                break;
        }
        return mes_select;

    }


    public String sumarRestarDiasFecha(Date fecha, int dias){

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fecha); // Configuramos la fecha que se recibe
        calendar.add(Calendar.DAY_OF_YEAR, dias);  // numero de días a añadir, o restar en caso de días<0

        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
        String cadenaFecha = formato.format(calendar.getTime());



        return cadenaFecha; // Devuelve el objeto Date con los nuevos días añadidos

    }

}