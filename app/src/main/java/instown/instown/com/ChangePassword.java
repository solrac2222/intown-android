package instown.instown.com;

import android.app.ProgressDialog;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class ChangePassword extends ActionBarActivity implements View.OnClickListener{
    EditText ed_new_pass,ed_new_con_pass;
    Button bt_save;
    Toolbar toolbar;
    ProgressDialog dialog;
    RequestParams params = new RequestParams();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.password_change);

        if( Build.VERSION.SDK_INT >= 9){
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        toolbar = (Toolbar)findViewById(R.id.my_awesome_toolbar);


        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        init();
        events();


    }

    private void init() {
        ed_new_pass=(EditText)findViewById(R.id.ed_new_pass);
        ed_new_con_pass=(EditText)findViewById(R.id.ed_new_con_pass);

        bt_save=(Button)findViewById(R.id.bt_save);

    }

    private void events() {
        bt_save.setOnClickListener(this);
    }


    @Override
    public void onResume() {

        super.onResume();
        TimeApp.reposo(ChangePassword.this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.bt_save:
                validar();
                break;
        }
    }

    private void validar() {
        if(ed_new_pass.getText().toString().equals("")){
            ed_new_pass.setError("La contraseña no puede estar vacia");
            ed_new_pass.requestFocus();
            ed_new_pass.setSelected(true);
        }else if(ed_new_con_pass.getText().toString().equals("")){
            ed_new_con_pass.setError("La contraseña no puede estar vacia");
            ed_new_con_pass.requestFocus();
            ed_new_con_pass.setSelected(true);
        }else if(!ed_new_pass.getText().toString().equals(ed_new_con_pass.getText().toString())){
            ed_new_con_pass.setError("Las contraseña no cohinciden");
            ed_new_con_pass.requestFocus();
            ed_new_con_pass.setSelected(true);
        }else{
            UpdateInfoUser();
        }

    }


    public void  UpdateInfoUser(){
        dialog = new ProgressDialog(ChangePassword.this);
        dialog.setMessage("Please Wait...");
        dialog.show();

        params.put("password", ed_new_pass.getText().toString());

        AsyncHttpClient myClient = new AsyncHttpClient();
        PersistentCookieStore cookieStore = new PersistentCookieStore(ChangePassword.this);
        myClient.setCookieStore(cookieStore);

        myClient.put(Constants.user, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                String txtResponse = String.valueOf(response);
                Log.e("", txtResponse);


                Toast toast1 =Toast.makeText(getApplicationContext(),"Contraseña actualizada", Toast.LENGTH_SHORT);
                toast1.setGravity(Gravity.CENTER, 0, 0);
                toast1.show();

                dialog.hide();

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);


                dialog.hide();


            }
        });
    }

}