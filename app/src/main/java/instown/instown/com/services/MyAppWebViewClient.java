package instown.instown.com.services;

import android.content.Intent;
import android.net.Uri;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import instown.instown.com.InfoFragment;

/**
 * Created by Francisco on 27/10/15.
 */
public class MyAppWebViewClient extends WebViewClient {

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
// Url base de la APP (al salir de esta url, abre el navegador) poner como se muestra, sin http://
        System.out.println(url);

       if (url.startsWith("http://www.intown.com/merchant")){
            InfoFragment.bt_see_more.performClick();

        }else{
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            view.getContext().startActivity(intent);

        }
        return true;

    }
}
