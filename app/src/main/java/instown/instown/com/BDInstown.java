package instown.instown.com;

/**
 * Created by Francisco on 17/6/15.
 */
import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class BDInstown {
    public static final String ID_FILA="id";
    public static final String ID_USUARIO="nombre";
    public static final String USERNAME="username";
    public static final String FIRSTNAME="first_name";
    public static final String LASTNAME="last_name";
    public static final String GENDER="gender";
    public static final String EMAIL="email";
    public static final String EMAIL_FREQUENCY="email_frequency";
    public static final String NOTIFICATIONS="notifications_enabled";
    public static final String CITY="city";
    public static final String STATE="state";
    public static final String TIEMPO="tiempo";

    //city
    public static final String ID_NAME="name";
    public static final String ESTADO="estado";



    private static final String N_B ="logeo";
    private static String N_TABLA= "logeo_bd";
    private static String N_TABLA_CITY= "city_bd";
    private static String N_TABLA_REPOSO= "reposo";
    private static String N_TABLA_CITY_ACTUAL= "city_actual_bd";
    private static final int VERSION_BD=16;

    private BDHelper nHelper;
    private final Context nContexto;
    private SQLiteDatabase nBD;


    private static class BDHelper extends SQLiteOpenHelper {

        public BDHelper(Context context) {
            super(context, N_B, null, VERSION_BD);
            // TODO Auto-generated constructor stub


        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            // TODO Auto-generated method stub



            db.execSQL(
                    "CREATE TABLE IF NOT EXISTS " + N_TABLA + "(" +
                            ID_FILA + " TEXT NOT NULL, " +
                            USERNAME + " TEXT, " +
                            FIRSTNAME + " TEXT, " +
                            LASTNAME + " TEXT, " +
                            EMAIL + " TEXT, " +
                            GENDER+ " TEXT, " +
                            EMAIL_FREQUENCY + " TEXT, " +
                            NOTIFICATIONS + " TEXT, " +
                            CITY + " TEXT, " +
                            STATE + " TEXT, " +
                            ID_USUARIO + " TEXT NOT NULL);"
            );


            db.execSQL(
                    "CREATE TABLE IF NOT EXISTS " + N_TABLA_CITY + "(" +
                            ID_FILA + " TEXT NOT NULL, " +
                            ESTADO + " TEXT NOT NULL, " +
                            ID_NAME + " TEXT NOT NULL);"
            );

            db.execSQL(
                    "CREATE TABLE IF NOT EXISTS " + N_TABLA_REPOSO + "(" +
                            ID_FILA + " TEXT NOT NULL, " +
                            TIEMPO + " TEXT NOT NULL);"
            );


        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // TODO Auto-generated method stub
            db.execSQL("DROP TABLE IF EXISTS " + N_TABLA);
            onCreate(db);
        }

    }
    public BDInstown (Context c){
        nContexto=c;
    }
    public BDInstown abrir() throws Exception{
        nHelper = new BDHelper(nContexto);
        nBD= nHelper.getWritableDatabase();
        return this;
    }
    public void cerrar() {
        // TODO Auto-generated method stub
        nHelper.close();
    }



    public long crear_entrada(String id_fila,String id_usuario,String username,String first_name,String last_name,String email,String email_frequency,String notifications_enabled,String city,String state,String gender) {
        // TODO Auto-generated method stub

        ContentValues cv = new ContentValues();
        cv.put(ID_FILA, id_fila);
        cv.put(ID_USUARIO, id_usuario);
        cv.put(USERNAME, username);
        cv.put(FIRSTNAME, first_name);
        cv.put(LASTNAME, last_name);
        cv.put(EMAIL, email);
        cv.put(EMAIL_FREQUENCY, email_frequency);
        cv.put(NOTIFICATIONS, notifications_enabled);
        cv.put(CITY, city);
        cv.put(STATE, state);
        cv.put(GENDER, gender);
        return nBD.insert(N_TABLA, null, cv);

    }

    public Cursor getUser(){
        String[]datos= new String[]{ID_FILA,ID_USUARIO,USERNAME,FIRSTNAME,LASTNAME,EMAIL,EMAIL_FREQUENCY,NOTIFICATIONS,CITY,STATE,GENDER};
        return nBD.query(N_TABLA, datos,null, null, null, null, null);
    }




    public long crear_city(String id_fila,String name,String estado) {
        // TODO Auto-generated method stub

        ContentValues cv = new ContentValues();
        cv.put(ID_FILA, id_fila);
        cv.put(ID_NAME, name);
        cv.put(ESTADO, estado);
        return nBD.insert(N_TABLA_CITY, null, cv);

    }

    public long crear_time(String id_tiempo) {
        // TODO Auto-generated method stub

        ContentValues cv = new ContentValues();
        cv.put(ID_FILA, "1");
        cv.put(TIEMPO, id_tiempo);
        return nBD.insert(N_TABLA_REPOSO, null, cv);

    }

    public void update_time(String valor) {
        // TODO Auto-generated method stub
        ContentValues cvEditor= new ContentValues();
        cvEditor.put(TIEMPO, valor);
        nBD.update(N_TABLA_REPOSO, cvEditor, null, null);
    }

    public int obtener_datos() {
        // TODO Auto-generated method stub
        String[]datos= new String[]{ID_FILA,ID_USUARIO};
        Cursor c =nBD.query(N_TABLA, datos, null, null, null, null, null);
        return c.getCount();
    }


    public void update_email_frequency(String valor) {
        // TODO Auto-generated method stub
        ContentValues cvEditor= new ContentValues();
        cvEditor.put(EMAIL_FREQUENCY, valor);
        nBD.update(N_TABLA, cvEditor, null, null);
    }

    public void update_notifications(String valor) {
        // TODO Auto-generated method stub
        ContentValues cvEditor= new ContentValues();
        cvEditor.put(NOTIFICATIONS, valor);
        nBD.update(N_TABLA, cvEditor, null, null);
    }

    public void update_info(String first_name,String last_name,String city,String gender,String state) {
        // TODO Auto-generated method stub
        ContentValues cvEditor= new ContentValues();
        cvEditor.put(FIRSTNAME, first_name);
        cvEditor.put(LASTNAME, last_name);
        cvEditor.put(CITY, city);
        cvEditor.put(GENDER, gender);
        cvEditor.put(STATE, state);
        nBD.update(N_TABLA, cvEditor, null, null);
    }

    public int obtener_user() {
        // TODO Auto-generated method stub
        String[]datos= new String[]{ID_FILA,ID_USUARIO};
        Cursor c =nBD.query(N_TABLA, datos, null, null, null, null, null);
        return c.getCount();
    }

    public Cursor getCity(){
        String[]datos= new String[]{ID_FILA,ID_NAME};
        return nBD.query(N_TABLA_CITY, datos, null, null, null, null, null);
    }

    public void borrar() throws SQLException {
        // TODO Auto-generated method stub
        nBD.delete(N_TABLA, null, null);
    }

    public void borrar_city() throws SQLException {
        // TODO Auto-generated method stub
        nBD.delete(N_TABLA_CITY, null, null);
    }

    public String GetTime() {
        // TODO Auto-generated method stub
        String[]datos= new String[]{ID_FILA,TIEMPO};
        String value="";
        Cursor cT =nBD.query(N_TABLA_REPOSO, datos, null, null, null, null, null);
        for(cT.moveToFirst();!cT.isAfterLast();cT.moveToNext()){
            value=cT.getString(cT.getColumnIndex(TIEMPO));
        }
        return value;
    }

    public String GetCitySelect() {
        // TODO Auto-generated method stub
        String[]datos= new String[]{ID_FILA,ID_NAME,ESTADO};
        String value="";
        Cursor cT =nBD.query(N_TABLA_CITY, datos, ESTADO+"='1'", null, null, null, null);
        for(cT.moveToFirst();!cT.isAfterLast();cT.moveToNext()){
            value=cT.getString(cT.getColumnIndex(ID_FILA));
        }
        return value;
    }

    public Cursor GetCitySelectAll() {
        // TODO Auto-generated method stub
        String[]datos= new String[]{ID_FILA,ID_NAME,ESTADO};
        String value="";
        Cursor cT =nBD.query(N_TABLA_CITY, datos, ESTADO+"='1'", null, null, null, null);
        return cT;
    }

    public int cityExists() {
        // TODO Auto-generated method stub
        String[]datos= new String[]{ID_FILA};
        Cursor c =nBD.query(N_TABLA_CITY, datos, null, null, null, null, null);
        return c.getCount();
    }

    public void UpdateCity(String idCity) {
        // TODO Auto-generated method stub
        ContentValues cvEditor= new ContentValues();
        cvEditor.put(ESTADO, "0");
        nBD.update(N_TABLA_CITY, cvEditor, null, null);

        //actualizar nueva ciudad
        ContentValues cvEditorActualizar= new ContentValues();
        cvEditorActualizar.put(ESTADO, "1");
        nBD.update(N_TABLA_CITY, cvEditorActualizar, ID_FILA+"="+idCity, null);

    }


    //city actual
    /*public long crear_city_actual(String id_fila) {
        // TODO Auto-generated method stub
        ContentValues cv = new ContentValues();
        cv.put(ID_CITY, id_fila);
        return nBD.insert(N_TABLA_CITY_ACTUAL, null, cv);

    }

    public void borrar_city_actual() throws SQLException {
        // TODO Auto-generated method stub
        nBD.delete(N_TABLA_CITY_ACTUAL, null, null);
    }

    public Cursor getCityActual(){
        String[]datos= new String[]{ID_CITY};
        return nBD.query(N_TABLA_CITY_ACTUAL, datos, null, null, null, null, null);
    }

    public int cityExists() {
        // TODO Auto-generated method stub
        String[]datos= new String[]{ID_CITY};
        Cursor c =nBD.query(N_TABLA_CITY_ACTUAL, datos, null, null, null, null, null);
        return c.getCount();
    }*/

}