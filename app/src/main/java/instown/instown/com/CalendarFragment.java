package instown.instown.com;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.text.format.DateUtils;
import android.text.format.Time;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


public class CalendarFragment extends Fragment implements View.OnClickListener {

    public Calendar month;
    public CalendarAdapter adapter;
    public Handler handler;
    public ArrayList<String> items; // container to store some random calendar items
    GridView gridview;
    TextView title,tx_today,tx_tomorrow,tx_weekend;
    ArrayList<String> fechas_eventos = new ArrayList<String>();
    ScrollView scroll_c;
    ImageView after,next;
    int mesActive=0;

    final static String TAG_ID = "id";
    final static String TAG_NAME= "name";
    final static String TAG_REGION= "region";
    ArrayList<HashMap<String, String>> OpcionesArray = new ArrayList<HashMap<String, String>>();
    TextView tx_selector,tx_event,tx_miles;
    RelativeLayout con_selector;
    String date_now,monthNum;
    BDInstown logeoBD;
    AlertDialog.Builder builderSingle,builderSingleMiles;
    ArrayAdapter<String> arrayAdapter,arrayAdapterMiles;
    String city_select="",miles="6",name_select="",respuestaWS=null;
    GPSTracker gps;
    static String Latitude="0";
    static String Longitude="0";
    String[] valuesCiudad = new String[4];


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.calendar_fragment, container, false);

        if( Build.VERSION.SDK_INT >= 9){
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        gps = new GPSTracker(getActivity());
        logeoBD = new BDInstown (getActivity());
        gridview = (GridView) view.findViewById(R.id.gridview);
        title  = (TextView) view.findViewById(R.id.title);
        tx_event= (TextView) view.findViewById(R.id.tx_event);
        scroll_c= ((ScrollView)view.findViewById(R.id.scroll_c));
        tx_selector=(TextView) view.findViewById(R.id.tx_selector);
        tx_today=(TextView) view.findViewById(R.id.tx_today);
        tx_tomorrow=(TextView) view.findViewById(R.id.tx_tomorrow);
        tx_weekend=(TextView) view.findViewById(R.id.tx_weekend);
        after=(ImageView) view.findViewById(R.id.after);
        next=(ImageView) view.findViewById(R.id.next);
        tx_miles=(TextView)view.findViewById(R.id.tx_miles);
        tx_miles.setOnClickListener(this);

        con_selector =(RelativeLayout) view.findViewById(R.id.con_selector);
        con_selector.setOnClickListener(this);
        tx_today.setOnClickListener(this);
        tx_tomorrow.setOnClickListener(this);
        tx_weekend.setOnClickListener(this);
        after.setOnClickListener(this);
        next.setOnClickListener(this);

        arrayAdapter=new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1);
        arrayAdapterMiles=new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1);

        try {
            logeoBD.abrir();
                int veces=0;

                Cursor cT =logeoBD.getCity();
                for(cT.moveToFirst();!cT.isAfterLast();cT.moveToNext()){
                    veces++;
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put(TAG_ID,  cT.getString(cT.getColumnIndex("id")));
                    map.put(TAG_NAME,cT.getString(cT.getColumnIndex("name")));
                    OpcionesArray.add(map);
                    arrayAdapter.add(cT.getString(cT.getColumnIndex("name")));

                }

                valuesCiudad=Constants.CiudadActual(getActivity());
                Latitude=valuesCiudad[0];
                Longitude=valuesCiudad[1];
                city_select=valuesCiudad[2];
                name_select=valuesCiudad[3];
                tx_selector.setText(name_select);


                //inicio
                builderSingle= new AlertDialog.Builder(getActivity());
                builderSingle.setTitle("City List");
                builderSingle.setNegativeButton("Cancelar",
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                builderSingle.setAdapter(arrayAdapter,
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                if(which==0){
                                    if (gps.canGetLocation()) {
                                        Latitude=gps.getLatitude()+"";
                                        Longitude=gps.getLongitude()+"";
                                        city_select="0";
                                    }
                                    HashMap<String, String> song = new HashMap<String, String>();
                                    song = OpcionesArray.get(which);
                                    tx_selector.setText(song.get(TAG_NAME));
                                    Constants.ActualizarCiudad(getActivity(), city_select);
                                }else{
                                    Latitude="0";
                                    Longitude="0";
                                    HashMap<String, String> song = new HashMap<String, String>();
                                    song = OpcionesArray.get(which);
                                    tx_selector.setText(song.get(TAG_NAME));
                                    city_select=song.get(TAG_ID);
                                    Constants.ActualizarCiudad(getActivity(),city_select);
                                }


                            }
                        });

                //miles
                arrayAdapterMiles.add("1 Mile");
                arrayAdapterMiles.add("2 Miles");
                arrayAdapterMiles.add("5 Miles");
                arrayAdapterMiles.add("10 Miles");
                arrayAdapterMiles.add("15 Miles");
                arrayAdapterMiles.add("20 Miles");
                arrayAdapterMiles.add("30 Miles");



                builderSingleMiles= new AlertDialog.Builder(getActivity());
                builderSingleMiles.setTitle("Miles List");
                builderSingleMiles.setNegativeButton("Cancelar",
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                builderSingleMiles.setAdapter(arrayAdapterMiles,
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                tx_miles.setText(arrayAdapterMiles.getItem(which));
                                miles=which+"";
                            }
                        });




                logeoBD.cerrar();

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }



        new MiTareaCalendario().execute();

        return view;
    }

    @Override
    public void onResume() {

        super.onResume();
        TimeApp.reposo(getActivity());

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.con_selector:
                builderSingle.show();
                break;
            case R.id.tx_today:
                Date d=new Date();

                SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
                String cadenaFecha = formato.format(d);

                Intent VentanaEventos = new Intent(getActivity(), CalendarIntFragment.class);
                VentanaEventos.putExtra("fecha", cadenaFecha);
                VentanaEventos.putExtra("ciudad",city_select);
                VentanaEventos.putExtra("miles", miles);
                VentanaEventos.putExtra("latitude", Latitude);
                VentanaEventos.putExtra("longitude", Longitude);
                startActivity(VentanaEventos);


            break;
            case R.id.tx_tomorrow:
                Date f=new Date();
                String fecha_manana=sumarRestarDiasFecha(f,1);

                Intent VentanaEventos2 = new Intent(getActivity(), CalendarIntFragment.class);
                VentanaEventos2.putExtra("fecha", fecha_manana);
                VentanaEventos2.putExtra("ciudad",city_select);
                VentanaEventos2.putExtra("miles", miles);
                VentanaEventos2.putExtra("latitude", Latitude);
                VentanaEventos2.putExtra("longitude", Longitude);
                startActivity(VentanaEventos2);
                break;
            case R.id.tx_weekend:

                Date dW=new Date();
                String dayL = (String) android.text.format.DateFormat.format("EEEE", dW);//Thursday
                String fecha_siguiente="";





                Calendar c = Calendar.getInstance();
                c.setTime(dW);
                int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
                System.out.println(dayOfWeek+"..");


                if(dayOfWeek==1){
                    fecha_siguiente=sumarRestarDiasFecha(dW,5);
                }else if(dayOfWeek==2){
                    fecha_siguiente=sumarRestarDiasFecha(dW,4);
                }else if(dayOfWeek==3){
                    fecha_siguiente=sumarRestarDiasFecha(dW,3);
                }else if(dayOfWeek==4){
                    fecha_siguiente=sumarRestarDiasFecha(dW,2);
                }else if(dayOfWeek==5){
                    fecha_siguiente=sumarRestarDiasFecha(dW,1);
                }else if(dayOfWeek==6){
                    fecha_siguiente=sumarRestarDiasFecha(dW,1);
                }else if(dayOfWeek==7){
                    fecha_siguiente=sumarRestarDiasFecha(dW,6);
                }

                System.out.println(dayL.toString());
                System.out.println(fecha_siguiente+"");
                System.out.println(city_select);
                System.out.println(miles);
                Intent VentanaEventosW = new Intent(getActivity(), CalendarIntFragment.class);
                VentanaEventosW.putExtra("fecha", fecha_siguiente);
                VentanaEventosW.putExtra("ciudad",city_select);
                VentanaEventosW.putExtra("miles", miles);
                VentanaEventosW.putExtra("latitude", Latitude);
                VentanaEventosW.putExtra("longitude", Longitude);
                startActivity(VentanaEventosW);



                break;
            case R.id.next:
                mesActive++;
                if(mesActive==0){
                    after.setVisibility(View.INVISIBLE);
                }else{
                    after.setVisibility(View.VISIBLE);
                }

                if(month.get(Calendar.MONTH)== month.getActualMaximum(Calendar.MONTH)) {
                    month.set((month.get(Calendar.YEAR)+1),month.getActualMinimum(Calendar.MONTH),1);
                } else {
                    month.set(Calendar.MONTH,month.get(Calendar.MONTH)+1);
                }
                refreshCalendar("next");

                break;
            case R.id.after:
                mesActive--;
                if(mesActive==0){
                    after.setVisibility(View.INVISIBLE);
                }else{
                    after.setVisibility(View.VISIBLE);
                }

                if(month.get(Calendar.MONTH)== month.getActualMinimum(Calendar.MONTH)) {
                    month.set((month.get(Calendar.YEAR)-1),month.getActualMaximum(Calendar.MONTH),1);
                } else {
                    month.set(Calendar.MONTH,month.get(Calendar.MONTH)-1);
                }
                refreshCalendar("after");
                break;
            case R.id.tx_miles:
                builderSingleMiles.show();
                break;
        }
    }


    public String sumarRestarDiasFecha(Date fecha, int dias){

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fecha); // Configuramos la fecha que se recibe
        calendar.add(Calendar.DAY_OF_YEAR, dias);  // numero de días a añadir, o restar en caso de días<0

        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
        String cadenaFecha = formato.format(calendar.getTime());



        return cadenaFecha; // Devuelve el objeto Date con los nuevos días añadidos

    }


    private class MiTareaCalendario extends AsyncTask<Void, Integer, Boolean> {
        @Override
        protected Boolean doInBackground(Void... params) {



            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {

            month = Calendar.getInstance();
            //fecha_server="2014-1-2";
            int mes_final=0,year=0;

            Time today=new Time(Time.getCurrentTimezone());
            today.setToNow();
            month = Calendar.getInstance();
            date_now=""+today.year+"-"+(today.month+1)+"-"+today.monthDay;
            String[] dateArr = date_now.split("-");
            if(Integer.parseInt(dateArr[1])==1){
                mes_final=12;
                year=Integer.parseInt(dateArr[0])-1;
            }else{
                mes_final=Integer.parseInt(dateArr[1])-1;
                year=Integer.parseInt(dateArr[0]);
            }
            month.set(year,mes_final,Integer.parseInt(dateArr[0]));
            monthNum= String.valueOf((today.month+1));

            if(monthNum.length()==1){
                monthNum="0"+monthNum;
            }

            title.setText(Constants.OnlyMonth(monthNum));



            items = new ArrayList<String>();
            adapter = new CalendarAdapter(getActivity(), month);

            gridview.setAdapter(adapter);

            handler = new Handler();
            handler.post(calendarUpdater);





            scroll_c.fullScroll(ScrollView.FOCUS_UP);
            gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View v, int position, long id) {

                    TextView date = (TextView)v.findViewById(R.id.date);
                    if(date instanceof TextView && !date.getText().equals("")) {

                        String day = date.getText().toString();
                        if (day.length() == 1) {
                            day = "0" + day;
                        }
                        // return chosen date as string format
                        //setResult(RESULT_OK, intent);
                        String fecha_click = android.text.format.DateFormat.format("yyyy-MM", month) + "-" + day;
                        System.out.println(fecha_click);
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                        Date dCom=new Date();
                        String dayCom = (String) android.text.format.DateFormat.format("yyyy-MM-dd", dCom);//Thursday

                        Date strDate = null;
                        try {
                            strDate = sdf.parse(fecha_click);
                            if (!new Date().after(strDate)) {
                                Intent VentanaEventos = new Intent(getActivity(), CalendarIntFragment.class);
                                VentanaEventos.putExtra("fecha", fecha_click);
                                VentanaEventos.putExtra("ciudad",city_select);
                                VentanaEventos.putExtra("miles", miles);
                                VentanaEventos.putExtra("latitude", Latitude);
                                VentanaEventos.putExtra("longitude", Longitude);
                                startActivity(VentanaEventos);
                            }else if (fecha_click.equals(dayCom)) {
                                Intent VentanaEventos = new Intent(getActivity(), CalendarIntFragment.class);
                                VentanaEventos.putExtra("fecha", fecha_click);
                                VentanaEventos.putExtra("ciudad",city_select);
                                VentanaEventos.putExtra("miles", miles);
                                VentanaEventos.putExtra("latitude", Latitude);
                                VentanaEventos.putExtra("longitude", Longitude);
                                startActivity(VentanaEventos);
                            }
                            System.out.println(strDate);
                            System.out.println(dayCom);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }



                        //}

                    }

                }
            });

           // new LlenarOpciones().execute();
        }

    }

    public void refreshCalendar(String value)
    {

        /*if(value.toString().equals("next")){
            monthNum= String.valueOf((today.month+2));
            if(monthNum.length()==1){
                monthNum="0"+monthNum;
            }
        }else{
            monthNum= String.valueOf((today.month+1));
            if(monthNum.length()==1){
                monthNum="0"+monthNum;
            }
        }

*/
        adapter.refreshDays();
        adapter.notifyDataSetChanged();
        handler.post(calendarUpdater); // generate some random calendar items

        title.setText(android.text.format.DateFormat.format("MMMM yyyy", month));
    }

    public Runnable calendarUpdater = new Runnable() {

        @Override
        public void run() {
            items.clear();
            // format random values. You can implement a dedicated class to provide real values
            for(int i=0;i<=31;i++) {

                int mes_select=month.get(Calendar.MONTH)+1;
                int year_select=month.get(Calendar.YEAR);


                String mes_select_string=mes_select+"";
                String dia_select_string=i+"";


                if(mes_select_string.length()==1){
                    mes_select_string="0"+mes_select_string;
                }



                if(dia_select_string.length()==1){
                    dia_select_string="0"+dia_select_string;
                }


                // System.out.println(year_select+"-"+mes_select_string+"-"+dia_select_string);

                if(fechas_eventos.contains(year_select+"-"+mes_select_string+"-"+dia_select_string)){
                    System.out.println(year_select+"-"+mes_select+"-"+dia_select_string);
                    items.add(Integer.toString(i));
                }

                //	if(r.nextInt(10)>6){}
            }

            adapter.setItems(items);
            adapter.notifyDataSetChanged();
            scroll_c.fullScroll(ScrollView.FOCUS_UP);
        }
    };


}