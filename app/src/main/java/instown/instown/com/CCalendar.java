package instown.instown.com;

class CCalendar {
    String id;
    String name;
    String descri_small;
    String photoId;
    String fecha;
    String has_reminder;

    CCalendar(String id, String name ,String descri_small, String photoId, String fecha, String has_reminder) {
        this.name = name;
        this.id = id;
        this.descri_small = descri_small;
        this.photoId = photoId;
        this.fecha = fecha;
        this.has_reminder = has_reminder;
    }
}