package instown.instown.com;

import android.app.ProgressDialog;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;


public class UpdateEmails extends ActionBarActivity implements View.OnClickListener{
    ImageView cir_vacio,cir_vacio_two,cir_vacio_three,cir_vacio_four;
    Button bt_save;
    Toolbar toolbar;
    BDInstown logeoBD;
    String email_frequency;
    ProgressDialog dialog;
    RequestParams params = new RequestParams();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.update_emails);

        if( Build.VERSION.SDK_INT >= 9){
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        toolbar = (Toolbar)findViewById(R.id.my_awesome_toolbar);


        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        logeoBD = new BDInstown (UpdateEmails.this);
        try {
            logeoBD.abrir();
            Cursor Cusuarios=logeoBD.getUser();
            for(Cusuarios.moveToFirst();!Cusuarios.isAfterLast();Cusuarios.moveToNext()){
                email_frequency=Cusuarios.getString(Cusuarios.getColumnIndex(BDInstown.EMAIL_FREQUENCY));
            }
            logeoBD.cerrar();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        init();
        events();


    }

    @Override
    public void onResume() {

        super.onResume();
        TimeApp.reposo(UpdateEmails.this);

    }

    private void init() {
        cir_vacio=(ImageView)findViewById(R.id.cir_vacio);
        cir_vacio_two=(ImageView)findViewById(R.id.cir_vacio_two);
        cir_vacio_three=(ImageView)findViewById(R.id.cir_vacio_three);
        cir_vacio_four=(ImageView)findViewById(R.id.cir_vacio_four);
        bt_save=(Button)findViewById(R.id.bt_save);
        if(email_frequency.toString().equals("0")){
            cir_vacio.setImageResource(R.drawable.cir_lleno);
        }else if(email_frequency.toString().equals("1")){
            cir_vacio_two.setImageResource(R.drawable.cir_lleno);
        }else if(email_frequency.toString().equals("2")){
            cir_vacio_three.setImageResource(R.drawable.cir_lleno);
        }else if(email_frequency.toString().equals("3")){
            cir_vacio_four.setImageResource(R.drawable.cir_lleno);
        }

    }

    private void events() {
        cir_vacio.setOnClickListener(this);
        cir_vacio_two.setOnClickListener(this);
        cir_vacio_three.setOnClickListener(this);
        cir_vacio_four.setOnClickListener(this);
        bt_save.setOnClickListener(this);
    }



    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.cir_vacio:
                cir_vacio.setImageResource(R.drawable.cir_lleno);
                cir_vacio_two.setImageResource(R.drawable.cir_vacio);
                cir_vacio_three.setImageResource(R.drawable.cir_vacio);
                cir_vacio_four.setImageResource(R.drawable.cir_vacio);
                email_frequency="0";
                break;
            case R.id.cir_vacio_two:
                cir_vacio.setImageResource(R.drawable.cir_vacio);
                cir_vacio_two.setImageResource(R.drawable.cir_lleno);
                cir_vacio_three.setImageResource(R.drawable.cir_vacio);
                cir_vacio_four.setImageResource(R.drawable.cir_vacio);
                email_frequency="1";
                break;
            case R.id.cir_vacio_three:
                cir_vacio.setImageResource(R.drawable.cir_vacio);
                cir_vacio_two.setImageResource(R.drawable.cir_vacio);
                cir_vacio_three.setImageResource(R.drawable.cir_lleno);
                cir_vacio_four.setImageResource(R.drawable.cir_vacio);
                email_frequency="2";
                break;
            case R.id.cir_vacio_four:
                cir_vacio.setImageResource(R.drawable.cir_vacio);
                cir_vacio_two.setImageResource(R.drawable.cir_vacio);
                cir_vacio_three.setImageResource(R.drawable.cir_vacio);
                cir_vacio_four.setImageResource(R.drawable.cir_lleno);
                email_frequency="3";
                break;
            case R.id.bt_save:
                UpdateInfoUser();
                break;
        }
    }


    public void  UpdateInfoUser(){
        dialog = new ProgressDialog(UpdateEmails.this);
        dialog.setMessage("Please Wait...");
        dialog.show();
        params.put("email_frequency",email_frequency);


        AsyncHttpClient myClient = new AsyncHttpClient();
        PersistentCookieStore cookieStore = new PersistentCookieStore(UpdateEmails.this);
        myClient.setCookieStore(cookieStore);

        myClient.put(Constants.user, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                String txtResponse = String.valueOf(response);
                Log.e("", txtResponse);


                Toast toast1 =Toast.makeText(getApplicationContext(),"Datos actualizados", Toast.LENGTH_SHORT);
                toast1.setGravity(Gravity.CENTER, 0, 0);
                toast1.show();
                try {
                    logeoBD.abrir();
                    logeoBD.update_email_frequency(email_frequency);
                    logeoBD.cerrar();

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }



                dialog.hide();

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);


                dialog.hide();


            }
        });
    }



}