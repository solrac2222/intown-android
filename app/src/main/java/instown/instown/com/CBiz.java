package instown.instown.com;

class CBiz {
    String id;
    String name;
    String numero;
    String descri_small;
    String photoId;
    String localizacion;
    String banner;
    String is_following;
    String links;
    String has_incentive;

    CBiz(String id, String name, String num, String descri_small, String photoId,String localizacion,String banner,String is_following,String links,String has_incentive) {
        this.name = name;
        this.id = id;
        this.descri_small = descri_small;
        this.numero = num;
        this.photoId = photoId;
        this.localizacion = localizacion;
        this.banner = banner;
        this.is_following=is_following;
        this.links=links;
        this.has_incentive=has_incentive;
    }
}