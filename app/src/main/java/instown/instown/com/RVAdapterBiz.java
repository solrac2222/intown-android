package instown.instown.com;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;
import com.squareup.picasso.Picasso;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import instown.instown.com.exception.NetworkException;
import instown.instown.com.exception.ParsingException;
import instown.instown.com.exception.ServerException;
import instown.instown.com.exception.TimeOutException;
import instown.instown.com.services.AppAsynchTask;

public class RVAdapterBiz extends RecyclerView.Adapter<RVAdapterBiz.PersonViewHolder> {
   Activity tabOvasN;
    String respuestaWS=null,detalles="";
    ProgressDialog dialog;
    Dialog dialog_pop;
    BDInstown logeoBD;

    public class PersonViewHolder extends RecyclerView.ViewHolder {

        CardView cv;
        TextView tx_i,tx_num,tx_d;
        ImageView img_check;


        PersonViewHolder(View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.cv);
            tx_num = (TextView)itemView.findViewById(R.id.tx_num);
            tx_i = (TextView)itemView.findViewById(R.id.tx_i);
            tx_d = (TextView)itemView.findViewById(R.id.tx_d);
            img_check = (ImageView)itemView.findViewById(R.id.img_check);





            tx_i.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int posicion=getPosition();

                    Intent VentanaDetalle = new Intent(v.getContext(),BizDetalle.class);
                    VentanaDetalle.putExtra("id",persons.get(posicion).id);
                    VentanaDetalle.putExtra("banner",persons.get(posicion).banner);
                    VentanaDetalle.putExtra("business_name",persons.get(posicion).name);
                    VentanaDetalle.putExtra("address_1",persons.get(posicion).descri_small);
                    VentanaDetalle.putExtra("location",persons.get(posicion).localizacion);
                    VentanaDetalle.putExtra("links",persons.get(posicion).links);
                    VentanaDetalle.putExtra("incentive",persons.get(posicion).descri_small);
                    VentanaDetalle.putExtra("has_incentive",persons.get(posicion).has_incentive);
                    v.getContext().startActivity(VentanaDetalle);

                }
            });

            tx_d.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int posicion=getPosition();

                    Intent VentanaDetalle = new Intent(v.getContext(),BizDetalle.class);
                    VentanaDetalle.putExtra("id",persons.get(posicion).id);
                    VentanaDetalle.putExtra("banner",persons.get(posicion).banner);
                    VentanaDetalle.putExtra("business_name",persons.get(posicion).name);
                    VentanaDetalle.putExtra("address_1",persons.get(posicion).descri_small);
                    VentanaDetalle.putExtra("location",persons.get(posicion).localizacion);
                    VentanaDetalle.putExtra("links",persons.get(posicion).links);
                    VentanaDetalle.putExtra("incentive",persons.get(posicion).descri_small);
                    VentanaDetalle.putExtra("has_incentive",persons.get(posicion).has_incentive);
                    v.getContext().startActivity(VentanaDetalle);

                }
            });

            img_check.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int posicion=getPosition();
                    logeoBD = new BDInstown (tabOvasN);

                    try {
                        logeoBD.abrir();
                        if(logeoBD.obtener_datos()>0){
                            if(img_check.getTag().toString().equals("no_follow")){
                                BizFollow(tabOvasN,persons.get(posicion).id+"",img_check,tx_num);
                            }else{
                                BizUnFollow(tabOvasN,persons.get(posicion).id+"",img_check,tx_num);
                            }
                        }else{
                            Intent menu = new Intent(tabOvasN, LoginPop.class);
                            tabOvasN.startActivity(menu);
                        }
                        logeoBD.cerrar();

                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }




                }
            });



        }



    }

    static List<CBiz> persons;

    RVAdapterBiz(Activity tabOvas, List<CBiz> persons){
        tabOvasN=tabOvas;
        this.persons = persons;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_biz, viewGroup, false);
        PersonViewHolder pvh = new PersonViewHolder(v);
        return pvh;
    }

    TextView tx_i,tx_num,tx_d;
    ImageView img_check;

    @Override
    public void onBindViewHolder(PersonViewHolder personViewHolder, int i) {
        personViewHolder.tx_i.setText(persons.get(i).name);
        personViewHolder.tx_d.setText(persons.get(i).descri_small);
        personViewHolder.tx_num.setText(persons.get(i).numero);

        if(persons.get(i).is_following=="false"){
            personViewHolder.img_check.setImageResource(R.drawable.checkoff);
            personViewHolder.img_check.setTag("no_follow");
        }else{
            personViewHolder.img_check.setImageResource(R.drawable.checkon);
            personViewHolder.img_check.setTag("follow");
        }

        if(persons.get(i).has_incentive=="true"){
            personViewHolder.tx_d.setTextColor(Color.parseColor("#ec3013"));
        }else{
            personViewHolder.tx_d.setTextColor(Color.parseColor("#000000"));
        }



    }





    @Override
    public int getItemCount() {
        return persons.size();
    }


    public void  BizFollow(final Activity activity, String id_info, final ImageView imgN, final TextView tx_numN){
        dialog = new ProgressDialog(activity);
        dialog.setMessage("Please Wait...");
        dialog.show();
        AsyncHttpClient myClient = new AsyncHttpClient();
        PersistentCookieStore cookieStore = new PersistentCookieStore(activity);
        myClient.setCookieStore(cookieStore);

        myClient.post(Constants.seguir_posts+id_info+"/follow/", null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                String txtResponse = String.valueOf(response);
                Log.e("", txtResponse);


                try {
                    JSONObject myObject = new JSONObject(txtResponse);
                    //JSONObject myObject = response;


                    if (!myObject.isNull("follower_count")){
                        tx_numN.setText(myObject.getString("follower_count").toString());
                        imgN.setImageResource(R.drawable.checkon);
                        imgN.setTag("follow");
                        pop_mensaje();
                    }



                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                dialog.hide();

                detalles="";

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                dialog.hide();

            }
        });
    }

    public void  BizUnFollow(final Activity activity, String id_info, final ImageView imgN, final TextView tx_numN){
        dialog = new ProgressDialog(activity);
        dialog.setMessage("Please Wait...");
        dialog.show();
        AsyncHttpClient myClient = new AsyncHttpClient();
        PersistentCookieStore cookieStore = new PersistentCookieStore(activity);
        myClient.setCookieStore(cookieStore);

        myClient.post(Constants.seguir_posts+id_info+"/unfollow/", null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                String txtResponse = String.valueOf(response);
                Log.e("", txtResponse);


                try {
                    JSONObject myObject = new JSONObject(txtResponse);
                    //JSONObject myObject = response;


                    if (!myObject.isNull("follower_count")){
                        tx_numN.setText(myObject.getString("follower_count").toString());
                        imgN.setImageResource(R.drawable.checkoff);
                        imgN.setTag("no_follow");
                    }



                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                dialog.hide();

                detalles="";

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                dialog.hide();

            }
        });
    }


    private void pop_mensaje() {
        dialog_pop = new Dialog(tabOvasN);
        dialog_pop.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_pop.setContentView(R.layout.pop_biz);
        String titulo="Thanks for \"Cheking Us out!\"";
        String description="Our events and happenings will now post on \"My Page\" so you can stay In The Know!";

        Button bt_saved=(Button) dialog_pop.findViewById(R.id.bt_saved);
        Button bt_close=(Button) dialog_pop.findViewById(R.id.bt_close);
        TextView textView3=(TextView) dialog_pop.findViewById(R.id.textView3);
        TextView textView4=(TextView) dialog_pop.findViewById(R.id.textView4);
        textView4.setText(description);
        textView3.setText(titulo);

        bt_saved.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_pop.hide();
                Home.selectItem(7);

            }
        });



        bt_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_pop.hide();
            }
        });

        dialog_pop.show();
    }




}
