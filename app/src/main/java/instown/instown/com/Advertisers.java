package instown.instown.com;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Advertisers extends ActionBarActivity {
    BDInstown logeoBD;
    int id_usuario;
    ListView buscador_list;
    AdaptadorOpciones adapter_opciones;
    ArrayList<HashMap<String, String>> ListOpciones = new ArrayList<HashMap<String, String>>();
    static String TAG_ID= "id";
    static String TAG_NAME = "name";
    Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.advertisers);

        if( Build.VERSION.SDK_INT >= 9){
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        toolbar = (Toolbar)findViewById(R.id.my_awesome_toolbar);


        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });



        buscador_list=(ListView)findViewById(R.id.buscador_list);

        HashMap<String, String> map = new HashMap<String, String>();
        map.put(TAG_ID, "1");
        map.put(TAG_NAME, "LOG IN");
        ListOpciones.add(map);

        HashMap<String, String> mapC = new HashMap<String, String>();
        mapC.put(TAG_ID, "2");
        mapC.put(TAG_NAME,"SIGN UP");
        ListOpciones.add(mapC);

        HashMap<String, String> mapE = new HashMap<String, String>();
        mapE.put(TAG_ID, "3");
        mapE.put(TAG_NAME,"ADVERTISE WITH US/VIDEO");
        ListOpciones.add(mapE);

        HashMap<String, String> mapN = new HashMap<String, String>();
        mapN.put(TAG_ID, "4");
        mapN.put(TAG_NAME,"TRY US-CREATE A SAMPLE ACCOUNT");
        ListOpciones.add(mapN);

        HashMap<String, String> mapNon = new HashMap<String, String>();
        mapNon.put(TAG_ID, "5");
        mapNon.put(TAG_NAME,"NON PROFIT SIGN UP");
        ListOpciones.add(mapNon);

        HashMap<String, String> mapHo = new HashMap<String, String>();
        mapHo.put(TAG_ID, "6");
        mapHo.put(TAG_NAME,"HOSPITALITY");
        ListOpciones.add(mapHo);

        HashMap<String, String> mapFa = new HashMap<String, String>();
        mapFa.put(TAG_ID, "7");
        mapFa.put(TAG_NAME,"FAQS");
        ListOpciones.add(mapFa);

        HashMap<String, String> mapCo = new HashMap<String, String>();
        mapCo.put(TAG_ID, "8");
        mapCo.put(TAG_NAME,"CONTACT US");
        ListOpciones.add(mapCo);

        adapter_opciones=new AdaptadorOpciones(Advertisers.this,ListOpciones);
        buscador_list.setAdapter(adapter_opciones);

        buscador_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Context context = view.getContext();

                if(position==0){
                    Intent ventana_web = new Intent(Advertisers.this, WebsiteMerchant.class);
                    ventana_web.putExtra("pagina", Constants.merchantLog);
                    startActivity(ventana_web);
                }else if(position==1){
                    Intent ventana_web = new Intent(Advertisers.this, WebsiteMerchant.class);
                    ventana_web.putExtra("pagina", Constants.merchantSign);
                    startActivity(ventana_web);
                }else if(position==2){
                    Intent ventana_web = new Intent(Advertisers.this, Prueba.class);
                    startActivity(ventana_web);
                }else if(position==3){
                    Intent ventana_web = new Intent(Advertisers.this, WebsiteMerchant.class);
                    ventana_web.putExtra("pagina", Constants.merchantAd);
                    startActivity(ventana_web);
                }else if(position==4){
                    Intent ventana_web = new Intent(Advertisers.this, WebsiteMerchant.class);
                    ventana_web.putExtra("pagina", Constants.merchantNon);
                    startActivity(ventana_web);
                }else if(position==5){
                    Intent ventana_web = new Intent(Advertisers.this, WebsiteMerchant.class);
                    ventana_web.putExtra("pagina", Constants.merchantHos);
                    startActivity(ventana_web);
                }else if(position==6){
                    Intent ventana_web = new Intent(Advertisers.this, WebsiteMerchant.class);
                    ventana_web.putExtra("pagina", Constants.why_be);
                    startActivity(ventana_web);
                }else if(position==7){
                    String[] to = { "team@intown.com"};
                    String[] cc = { "" };
                    enviar(to, cc, "Contáctenos", "");
                }


            }


        });


    }



    @Override
    public void onResume() {

        super.onResume();
        TimeApp.reposo(Advertisers.this);

    }

    private void enviar(String[] to, String[] cc,String asunto, String mensaje) {
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setData(Uri.parse("mailto:"));
        //String[] to = direccionesEmail;
        //String[] cc = copias;
        emailIntent.putExtra(Intent.EXTRA_EMAIL, to);
        emailIntent.putExtra(Intent.EXTRA_CC, cc);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, asunto);
        emailIntent.putExtra(Intent.EXTRA_TEXT, mensaje);
        emailIntent.setType("message/rfc822");
        startActivity(Intent.createChooser(emailIntent, "Email "));
    }

}