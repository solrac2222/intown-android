package instown.instown.com;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;


public class ExploreFragment extends ActionBarActivity implements View.OnClickListener {

    final static String TAG_ID = "id";
    final static String TAG_NOMBRE = "nombre";
    final static String TAG_NUMERO = "numero";
    final static String TAG_IMAGEN = "imagen";
    final static String TAG_DESCRIPCION = "descripcion";
    GridView gridview;
    static ArrayList<HashMap<String, String>> ExploreArray = new ArrayList<HashMap<String, String>>();
    ArrayList<HashMap<String, String>> FilterArray = new ArrayList<HashMap<String, String>>();
    static ExploreAdapter explore_adapter;
    String subcategoria,nombre,sc="0",page_size="20";
    TextView food,tx_filter;
    Toolbar toolbar;
    AlertDialog.Builder builderSingle,builderSingleMiles,builderSingleFilter;
    ArrayAdapter<String> arrayAdapter,arrayAdapterMiles,arrayAdapterFilter;
    ArrayList<String> ArrayFilterNum = new ArrayList<String>();



    BDInstown logeoBD;
    TextView tx_selector,tx_miles;
    ProgressDialog dialog;
    String city_select="0",miles="6",name_select="",respuestaWS=null;
    final static String TAG_NAME= "name";
    ArrayList<HashMap<String, String>> OpcionesArray = new ArrayList<HashMap<String, String>>();

    GPSTracker gps;
    static String Latitude="0";
    static String Longitude="0";
    TextView tx_sorry;
    String[] valuesCiudad = new String[4];

    static RelativeLayout content_load;
    static String Next="";
    private static boolean loading = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.explore_fragment_categoria);

        if( Build.VERSION.SDK_INT >= 9){
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        logeoBD = new BDInstown (this);
        gps = new GPSTracker(ExploreFragment.this);

        arrayAdapter=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1);
        arrayAdapterMiles=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1);
        arrayAdapterFilter=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1);
        tx_selector=(TextView)findViewById(R.id.tx_selector);
        tx_selector.setOnClickListener(this);
        tx_miles=(TextView)findViewById(R.id.tx_miles);
        tx_miles.setOnClickListener(this);
        tx_filter=(TextView)findViewById(R.id.tx_filter);
        tx_filter.setOnClickListener(this);
        toolbar = (Toolbar)findViewById(R.id.my_awesome_toolbar);
        tx_sorry= (TextView)findViewById(R.id.tx_sorry);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("siii");
                onBackPressed();
            }
        });
        content_load=(RelativeLayout)findViewById(R.id.content_load);
        gridview = (GridView)findViewById(R.id.gridview);
        food=(TextView)findViewById(R.id.food);

        if (!getIntent().getStringExtra("subcategoria").equals(null)) {
            subcategoria=getIntent().getStringExtra("subcategoria");
            nombre=getIntent().getStringExtra("nombre");
            city_select=getIntent().getStringExtra("ciudad");
            food.setText(nombre);
            if (gps.canGetLocation()) {
                Latitude=gps.getLatitude()+"";
                Longitude=gps.getLongitude()+"";
            }
            new MiTareaExplore().execute();
            new MiFilter().execute();
        }


        try {
            logeoBD.abrir();
            try {
                logeoBD.abrir();
                int veces=0;

                Cursor cT =logeoBD.getCity();
                for(cT.moveToFirst();!cT.isAfterLast();cT.moveToNext()){
                    veces++;
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put(TAG_ID,  cT.getString(cT.getColumnIndex("id")));
                    map.put(TAG_NAME,cT.getString(cT.getColumnIndex("name")));
                    if(veces==1){
                        tx_selector.setText(cT.getString(cT.getColumnIndex("name")));
                        city_select=cT.getString(cT.getColumnIndex("id"));
                    }

                    //if(city_select.toString().equals(cT.getString(cT.getColumnIndex("id")))){
                        //tx_selector.setText(cT.getString(cT.getColumnIndex("name")));
                   // }


                    OpcionesArray.add(map);
                    arrayAdapter.add(cT.getString(cT.getColumnIndex("name")));

                }

                valuesCiudad=Constants.CiudadActual(ExploreFragment.this);
                Latitude=valuesCiudad[0];
                Longitude=valuesCiudad[1];
                city_select=valuesCiudad[2];
                name_select=valuesCiudad[3];
                tx_selector.setText(name_select);


                //inicio
                builderSingle= new AlertDialog.Builder(this);
                builderSingle.setTitle("City List");
                builderSingle.setNegativeButton("Cancelar",
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                builderSingle.setAdapter(arrayAdapter,
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                if(which==0){
                                    if (gps.canGetLocation()) {
                                        Latitude=gps.getLatitude()+"";
                                        Longitude=gps.getLongitude()+"";
                                        city_select="0";
                                    }
                                    HashMap<String, String> song = new HashMap<String, String>();
                                    song = OpcionesArray.get(which);
                                    tx_selector.setText(song.get(TAG_NAME));
                                    Constants.ActualizarCiudad(ExploreFragment.this, city_select);
                                }else{
                                    Latitude="0";
                                    Longitude="0";
                                    HashMap<String, String> song = new HashMap<String, String>();
                                    song = OpcionesArray.get(which);
                                    tx_selector.setText(song.get(TAG_NAME));
                                    city_select=song.get(TAG_ID);
                                    Constants.ActualizarCiudad(ExploreFragment.this,city_select);
                                }


                                new MiTareaExplore().execute();
                                new MiFilter().execute();
                            }
                        });

                //miles
                arrayAdapterMiles.add("1 Mile");
                arrayAdapterMiles.add("2 Miles");
                arrayAdapterMiles.add("5 Miles");
                arrayAdapterMiles.add("10 Miles");
                arrayAdapterMiles.add("15 Miles");
                arrayAdapterMiles.add("20 Miles");
                arrayAdapterMiles.add("30 Miles");



                builderSingleMiles= new AlertDialog.Builder(this);
                builderSingleMiles.setTitle("Miles List");
                builderSingleMiles.setNegativeButton("Cancelar",
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                builderSingleMiles.setAdapter(arrayAdapterMiles,
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {



                                tx_miles.setText(arrayAdapterMiles.getItem(which));
                                miles=which+"";
                                new MiTareaExplore().execute();
                                new MiFilter().execute();
                               // MiTareaBiz(getActivity());
                                // String[] separated = miles.split(" ");
                                //miles=separated[0];


                                // new HomeData(getActivity()).execute();

                            }
                        });




                logeoBD.cerrar();

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            logeoBD.cerrar();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


    }

    @Override
    public void onResume() {

        super.onResume();
        TimeApp.reposo(ExploreFragment.this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tx_miles:
                builderSingleMiles.show();
                break;
            case R.id.tx_selector:
                builderSingle.show();
                break;
            case R.id.tx_filter:
                builderSingleFilter.show();
                break;

        }
    }

    private class MiTareaExplore extends AsyncTask<Void, Integer, Boolean> {
        private ProgressDialog dialog;

        @Override
        protected Boolean doInBackground(Void... params) {

            HttpClient httpclient = new DefaultHttpClient();
            HttpGet httppost = new HttpGet(String.format(Constants.subcategories,subcategoria,miles,city_select,sc,page_size,Latitude,Longitude));


            System.out.println(String.format(Constants.subcategories,subcategoria,miles,city_select,sc,page_size,Latitude,Longitude));



            ExploreArray.clear();
            FilterArray.clear();

            try {
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);

                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);

                HttpEntity entity = response.getEntity();
                InputStream instream = entity.getContent();
                String result = Constants.convertStreamToString(instream);
                System.out.println("result "+result);

                JSONArray resultado= null;
                JSONObject myObject = new JSONObject(result);
                Next=myObject.getString("next");
               // email_valido=myObject.getString("email");
               // String results=myObject.getString("results");
                //JSONObject resultsJ = myObject.getJSONObject(results);
                //System.out.println(resultsJ.length()+"");

                resultado = myObject.getJSONArray("results");

                for(int i = 0; i < resultado.length(); i++){
                    JSONObject c = resultado.getJSONObject(i);

                    String merchant=c.getString("merchant");
                    String subcategory=c.getString("subcategory");
                    JSONObject myObjectMerchant  = new JSONObject(merchant);
                    JSONObject myObjectSubcategory  = new JSONObject(subcategory);
                    String business_name=myObjectMerchant.getString("business_name");
                    String id_category=myObjectSubcategory.getString("id");
                    String name_category=myObjectSubcategory.getString("name");


                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put(TAG_ID, c.getString("id"));
                    map.put(TAG_NOMBRE,c.getString("title"));
                    map.put(TAG_DESCRIPCION,business_name);
                    map.put(TAG_IMAGEN,c.getString("image"));
                    ExploreArray.add(map);

                }




            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
            } catch (IOException e) {
                // TODO Auto-generated catch block
            } catch (JSONException e) {
                e.printStackTrace();
            }


            return true;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            int progreso = values[0].intValue();

        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(ExploreFragment.this);
            dialog.setMessage("Cargando Datos..");
            dialog.show();
        }



        @Override
        protected void onPostExecute(Boolean result) {
            dialog.hide();


            if(ExploreArray.size()==0){
                tx_sorry.setVisibility(View.VISIBLE);
            }else{
                tx_sorry.setVisibility(View.GONE);
            }


            explore_adapter=new ExploreAdapter(ExploreFragment.this, ExploreArray);
            gridview.setAdapter(explore_adapter);
            gridview.setOnScrollListener(new GVOnScrollListener());
            gridview.setOnItemClickListener(new AdapterView.OnItemClickListener()
            {
                public void onItemClick(AdapterView<?> parent, View v, int position, long ids)
                {


                    HashMap<String, String> song = new HashMap<String, String>();
                    song = ExploreArray.get(position);

                    Intent ventana_post= new Intent(ExploreFragment.this, InfoFragment.class);
                    new Bundle();
                    ventana_post.putExtra("post",song.get(TAG_ID));
                    startActivity(ventana_post);
                }

            });


        }
    }

    private class MiFilter extends AsyncTask<Void, Integer, Boolean> {
        private ProgressDialog dialog;

        @Override
        protected Boolean doInBackground(Void... params) {

            HttpClient httpclient = new DefaultHttpClient();
            HttpGet httppost = new HttpGet(String.format(Constants.filtered,subcategoria,city_select,miles,Latitude,Longitude));

            ArrayFilterNum.clear();
            arrayAdapterFilter.clear();
            System.out.println(String.format(Constants.filtered, subcategoria, city_select, miles, Latitude, Longitude));

            try {

                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);

                HttpEntity entity = response.getEntity();
                InputStream instream = entity.getContent();
                String result = Constants.convertStreamToString(instream);
                System.out.println("result " + result);

                ArrayFilterNum.add("0");
                arrayAdapterFilter.add("VIEW ALL");


                JSONArray myObject = new JSONArray(result);

                for(int i = 0; i < myObject.length(); i++){
                    JSONObject c = myObject.getJSONObject(i);


                    String id_category=c.getString("id");
                    String name_category=c.getString("name");
                    String post_count=c.getString("post_count");


                    ArrayFilterNum.add(id_category);
                    arrayAdapterFilter.add(name_category+" ("+post_count+")");

                }




            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
            } catch (IOException e) {
                // TODO Auto-generated catch block
            } catch (JSONException e) {
                e.printStackTrace();
            }


            return true;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {


        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(ExploreFragment.this);
            dialog.setMessage("Cargando Datos..");
            dialog.show();
        }



        @Override
        protected void onPostExecute(Boolean result) {
            dialog.hide();



            if(sc.toString().equals("0")){



                /*for(int j=0;j<FilterArray.size();j++){


                }

                String nameC="",idC="";
                arrayAdapterFilter.clear();
                ArrayFilterNum.clear();
                int totalC=0;
                arrayAdapterFilter.add("VIEW ALL");
                ArrayFilterNum.add("0");


                //mostrar subcategorias
                for(int j=0;j<FilterArray.size();j++){
                    HashMap<String, String> songF = new HashMap<String, String>();
                    songF = FilterArray.get(j);
                    songF.get(TAG_NOMBRE);
                    if(j==0){
                        nameC=songF.get(TAG_NOMBRE);
                        idC=songF.get(TAG_ID);
                        totalC=1;
                    }else if((j+1)==FilterArray.size()){
                        if(nameC.toString().equals(songF.get(TAG_NOMBRE))){
                            totalC++;
                            arrayAdapterFilter.add(nameC+" ("+totalC+")");
                            ArrayFilterNum.add(idC);
                        }else{
                            arrayAdapterFilter.add(nameC+" ("+totalC+")");
                            ArrayFilterNum.add(idC);
                            nameC=songF.get(TAG_NOMBRE);
                            idC=songF.get(TAG_ID);
                            totalC=1;
                            arrayAdapterFilter.add(nameC+" ("+totalC+")");
                            ArrayFilterNum.add(idC);
                        }
                    }else{
                        if(nameC.toString().equals(songF.get(TAG_NOMBRE))){
                            totalC++;
                        }else{
                            arrayAdapterFilter.add(nameC+" ("+totalC+")");
                            ArrayFilterNum.add(idC);
                            nameC=songF.get(TAG_NOMBRE);
                            idC=songF.get(TAG_ID);
                            totalC=1;
                        }
                    }

                }*/

                builderSingleFilter= new AlertDialog.Builder(ExploreFragment.this);
                builderSingleFilter.setTitle("Filter");
                builderSingleFilter.setNegativeButton("Cancelar",
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                builderSingleFilter.setAdapter(arrayAdapterFilter,
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                System.out.println(ArrayFilterNum.get(which));
                                sc=ArrayFilterNum.get(which);
                                new MiTareaExplore().execute();

                            }
                        });

            }else{
                sc="0";
            }

        }
    }



    private class MiTareaExplore2 extends AsyncTask<Void, Integer, Boolean> {
        @Override
        protected Boolean doInBackground(Void... params) {

            HashMap<String, String> map = new HashMap<String, String>();
            map.put(TAG_ID, "1");
            map.put(TAG_NOMBRE, "intown.com");
            map.put(TAG_DESCRIPCION, "Free Chop Sticks\nfor the Kids!");
            map.put(TAG_IMAGEN, "http://www.graymatter.a2hosted.com/proyectos/instown/caja.png");
            ExploreArray.add(map);

            HashMap<String, String> map2 = new HashMap<String, String>();
            map2.put(TAG_ID, "2");
            map2.put(TAG_NOMBRE, "intown.com");
            map2.put(TAG_DESCRIPCION, "Free Chop Sticks\nfor the Kids!");
            map2.put(TAG_IMAGEN, "http://www.graymatter.a2hosted.com/proyectos/instown/caja.png");
            ExploreArray.add(map2);

            HashMap<String, String> map3 = new HashMap<String, String>();
            map3.put(TAG_ID, "3");
            map3.put(TAG_NOMBRE, "intown.com");
            map3.put(TAG_DESCRIPCION, "Free Chop Sticks\nfor the Kids!");
            map3.put(TAG_IMAGEN, "http://www.graymatter.a2hosted.com/proyectos/instown/caja.png");
            ExploreArray.add(map3);

            HashMap<String, String> map4 = new HashMap<String, String>();
            map4.put(TAG_ID, "4");
            map4.put(TAG_NOMBRE, "intown.com");
            map4.put(TAG_DESCRIPCION, "Free Chop Sticks\nfor the Kids!");
            map4.put(TAG_IMAGEN, "http://www.graymatter.a2hosted.com/proyectos/instown/caja.png");
            ExploreArray.add(map4);

            HashMap<String, String> map5 = new HashMap<String, String>();
            map5.put(TAG_ID, "5");
            map5.put(TAG_NOMBRE, "intown.com");
            map5.put(TAG_DESCRIPCION, "Free Chop Sticks\nfor the Kids!");
            map5.put(TAG_IMAGEN, "http://www.graymatter.a2hosted.com/proyectos/instown/caja.png");
            ExploreArray.add(map5);

            HashMap<String, String> map6 = new HashMap<String, String>();
            map6.put(TAG_ID, "6");
            map6.put(TAG_NOMBRE, "intown.com");
            map6.put(TAG_DESCRIPCION, "Free Chop Sticks\nfor the Kids!");
            map6.put(TAG_IMAGEN, "http://www.graymatter.a2hosted.com/proyectos/instown/caja.png");
            ExploreArray.add(map6);

            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if(ExploreArray.size()!=0){
                explore_adapter=new ExploreAdapter(ExploreFragment.this, ExploreArray);
                gridview.setAdapter(explore_adapter);

                gridview.setOnItemClickListener(new AdapterView.OnItemClickListener()
                {
                    public void onItemClick(AdapterView<?> parent, View v, int position, long ids)
                    {
                            Intent ventana_categoria= new Intent(ExploreFragment.this, ExploreFragmentCategoria.class);
                            new Bundle();
                            ventana_categoria.putExtra("imagen","2");
                            startActivity(ventana_categoria);
                    }

                });

            }



        }

    }


    public static final class GVOnScrollListener implements AbsListView.OnScrollListener {
        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {

        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem,
                             int visibleItemCount, int totalItemCount) {

            Log.v("first visible count", firstVisibleItem + "");
            Log.v("visible count",visibleItemCount+"");
            Log.v("total item count", totalItemCount + "");

            if (!loading && (totalItemCount - visibleItemCount)<=(firstVisibleItem)) {

                if(!Next.toString().equals("null")){
                    loading = true;
                    content_load.setVisibility(View.VISIBLE);
                    HomeDataReload();
                }

            }



        }
    }


    public static void HomeDataReload(){
        AsyncHttpClient myClient = new AsyncHttpClient();
        System.out.println(Next);
        myClient.get(Next, null, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                String txtResponse = String.valueOf(response);


                try {
                    JSONArray resultado = null;
                    JSONObject myObject = new JSONObject(txtResponse);
                    Next=myObject.getString("next");
                    // next=myObject.getString("next");

                    resultado = myObject.getJSONArray("results");

                    for(int i = 0; i < resultado.length(); i++){
                        JSONObject c = resultado.getJSONObject(i);

                        String merchant=c.getString("merchant");
                        String subcategory=c.getString("subcategory");
                        JSONObject myObjectMerchant  = new JSONObject(merchant);
                        JSONObject myObjectSubcategory  = new JSONObject(subcategory);
                        String business_name=myObjectMerchant.getString("business_name");
                        String id_category=myObjectSubcategory.getString("id");
                        String name_category=myObjectSubcategory.getString("name");


                        HashMap<String, String> map = new HashMap<String, String>();
                        map.put(TAG_ID, c.getString("id"));
                        map.put(TAG_NOMBRE,c.getString("title"));
                        map.put(TAG_DESCRIPCION,business_name);
                        map.put(TAG_IMAGEN,c.getString("image"));
                        ExploreArray.add(map);

                    }

                    loading = false;
                    content_load.setVisibility(View.GONE);
                    explore_adapter.notifyDataSetChanged();


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }


            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                System.out.println(statusCode);
                Log.d("THROW", throwable.toString());
            }

        });
    }


}