package instown.instown.com;


import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

import instown.instown.com.exception.NetworkException;
import instown.instown.com.exception.ParsingException;
import instown.instown.com.exception.ServerException;
import instown.instown.com.exception.TimeOutException;
import instown.instown.com.services.AppAsynchTask;


public class InfoFragmentDetails extends ActionBarActivity implements View.OnClickListener {
    Toolbar toolbar;
    String bannerS,website="",facebook="",twitter="",pinterest="",id_merchant,business_name,card_details;
    ImageView banner;
    TextView tx_name,tx_south,tx_numero;
    ImageView fb_ico,tw_ico,p_ico;
    RelativeLayout cont_incentive;
    String respuestaWS=null;
    final static String TAG_ID = "id";
    final static String TAG_NOMBRE = "nombre";
    final static String TAG_IMAGEN = "imagen";
    final static String TAG_DESCRIPCION = "descripcion";
    ArrayList<HashMap<String, String>> HomeArray = new ArrayList<HashMap<String, String>>();
    ExploreAdapter explore_adapter;
    GridView gridview_home;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.info_fragment_details);

        if( Build.VERSION.SDK_INT >= 9){
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        if (!getIntent().getStringExtra("banner").equals(null)) {
            bannerS=getIntent().getStringExtra("banner");
            website=getIntent().getStringExtra("website");
            facebook=getIntent().getStringExtra("facebook");
            twitter=getIntent().getStringExtra("twitter");
            pinterest=getIntent().getStringExtra("pinterest");
            id_merchant=getIntent().getStringExtra("id_merchant");
            business_name=getIntent().getStringExtra("business_name");
            card_details=getIntent().getStringExtra("card_details");
            new InfoData(this).execute();
        }

        init();
        events();

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("siii");
                onBackPressed();
            }
        });


    }

    @Override
    public void onResume() {

        super.onResume();
        TimeApp.reposo(InfoFragmentDetails.this);

    }

    private void init() {
        toolbar = (Toolbar)findViewById(R.id.my_awesome_toolbar);
        banner= (ImageView)findViewById(R.id.banner);
        tx_name= (TextView)findViewById(R.id.tx_name);
        tx_south= (TextView)findViewById(R.id.tx_south);
        tx_numero= (TextView)findViewById(R.id.tx_numero);
        fb_ico= (ImageView)findViewById(R.id.fb_ico);
        tw_ico= (ImageView)findViewById(R.id.tw_ico);
        p_ico= (ImageView)findViewById(R.id.p_ico);
        cont_incentive= (RelativeLayout)findViewById(R.id.cont_incentive);
        gridview_home=(GridView)findViewById(R.id.gridview_home);

        if(card_details.toString().equals("")){
            cont_incentive.setVisibility(View.GONE);
        }
        tx_name.setText(business_name);
        tx_numero.setText(card_details);

    }

    private void events() {
        Picasso.with(InfoFragmentDetails.this)
                .load(bannerS)
                .skipMemoryCache()
                .into(banner);
        tx_south.setOnClickListener(this);
        fb_ico.setOnClickListener(this);
        tw_ico.setOnClickListener(this);
        p_ico.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tx_south:
                if (website != null && !website.isEmpty()) {
                    abrir_url(website);
                }else{
                    Toast toast2 =Toast.makeText(InfoFragmentDetails.this,"No Disponible", Toast.LENGTH_SHORT);
                    toast2.show();
                }
                break;
            case R.id.fb_ico:
                if (facebook != null && !facebook.isEmpty()) {
                    abrir_url(facebook);
                }else{
                    Toast toast2 =Toast.makeText(InfoFragmentDetails.this,"No Disponible", Toast.LENGTH_SHORT);
                    toast2.show();
                }
                break;
            case R.id.tw_ico:
                if (twitter != null && !twitter.isEmpty()) {
                    abrir_url(twitter);
                }else{
                    Toast toast2 =Toast.makeText(InfoFragmentDetails.this,"No Disponible", Toast.LENGTH_SHORT);
                    toast2.show();
                }
                break;
            case R.id.p_ico:
                if (pinterest != null && !pinterest.isEmpty()) {
                    abrir_url(pinterest);
                }else{
                    Toast toast2 =Toast.makeText(InfoFragmentDetails.this,"No Disponible", Toast.LENGTH_SHORT);
                    toast2.show();
                }
                break;
        }

    }

    private void abrir_url(String url) {
        Uri uriWeb = Uri.parse(url);
        Intent intentWeb = new Intent(Intent.ACTION_VIEW, uriWeb);
        startActivity(intentWeb);
    }


    public class InfoData extends AppAsynchTask<Void, String, String> {
        Activity actividad;


        public InfoData(Activity activity) {
            super(activity);
            // TODO Auto-generated constructor stub
            actividad=activity;
        }

        @Override
        protected String customDoInBackground(Void... params)
                throws NetworkException, ServerException, ParsingException,
                TimeOutException, IOException, JSONException {

            HttpClient httpclient = new DefaultHttpClient();
            HttpGet httppost = new HttpGet(String.format(Constants.merchants_items,Constants.urlEncode(id_merchant)));
            System.out.println(String.format(Constants.merchants_items,Constants.urlEncode(id_merchant)));
            HomeArray.clear();

            try {

                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);

                HttpEntity entity = response.getEntity();
                InputStream instream = entity.getContent();
                String result = Constants.convertStreamToString(instream);
                System.out.println("result "+result);
                JSONArray resultado= null;

                if(result.length()<32) {

                }else{
                    resultado = new JSONArray(result);

                    for(int i = 0; i < resultado.length(); i++){
                        JSONObject c = resultado.getJSONObject(i);
                        HashMap<String, String> map = new HashMap<String, String>();
                        map.put(TAG_ID, c.getString("id"));
                        map.put(TAG_NOMBRE,business_name);
                        map.put(TAG_DESCRIPCION,c.getString("title"));
                        map.put(TAG_IMAGEN,c.getString("image"));
                        HomeArray.add(map);

                    }
                }



                respuestaWS="si";


            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
            } catch (IOException e) {
                // TODO Auto-generated catch block
            } catch (JSONException e) {
                e.printStackTrace();
            }


            return respuestaWS;

        }

        @Override
        protected void customOnPostExecute(String result){
            respuestaWS=null;

            explore_adapter=new ExploreAdapter(InfoFragmentDetails.this, HomeArray);
            gridview_home.setAdapter(explore_adapter);

            gridview_home.setOnItemClickListener(new AdapterView.OnItemClickListener(){
                public void onItemClick(AdapterView<?> parent, View v, int position, long ids)
                {


                    HashMap<String, String> song = new HashMap<String, String>();
                    song = HomeArray.get(position);

                    Intent ventana_post= new Intent(InfoFragmentDetails.this, InfoFragment.class);
                    new Bundle();
                    ventana_post.putExtra("post",song.get(TAG_ID));
                    startActivity(ventana_post);
                }

            });


        }


    }


   /*

    private class MiTareaPost extends AsyncTask<Void, Integer, Boolean> {
        private ProgressDialog dialog;

        @Override
        protected Boolean doInBackground(Void... params) {

            HttpClient httpclient = new DefaultHttpClient();
            HttpGet httppost = new HttpGet(Constants.post+post);
            System.out.println(Constants.post+post);
            try {
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);

                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);

                HttpEntity entity = response.getEntity();
                InputStream instream = entity.getContent();
                String result = Constants.convertStreamToString(instream);
                System.out.println("result "+result);

                JSONArray resultado= null;
                JSONObject myObject = new JSONObject(result);

                id=myObject.getString("id");
                String merchant=myObject.getString("merchant");
                JSONObject myObjectMerchant  = new JSONObject(merchant);
                business_name=myObjectMerchant.getString("business_name");
                image=myObjectMerchant.getString("image");
                id_coordenadas=myObjectMerchant.getString("id");
                titulo=myObject.getString("title");
                details=myObject.getString("details");
                card_details=myObject.getString("card_details");


                links=myObjectMerchant.getString("links");
                JSONArray myObjectLinks  = new JSONArray(links);
                for(int i=0;i<myObjectLinks.length();i++){
                    JSONObject cLink = myObjectLinks.getJSONObject(i);
                    if(cLink.getString("type").toString().equals("0")){
                        website=cLink.getString("url").toString();
                    }else if(cLink.getString("type").toString().equals("1")){
                        facebook=cLink.getString("url").toString();
                    }else if(cLink.getString("type").toString().equals("2")){
                        twitter=cLink.getString("url").toString();
                    }else if(cLink.getString("type").toString().equals("4")){
                        pinterest=cLink.getString("url").toString();
                    }
                }


                share_url=myObject.getString("share_url");
                String locations=myObject.getString("locations");
                JSONArray myObjectLocations  = new JSONArray(locations);

                disponibles=myObjectLocations.length();

                if(myObjectLocations.length()!=0){
                    JSONObject c = myObjectLocations.getJSONObject(0);
                    address_1=c.getString("address_1");
                    city=c.getString("city");
                }






            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
            } catch (IOException e) {
                // TODO Auto-generated catch block
            } catch (JSONException e) {
                e.printStackTrace();
            }


            return true;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            int progreso = values[0].intValue();

        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(InfoFragmentDetails.this);
            dialog.setMessage("Cargando Datos..");
            dialog.show();
        }



        @Override
        protected void onPostExecute(Boolean result) {
            dialog.hide();
            tx_name.setText(business_name);
            tx_numero.setText(card_details);
            if(card_details.toString().equals("")){
                cont_incentive.setVisibility(View.GONE);
            }

          //  web.loadData(details, "text/html", null);

            web.loadUrl("http://www.intown.com/post/"+post+"/content/");

            web.getSettings().setLoadWithOverviewMode(true);
            web.getSettings().setUseWideViewPort(true);
            new MiTareaCoordenadas().execute();
        }
    }
*/
}