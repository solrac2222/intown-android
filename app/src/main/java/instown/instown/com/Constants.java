package instown.instown.com;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.widget.ArrayAdapter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;

/**
 * Created by Francisco on 31/5/15.
 */
public class Constants {
    private final Context nContexto;
    //public static String URL_DOMINIO ="http://intown.etwirl.com:9001/api";
    public static String URL_DOMINIO ="http://intown.com/api";
    public static String FINAL ="?format=json";
    public static String NAME_APP ="InTown";



    public static String VERSION = "v1";
    public static String registration = URL_DOMINIO+"/users/signup/";
    public static String categories = URL_DOMINIO+"/categories";
    public static String post_cities = URL_DOMINIO+"/posts/cities/"+FINAL;
    public static String loginFB= URL_DOMINIO+"/users/facebook/";
    public static String loginTW= URL_DOMINIO+"/users/twitter/";
    public static String login= URL_DOMINIO+"/users/login/";
    public static String events = URL_DOMINIO+"/posts/events/";
    public static String subcategories = URL_DOMINIO+"/posts/live/?c=%s&dist=%s&city=%s&sc=%s&page_size=%s&lat=%s&lng=%s";
    public static String post = URL_DOMINIO+"/posts/";
    public static String location = URL_DOMINIO+"/locations/";
    public static String merchants = URL_DOMINIO+"/merchants/active/?dist=%s&city=%s&c=%s&q=%s&lat=%s&lng=%s";
    public static String posts = URL_DOMINIO+"/merchants/";
    public static String seguir_posts = URL_DOMINIO+"/merchants/";
    public static String posts_remind = URL_DOMINIO+"/posts/%s/remind/";
    public static String posts_want = URL_DOMINIO+"/posts/%s/want/";
    public static String posts_redeem = URL_DOMINIO+"/posts/%s/redeem/";

    public static String my_stuff = URL_DOMINIO+"/posts/my_stuff/?c=%s&dist=%s&city=%s&type=%s&lat=%s&lng=%s&page_size=%s";
    public static String home = URL_DOMINIO+"/posts/live/?fc=%s&sc=%s&c=%s&dist=%s&city=%s&q=%s&lat=%s&lng=%s&page_size=%s";
    public static String page = URL_DOMINIO+"/posts/live/?fc=%s&sc=%s&c=%s&dist=%s&city=%s";
    public static String caledario_events = URL_DOMINIO+"/posts/events/?d=%s&city=%s&dist=%s&lat=%s&lng=%s";
    public static String user = URL_DOMINIO+"/users/me/";
    public static String merchants_items = URL_DOMINIO+"/merchants/%s/posts/";
    public static String consumerKey="NmS3Q9OmCMplwSd2C0tpyUJ2j";
    public static String consumerSecret="hKTWELXnCPQc3RNYkiOW5ZCMUYz3cOkhF9Z7Pz9VvEFOIL9qeJ";
    public static String pass_reset = URL_DOMINIO+"/users/password/reset/";
    public static String filtered = URL_DOMINIO+"/subcategories/filtered/?c=%s&city=%s&dist=%s&lat=%s&lng=%s";
    public static String remove_post = URL_DOMINIO+"/posts/%s/remove/";



    //web
    public static String why_be = "http://intown.com/faqs/";
    public static String terms = "http://intown.com/tou/";
    public static String privacy = "http://intown.com/privacy/";



   public static String video = "http://graymatter.a2hosted.com/proyectos/instown/video2.mp4";
    //public static String video = "http://www.ebookfrenzy.com/android_book/movie.mp4";


    //merchant
    public static String merchant = "http://intown.com/accounts/login/?next=/merchant/";
    public static String merchantSign = "http://intown.com/accounts/merchant/signup/";
    public static String merchantLog = "http://intown.com/merchant/";
    public static String merchantAd = "http://intown.com/accounts/merchant/signup/ad/";
    public static String merchantNon = "http://intown.com/accounts/merchant/signup/nonprofit/";
    public static String merchantHos = "http://intown.com/hospitality/";



    //videoURL="http://cs634501v4.vk.me/u295938807/videos/b355148015.360.mp4?extra=zYpDBBvOh_6r7qnQthtVHFIqutJxOCNHYURSMNvEJc5qpp5HVX8hkC0j1a0MAJDFX3v2Q30bZlwGyEPRpMyp9WgM4c_LhF3J";




    //http://intown.com/api/posts/events/?d=2015-06-23&city=23

    public Constants (Context c){
        nContexto=c;
    }

    static String convertStreamToString(InputStream is) {

        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    public static String urlEncode(String s) {
        try {
            return URLEncoder.encode(s, "UTF-8");
        }
        catch (UnsupportedEncodingException e) {
            Log.wtf("", "UTF-8 should always be supported", e);
            return "";
        }
    }

    public static String OnlyMonth(String month) {

        String monthString;
        switch (month) {
            case "01":  monthString = "January";
                break;
            case "02":  monthString = "February";
                break;
            case "03":  monthString = "March";
                break;
            case "04":  monthString = "April";
                break;
            case "05":  monthString = "May";
                break;
            case "06":  monthString = "June";
                break;
            case "07":  monthString = "July";
                break;
            case "08":  monthString = "August";
                break;
            case "09":  monthString = "September";
                break;
            case "10": monthString = "October";
                break;
            case "11": monthString = "November";
                break;
            case "12": monthString = "December";
                break;
            default: monthString = "Invalid month";
                break;
        }


        return monthString;

    }


    public static String[] CiudadActual(Activity actividad) {
        String[] values = new String[4];
        String Latitude="",Longitude="",city_select="",name_select="";
        Cursor cT;

        BDInstown logeoBD = new BDInstown (actividad);
        try {
            logeoBD.abrir();
            if(logeoBD.GetCitySelect().toString().equals("")){
                GPSTracker gps = new GPSTracker(actividad);
                if (gps.canGetLocation()) {
                    Latitude=gps.getLatitude()+"";
                    Longitude=gps.getLongitude()+"";
                    city_select="0";
                    name_select="Your current location";
                }
            }else{
                cT=logeoBD.GetCitySelectAll();
                for(cT.moveToFirst();!cT.isAfterLast();cT.moveToNext()){
                    city_select=cT.getString(cT.getColumnIndex("id"));
                    name_select=cT.getString(cT.getColumnIndex("name"));
                }

            }
            logeoBD.cerrar();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        values[0]=Latitude+"";
        values[1]=Longitude+"";
        values[2]=city_select+"";
        values[3]=name_select+"";
        return values;
    }

    public static void ActualizarCiudad(Activity actividad,String id_ciudad) {
        BDInstown logeoBD = new BDInstown (actividad);
        try {
            logeoBD.abrir();
            logeoBD.UpdateCity(id_ciudad);
            logeoBD.cerrar();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}