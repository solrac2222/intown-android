package instown.instown.com;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.Time;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import instown.instown.com.exception.NetworkException;
import instown.instown.com.exception.ParsingException;
import instown.instown.com.exception.ServerException;
import instown.instown.com.exception.TimeOutException;
import instown.instown.com.services.AppAsynchTask;

import static android.widget.AbsListView.OnScrollListener;


public class BizFragment extends Fragment implements View.OnClickListener {
    private static List<CBiz> list_biz = new ArrayList<>();
    private static RecyclerView rvp;
    String next="";
    static TextView tx_sorry;
    static int pastVisiblesItems;
    static int visibleItemCount;
    static int totalItemCount;
    static LinearLayoutManager llm;
    static RVAdapterBiz adapter;
    AlertDialog.Builder builderSingle,builderSingleMiles;
    ArrayAdapter<String> arrayAdapter,arrayAdapterMiles;
    BDInstown logeoBD;
    final static String TAG_ID = "id";
    final static String TAG_NOMBRE = "nombre";
    final static String TAG_IMAGEN = "imagen";
    final static String TAG_DESCRIPCION = "descripcion";
    final static String TAG_NAME= "name";
    ArrayList<HashMap<String, String>> OpcionesArray = new ArrayList<HashMap<String, String>>();
    static String city_select="";
    static String miles="6";
    String name_select="";
    String respuestaWS=null;
    TextView tx_selector;
    TextView tx_miles;
    static TextView foodT;
    static ProgressDialog dialog;
    static String categorie="0";
    static Activity actividad;
    static String q="";
    GPSTracker gps;
    static String Latitude="0";
    static String Longitude="0";
    static boolean loadingMore = false;
    int latest=0,previous=0,total_resultado,total_mostrado=0;
    static RelativeLayout content_load;
    static String Next="";
    String[] valuesCiudad = new String[4];


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.biz_listing, container, false);

        if( Build.VERSION.SDK_INT >= 9){
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        actividad=getActivity();
        gps = new GPSTracker(getActivity());
        arrayAdapter=new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1);
        arrayAdapterMiles=new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1);

        tx_selector=(TextView) view.findViewById(R.id.tx_selector);
        tx_selector.setOnClickListener(this);
        tx_miles=(TextView) view.findViewById(R.id.tx_miles);
        tx_miles.setOnClickListener(this);
        tx_sorry= (TextView) view.findViewById(R.id.tx_sorry);
        foodT=(TextView) view.findViewById(R.id.foodT);
        content_load=(RelativeLayout)view.findViewById(R.id.content_load);

        rvp=(RecyclerView)view.findViewById(R.id.rvp);

        llm = new LinearLayoutManager(getActivity());
        rvp.setLayoutManager(llm);


        logeoBD = new BDInstown (getActivity());

        try {
            logeoBD.abrir();
            try {
                logeoBD.abrir();
                int veces=0;
                //GetCitySelect=logeoBD.GetCitySelect();

                Cursor cT =logeoBD.getCity();
                for(cT.moveToFirst();!cT.isAfterLast();cT.moveToNext()){
                    veces++;
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put(TAG_ID,  cT.getString(cT.getColumnIndex("id")));
                    map.put(TAG_NAME,cT.getString(cT.getColumnIndex("name")));
                    /*if(veces==1){
                        tx_selector.setText(cT.getString(cT.getColumnIndex("name")));
                        city_select=cT.getString(cT.getColumnIndex("id"));
                    }*/


                    OpcionesArray.add(map);
                    arrayAdapter.add(cT.getString(cT.getColumnIndex("name")));

                }

                valuesCiudad=Constants.CiudadActual(getActivity());
                Latitude=valuesCiudad[0];
                Longitude=valuesCiudad[1];
                city_select=valuesCiudad[2];
                name_select=valuesCiudad[3];
                tx_selector.setText(name_select);

                //inicio
                builderSingle= new AlertDialog.Builder(getActivity());
                builderSingle.setTitle("City List");
                builderSingle.setNegativeButton("Cancelar",
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                builderSingle.setAdapter(arrayAdapter,
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                if(which==0){
                                    if (gps.canGetLocation()) {
                                        Latitude=gps.getLatitude()+"";
                                        Longitude=gps.getLongitude()+"";
                                        city_select="0";
                                    }
                                    HashMap<String, String> song = new HashMap<String, String>();
                                    song = OpcionesArray.get(which);
                                    tx_selector.setText(song.get(TAG_NAME));
                                    Constants.ActualizarCiudad(getActivity(), city_select);
                                }else{
                                    Latitude="0";
                                    Longitude="0";
                                    HashMap<String, String> song = new HashMap<String, String>();
                                    song = OpcionesArray.get(which);
                                    tx_selector.setText(song.get(TAG_NAME));
                                    city_select=song.get(TAG_ID);
                                    Constants.ActualizarCiudad(getActivity(),city_select);
                                }

                                MiTareaBiz();
                            }
                        });

                //miles
                arrayAdapterMiles.add("1 Mile");
                arrayAdapterMiles.add("2 Miles");
                arrayAdapterMiles.add("5 Miles");
                arrayAdapterMiles.add("10 Miles");
                arrayAdapterMiles.add("15 Miles");
                arrayAdapterMiles.add("20 Miles");
                arrayAdapterMiles.add("30 Miles");



                builderSingleMiles= new AlertDialog.Builder(getActivity());
                builderSingleMiles.setTitle("Miles List");
                builderSingleMiles.setNegativeButton("Cancelar",
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                builderSingleMiles.setAdapter(arrayAdapterMiles,
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {



                                tx_miles.setText(arrayAdapterMiles.getItem(which));
                                miles=which+"";
                                MiTareaBiz();
                                // String[] separated = miles.split(" ");
                                //miles=separated[0];


                               // new HomeData(getActivity()).execute();

                            }
                        });




                logeoBD.cerrar();

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            logeoBD.cerrar();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        MiTareaBiz();

        return view;
    }

    @Override
    public void onResume() {

        super.onResume();
        TimeApp.reposo(getActivity());

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tx_miles:
                builderSingleMiles.show();
                break;
            case R.id.tx_selector:
                builderSingle.show();
                break;

        }
    }


    public static void  MiTareaBiz(){
        dialog = new ProgressDialog(actividad);
        dialog.setMessage("Please Wait...");
        dialog.show();
        list_biz.clear();
        AsyncHttpClient myClient = new AsyncHttpClient();
        PersistentCookieStore cookieStore = new PersistentCookieStore(actividad);
        myClient.setCookieStore(cookieStore);
        System.out.println(String.format(Constants.merchants,miles,city_select,categorie,q,Latitude,Longitude));
        myClient.get(String.format(Constants.merchants,miles,city_select,categorie,q,Latitude,Longitude), null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                String txtResponse = String.valueOf(response);
                System.out.println(txtResponse);
                q="";
                try {
                    JSONObject myObject = new JSONObject(txtResponse);

                   // next=myObject.getString("next");
                    JSONArray myObjectitems  = new JSONArray(myObject.getString("results"));
                    Next=myObject.getString("next");
                    System.out.println(Next+" next");
                    for(int i = 0; i < myObjectitems.length(); i++){
                        String points="";
                        JSONObject c = myObjectitems.getJSONObject(i);

                        JSONArray myObjectLocations  = new JSONArray(c.getString("locations"));
                        for(int j = 0; j < myObjectLocations.length(); j++){
                            JSONObject d = myObjectLocations.getJSONObject(j);
                            points=d.getString("location");
                        }
                        System.out.println("valor "+c.getString("has_incentive"));
                        list_biz.add(new CBiz(c.getString("id"),c.getString("business_name"), c.getString("follow_count"),c.getString("incentive"),"1",c.getString("locations"),c.getString("banner"),c.getString("is_following"),c.getString("links"),c.getString("has_incentive")));



                        }

                    if(list_biz.size()==0){
                        tx_sorry.setVisibility(View.VISIBLE);
                    }else{
                        tx_sorry.setVisibility(View.GONE);
                    }

                    adapter = new RVAdapterBiz(actividad,list_biz);
                    rvp.setAdapter(adapter);

                    rvp.setOnScrollListener(new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                            visibleItemCount = llm.getChildCount();
                            totalItemCount = llm.getItemCount();
                            pastVisiblesItems = llm.findFirstVisibleItemPosition();

                            if (!loadingMore) {
                                if ( (visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                                    if(!Next.toString().equals("null")){
                                        loadingMore = true;
                                        MiTareaBizReload();
                                        content_load.setVisibility(View.VISIBLE);
                                    }
                                }
                            }
                        }
                    });


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                dialog.hide();


            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                dialog.hide();

            }
        });
    }


    public static void  MiTareaBizReload(){
        AsyncHttpClient myClient = new AsyncHttpClient();
        PersistentCookieStore cookieStore = new PersistentCookieStore(actividad);
        myClient.setCookieStore(cookieStore);
        System.out.println(Next);
        myClient.get(Next, null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                String txtResponse = String.valueOf(response);
                System.out.println(txtResponse);
                q="";
                try {
                    JSONObject myObject = new JSONObject(txtResponse);

                    // next=myObject.getString("next");
                    JSONArray myObjectitems  = new JSONArray(myObject.getString("results"));
                    Next=myObject.getString("next");
                    System.out.println(Next+" next");
                    for(int i = 0; i < myObjectitems.length(); i++){
                        String points="";
                        JSONObject c = myObjectitems.getJSONObject(i);

                        JSONArray myObjectLocations  = new JSONArray(c.getString("locations"));
                        for(int j = 0; j < myObjectLocations.length(); j++){
                            JSONObject d = myObjectLocations.getJSONObject(j);
                            points=d.getString("location");
                        }

                        list_biz.add(new CBiz(c.getString("id"),c.getString("business_name"), c.getString("follow_count"),c.getString("incentive"),"1",c.getString("locations"),c.getString("banner"),c.getString("is_following"),c.getString("links"),c.getString("has_incentive")));



                    }

                    loadingMore = false;
                    content_load.setVisibility(View.GONE);
                    adapter.notifyDataSetChanged();



                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);

            }
        });
    }



/*

    public class MiTareaBiz2 extends AppAsynchTask<Void, String, String> {
        Activity actividad;


        public MiTareaBiz2(Activity activity) {
            super(activity);
            // TODO Auto-generated constructor stub
            actividad=activity;
        }

        @Override
        protected String customDoInBackground(Void... params)
                throws NetworkException, ServerException, ParsingException,
                TimeOutException, IOException, JSONException {


            HttpClient httpclient = new DefaultHttpClient();
            HttpGet httppost = new HttpGet(String.format(Constants.merchants,miles,city_select));
            System.out.println(String.format(Constants.merchants,miles,city_select));

            try {
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);

                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);

                HttpEntity entity = response.getEntity();
                InputStream instream = entity.getContent();
                String result = Constants.convertStreamToString(instream);
                JSONArray resultado= null;
                JSONObject myObject = new JSONObject(result);
                next=myObject.getString("next");
                JSONArray myObjectitems  = new JSONArray(myObject.getString("results"));

                for(int i = 0; i < myObjectitems.length(); i++){
                    String points="";
                    JSONObject c = myObjectitems.getJSONObject(i);

                    JSONArray myObjectLocations  = new JSONArray(c.getString("locations"));
                    for(int j = 0; j < myObjectLocations.length(); j++){
                        JSONObject d = myObjectLocations.getJSONObject(j);
                        points=d.getString("location");
                    }

                    list_biz.add(new CBiz(c.getString("id"),c.getString("business_name"), c.getString("follow_count"),c.getString("incentive"),"1",points,c.getString("banner")));
                }

                respuestaWS="si";


            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
            } catch (IOException e) {
                // TODO Auto-generated catch block
            }catch (JSONException e) {
                e.printStackTrace();
            }

            return respuestaWS;

        }

        @Override
        protected void customOnPostExecute(String result){

            adapter = new RVAdapterBiz(getActivity(),list_biz);
            rvp.setAdapter(adapter);

            rvp.setOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                    visibleItemCount = llm.getChildCount();
                    totalItemCount = llm.getItemCount();
                    pastVisiblesItems = llm.findFirstVisibleItemPosition();

                    if (!loadingMore) {
                        if ( (visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                          if(!next.equals(null)){
                              new MiTareaBizMore(getActivity(),next).execute();
                          }


                        }
                    }
                }
            });




        }


    }



    public class MiTareaBizMore extends AppAsynchTask<Void, String, String> {
        Activity actividad;
        String url;


        public MiTareaBizMore(Activity activity,String urlE) {
            super(activity);
            // TODO Auto-generated constructor stub
            actividad=activity;
            url=urlE;
        }



        @Override
        protected String customDoInBackground(Void... params)
                throws NetworkException, ServerException, ParsingException,
                TimeOutException, IOException, JSONException {

            loadingMore = true;
            HttpClient httpclient = new DefaultHttpClient();
            HttpGet httppost = new HttpGet(url);
            try {
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);

                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);

                HttpEntity entity = response.getEntity();
                InputStream instream = entity.getContent();
                String result = Constants.convertStreamToString(instream);
                JSONArray resultado= null;
                JSONObject myObject = new JSONObject(result);
                next=myObject.getString("next");
                JSONArray myObjectitems  = new JSONArray(myObject.getString("results"));

                for(int i = 0; i < myObjectitems.length(); i++){
                    String points="";
                    JSONObject c = myObjectitems.getJSONObject(i);

                    JSONArray myObjectLocations  = new JSONArray(c.getString("locations"));
                    for(int j = 0; j < myObjectLocations.length(); j++){
                        JSONObject d = myObjectLocations.getJSONObject(j);
                        points=d.getString("location");
                    }

                    list_biz.add(new CBiz(c.getString("id"),c.getString("business_name"), c.getString("follow_count"),c.getString("address_1"),"1",points,c.getString("banner")));
                }

                respuestaWS="si";


            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
            } catch (IOException e) {
                // TODO Auto-generated catch block
            }catch (JSONException e) {
                e.printStackTrace();
            }

            return respuestaWS;

        }

        @Override
        protected void customOnPostExecute(String result){
            loadingMore = false;
            adapter.notifyDataSetChanged();
            //rvp.notify();
           // rvp.setAdapter(adapter);


        }


    }
*/
}