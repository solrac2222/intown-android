package instown.instown.com;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import java.util.ArrayList;
import java.util.HashMap;


public class ExploreFragmentCategoria extends Activity {

    final static String TAG_ID = "id";
    final static String TAG_NOMBRE = "nombre";
    final static String TAG_IMAGEN = "imagen";
    final static String TAG_DESCRIPCION = "descripcion";
    GridView gridview;
    ArrayList<HashMap<String, String>> ExploreArray = new ArrayList<HashMap<String, String>>();
    ExploreAdapter explore_adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.explore_fragment);

        if( Build.VERSION.SDK_INT >= 9){
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        gridview = (GridView)findViewById(R.id.gridview);

        new MiTareaExplore().execute();
    }

    @Override
    public void onResume() {

        super.onResume();
        TimeApp.reposo(ExploreFragmentCategoria.this);

    }


    private class MiTareaExplore extends AsyncTask<Void, Integer, Boolean> {
        @Override
        protected Boolean doInBackground(Void... params) {

            HashMap<String, String> map = new HashMap<String, String>();
            map.put(TAG_ID, "1");
            map.put(TAG_NOMBRE, "intown.com");
            map.put(TAG_DESCRIPCION, "Free Chop Sticks\nfor the Kids!");
            map.put(TAG_IMAGEN, "http://www.graymatter.a2hosted.com/proyectos/instown/caja.png");
            ExploreArray.add(map);

            HashMap<String, String> map2 = new HashMap<String, String>();
            map2.put(TAG_ID, "2");
            map2.put(TAG_NOMBRE, "intown.com");
            map2.put(TAG_DESCRIPCION, "Free Chop Sticks\nfor the Kids!");
            map2.put(TAG_IMAGEN, "http://www.graymatter.a2hosted.com/proyectos/instown/caja.png");
            ExploreArray.add(map2);

            HashMap<String, String> map3 = new HashMap<String, String>();
            map3.put(TAG_ID, "3");
            map3.put(TAG_NOMBRE, "intown.com");
            map3.put(TAG_DESCRIPCION, "Free Chop Sticks\nfor the Kids!");
            map3.put(TAG_IMAGEN, "http://www.graymatter.a2hosted.com/proyectos/instown/caja.png");
            ExploreArray.add(map3);

            HashMap<String, String> map4 = new HashMap<String, String>();
            map4.put(TAG_ID, "4");
            map4.put(TAG_NOMBRE, "intown.com");
            map4.put(TAG_DESCRIPCION, "Free Chop Sticks\nfor the Kids!");
            map4.put(TAG_IMAGEN, "http://www.graymatter.a2hosted.com/proyectos/instown/caja.png");
            ExploreArray.add(map4);

            HashMap<String, String> map5 = new HashMap<String, String>();
            map5.put(TAG_ID, "5");
            map5.put(TAG_NOMBRE, "intown.com");
            map5.put(TAG_DESCRIPCION, "Free Chop Sticks\nfor the Kids!");
            map5.put(TAG_IMAGEN, "http://www.graymatter.a2hosted.com/proyectos/instown/caja.png");
            ExploreArray.add(map5);

            HashMap<String, String> map6 = new HashMap<String, String>();
            map6.put(TAG_ID, "6");
            map6.put(TAG_NOMBRE, "intown.com");
            map6.put(TAG_DESCRIPCION, "Free Chop Sticks\nfor the Kids!");
            map6.put(TAG_IMAGEN, "http://www.graymatter.a2hosted.com/proyectos/instown/caja.png");
            ExploreArray.add(map6);

            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if(ExploreArray.size()!=0){
                explore_adapter=new ExploreAdapter(ExploreFragmentCategoria.this, ExploreArray);
                gridview.setAdapter(explore_adapter);

                gridview.setOnItemClickListener(new AdapterView.OnItemClickListener()
                {
                    public void onItemClick(AdapterView<?> parent, View v, int position, long ids)
                    {
                            Intent ventana_categoria= new Intent(ExploreFragmentCategoria.this, InfoFragment.class);
                            new Bundle();
                            ventana_categoria.putExtra("imagen","2");
                            startActivity(ventana_categoria);
                    }

                });

            }



        }

    }


}