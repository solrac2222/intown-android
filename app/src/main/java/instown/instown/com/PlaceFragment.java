package instown.instown.com;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ScrollView;

import java.util.ArrayList;
import java.util.HashMap;


public class PlaceFragment extends Activity {

    final static String TAG_ID = "id";
    final static String TAG_NOMBRE = "nombre";
    final static String TAG_IMAGEN = "imagen";
    final static String TAG_DESCRIPCION = "descripcion";
    ExpandableHeightGridView gridview;
    ArrayList<HashMap<String, String>> ExploreArray = new ArrayList<HashMap<String, String>>();
    ExploreAdapter explore_adapter;
    ScrollView scrollView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.place_fragment);

        if( Build.VERSION.SDK_INT >= 9){
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        scrollView= ((ScrollView)findViewById(R.id.scrollView));
        gridview = (ExpandableHeightGridView)findViewById(R.id.gv_place);
        gridview.setExpanded(true);


        new MiTareaExplore().execute();
        scrollView.fullScroll(ScrollView.FOCUS_UP);
    }

    @Override
    public void onResume() {

        super.onResume();
        TimeApp.reposo(PlaceFragment.this);

    }


    private class MiTareaExplore extends AsyncTask<Void, Integer, Boolean> {
        @Override
        protected Boolean doInBackground(Void... params) {

            HashMap<String, String> map = new HashMap<String, String>();
            map.put(TAG_ID, "1");
            map.put(TAG_NOMBRE, "name1");
            map.put(TAG_DESCRIPCION, "des1");
            map.put(TAG_IMAGEN, "http://www.graymatter.a2hosted.com/proyectos/instown/caja.png");
            ExploreArray.add(map);

            HashMap<String, String> map2 = new HashMap<String, String>();
            map2.put(TAG_ID, "2");
            map2.put(TAG_NOMBRE, "name2");
            map2.put(TAG_DESCRIPCION, "des2");
            map2.put(TAG_IMAGEN, "http://www.graymatter.a2hosted.com/proyectos/instown/caja.png");
            ExploreArray.add(map2);

            HashMap<String, String> map3 = new HashMap<String, String>();
            map3.put(TAG_ID, "3");
            map3.put(TAG_NOMBRE, "name3");
            map3.put(TAG_DESCRIPCION, "des3");
            map3.put(TAG_IMAGEN, "http://www.graymatter.a2hosted.com/proyectos/instown/caja.png");
            ExploreArray.add(map3);

            HashMap<String, String> map4 = new HashMap<String, String>();
            map4.put(TAG_ID, "4");
            map4.put(TAG_NOMBRE, "name4");
            map4.put(TAG_DESCRIPCION, "des4");
            map4.put(TAG_IMAGEN, "http://www.graymatter.a2hosted.com/proyectos/instown/caja.png");
            ExploreArray.add(map4);

            HashMap<String, String> map5 = new HashMap<String, String>();
            map5.put(TAG_ID, "5");
            map5.put(TAG_NOMBRE, "name5");
            map5.put(TAG_DESCRIPCION, "des5");
            map5.put(TAG_IMAGEN, "http://www.graymatter.a2hosted.com/proyectos/instown/caja.png");
            ExploreArray.add(map5);

            HashMap<String, String> map6 = new HashMap<String, String>();
            map6.put(TAG_ID, "6");
            map6.put(TAG_NOMBRE, "name6");
            map6.put(TAG_DESCRIPCION, "des6");
            map6.put(TAG_IMAGEN, "http://www.graymatter.a2hosted.com/proyectos/instown/caja.png");
            ExploreArray.add(map6);

            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            scrollView.fullScroll(ScrollView.FOCUS_UP);
            if(ExploreArray.size()!=0){
                explore_adapter=new ExploreAdapter(PlaceFragment.this, ExploreArray);
                gridview.setAdapter(explore_adapter);

            }
            scrollView.fullScroll(ScrollView.FOCUS_UP);


        }

    }


}