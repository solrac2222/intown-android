package instown.instown.com;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


public class PageFragment extends Fragment implements View.OnClickListener {
    BDInstown logeoBD;
    AlertDialog.Builder builderSingle,builderSingleMiles;
    ArrayAdapter<String> arrayAdapter,arrayAdapterMiles;
    static String city_select="";
    static String miles="6";
    String name_select="";
    String respuestaWS=null;
    String category="";
    TextView tx_selector,tx_miles;
    static ProgressDialog dialog;
    final static String TAG_ID = "id";
    final static String TAG_NAME= "name";
    final static String TAG_NOMBRE = "nombre";
    final static String TAG_IMAGEN = "imagen";
    final static String TAG_DESCRIPCION = "descripcion";
    ArrayList<HashMap<String, String>> OpcionesArray = new ArrayList<HashMap<String, String>>();
    static ArrayList<HashMap<String, String>> PageArray = new ArrayList<HashMap<String, String>>();
    static ExploreAdapter page_adapter;
    static GridView gridview_page;
    static String categorie="0";
    static Activity actividad;
    static String q="";
    GPSTracker gps;
    static String Latitude="0";
    static String Longitude="0";
    static TextView tx_sorry;
    String[] valuesCiudad = new String[4];
    private static boolean loading = false;
    static int pastVisiblesItems;
    static int visibleItemCount;
    static int totalItemCount;
    static LinearLayoutManager llm;
    static RelativeLayout content_load;
    static String Next="";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.page_fragment, container, false);

        if( Build.VERSION.SDK_INT >= 9){
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        actividad=getActivity();
        gps = new GPSTracker(getActivity());
        arrayAdapter=new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1);
        arrayAdapterMiles=new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1);

        tx_selector=(TextView) view.findViewById(R.id.tx_selector);
        tx_selector.setOnClickListener(this);
        tx_miles=(TextView) view.findViewById(R.id.tx_miles);
        tx_miles.setOnClickListener(this);
        gridview_page=(GridView)view.findViewById(R.id.gridview_home);
        tx_sorry= (TextView) view.findViewById(R.id.tx_sorry);
        content_load=(RelativeLayout)view.findViewById(R.id.content_load);

        logeoBD = new BDInstown (getActivity());

        try {
            logeoBD.abrir();
            try {
                logeoBD.abrir();

                int veces=0;
                Cursor cT =logeoBD.getCity();
                for(cT.moveToFirst();!cT.isAfterLast();cT.moveToNext()){
                    veces++;
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put(TAG_ID,  cT.getString(cT.getColumnIndex("id")));
                    map.put(TAG_NAME,cT.getString(cT.getColumnIndex("name")));
                    if(veces==1){
                        tx_selector.setText(cT.getString(cT.getColumnIndex("name")));
                        city_select=cT.getString(cT.getColumnIndex("id"));
                    }


                    OpcionesArray.add(map);
                    arrayAdapter.add(cT.getString(cT.getColumnIndex("name")));

                }

                valuesCiudad=Constants.CiudadActual(getActivity());
                Latitude=valuesCiudad[0];
                Longitude=valuesCiudad[1];
                city_select=valuesCiudad[2];
                name_select=valuesCiudad[3];
                tx_selector.setText(name_select);


                //inicio
                builderSingle= new AlertDialog.Builder(getActivity());
                builderSingle.setTitle("City List");
                builderSingle.setNegativeButton("Cancelar",
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                builderSingle.setAdapter(arrayAdapter,
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                if(which==0){
                                    if (gps.canGetLocation()) {
                                        Latitude=gps.getLatitude()+"";
                                        Longitude=gps.getLongitude()+"";
                                        city_select="0";
                                    }
                                    HashMap<String, String> song = new HashMap<String, String>();
                                    song = OpcionesArray.get(which);
                                    tx_selector.setText(song.get(TAG_NAME));
                                    Constants.ActualizarCiudad(getActivity(), city_select);
                                }else{
                                    Latitude="0";
                                    Longitude="0";
                                    HashMap<String, String> song = new HashMap<String, String>();
                                    song = OpcionesArray.get(which);
                                    tx_selector.setText(song.get(TAG_NAME));
                                    city_select=song.get(TAG_ID);
                                    Constants.ActualizarCiudad(getActivity(),city_select);
                                }

                                MiTareaPage();
                            }
                        });

                //miles
                arrayAdapterMiles.add("1 Mile");
                arrayAdapterMiles.add("2 Miles");
                arrayAdapterMiles.add("5 Miles");
                arrayAdapterMiles.add("10 Miles");
                arrayAdapterMiles.add("15 Miles");
                arrayAdapterMiles.add("20 Miles");
                arrayAdapterMiles.add("30 Miles");



                builderSingleMiles= new AlertDialog.Builder(getActivity());
                builderSingleMiles.setTitle("Miles List");
                builderSingleMiles.setNegativeButton("Cancelar",
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                builderSingleMiles.setAdapter(arrayAdapterMiles,
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {



                                tx_miles.setText(arrayAdapterMiles.getItem(which));
                                miles=which+"";
                                MiTareaPage();
                                // String[] separated = miles.split(" ");
                                //miles=separated[0];


                                // new HomeData(getActivity()).execute();

                            }
                        });




                logeoBD.cerrar();

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            logeoBD.cerrar();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        MiTareaPage();

        return view;
    }

    @Override
    public void onResume() {

        super.onResume();
        TimeApp.reposo(getActivity());

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tx_miles:
                builderSingleMiles.show();
                break;
            case R.id.tx_selector:
                builderSingle.show();
                break;

        }
    }

    public static void MiTareaPage(){
        dialog = new ProgressDialog(actividad);
        dialog.setMessage("Please Wait...");
        dialog.show();
        PageArray.clear();
        AsyncHttpClient myClient = new AsyncHttpClient();
        PersistentCookieStore cookieStore = new PersistentCookieStore(actividad);
        myClient.setCookieStore(cookieStore);
        String url=String.format(Constants.home,"true","0",categorie,miles,city_select,q,Latitude,Longitude,"20");
        System.out.println(url);
        myClient.get(url, null, new JsonHttpResponseHandler() {


            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                String txtResponse = String.valueOf(response);
                System.out.println(txtResponse);
                q="";

                try {
                    JSONArray resultado = null;
                    JSONObject myObject = new JSONObject(txtResponse);
                    Next=myObject.getString("next");
                    System.out.println(Next);
                    // next=myObject.getString("next");
                    resultado = myObject.getJSONArray("results");

                    for (int i = 0; i < resultado.length(); i++) {
                        JSONObject c = resultado.getJSONObject(i);

                        String merchant = c.getString("merchant");
                        JSONObject myObjectMerchant = new JSONObject(merchant);
                        String business_name = myObjectMerchant.getString("business_name");


                        HashMap<String, String> map = new HashMap<String, String>();
                        map.put(TAG_ID, c.getString("id"));
                        map.put(TAG_NOMBRE, business_name);
                        map.put(TAG_DESCRIPCION, c.getString("title"));
                        map.put(TAG_IMAGEN, c.getString("image"));
                        PageArray.add(map);

                    }

                    if(PageArray.size()==0){
                        tx_sorry.setVisibility(View.VISIBLE);
                    }else{
                        tx_sorry.setVisibility(View.GONE);
                    }

                    page_adapter = new ExploreAdapter(actividad, PageArray);
                    gridview_page.setAdapter(page_adapter);
                    gridview_page.setOnScrollListener(new GVOnScrollListener());
                    gridview_page.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        public void onItemClick(AdapterView<?> parent, View v, int position, long ids) {


                            HashMap<String, String> song = new HashMap<String, String>();
                            song = PageArray.get(position);

                            Intent ventana_post = new Intent(actividad, InfoFragment.class);
                            new Bundle();
                            ventana_post.putExtra("post", song.get(TAG_ID));
                            actividad.startActivity(ventana_post);
                        }

                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                dialog.hide();


            }


            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                System.out.println(statusCode);
                Log.d("THROW", throwable.toString());
                dialog.dismiss();
            }

        });
    }

    public static void MiTareaPageReload(){
        AsyncHttpClient myClient = new AsyncHttpClient();
        PersistentCookieStore cookieStore = new PersistentCookieStore(actividad);
        myClient.setCookieStore(cookieStore);
        System.out.println(Next);
        myClient.get(Next, null, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                String txtResponse = String.valueOf(response);


                try {
                    JSONArray resultado = null;
                    JSONObject myObject = new JSONObject(txtResponse);
                    Next=myObject.getString("next");
                    // next=myObject.getString("next");
                    resultado = myObject.getJSONArray("results");

                    for (int i = 0; i < resultado.length(); i++) {
                        JSONObject c = resultado.getJSONObject(i);

                        String merchant = c.getString("merchant");
                        JSONObject myObjectMerchant = new JSONObject(merchant);
                        String business_name = myObjectMerchant.getString("business_name");


                        HashMap<String, String> map = new HashMap<String, String>();
                        map.put(TAG_ID, c.getString("id"));
                        map.put(TAG_NOMBRE, business_name);
                        map.put(TAG_DESCRIPCION, c.getString("title"));
                        map.put(TAG_IMAGEN, c.getString("image"));
                        PageArray.add(map);

                    }

                    loading = false;
                    content_load.setVisibility(View.GONE);
                    page_adapter.notifyDataSetChanged();


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }


            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                System.out.println(statusCode);
                Log.d("THROW", throwable.toString());
            }

        });
    }

    public static final class GVOnScrollListener implements AbsListView.OnScrollListener {
        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {

        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem,
                             int visibleItemCount, int totalItemCount) {

            Log.v("first visible count",firstVisibleItem+"");
            Log.v("visible count",visibleItemCount+"");
            Log.v("total item count", totalItemCount + "");

            if (!loading && (totalItemCount - visibleItemCount)<=(firstVisibleItem)) {

                if(!Next.toString().equals("null")){
                    loading = true;
                    content_load.setVisibility(View.VISIBLE);
                    MiTareaPageReload();
                }

            }



        }
    }

}