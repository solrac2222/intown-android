package instown.instown.com;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.view.View.OnClickListener;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import android.text.format.Time;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import instown.instown.com.exception.NetworkException;
import instown.instown.com.exception.ParsingException;
import instown.instown.com.exception.ServerException;
import instown.instown.com.exception.TimeOutException;
import instown.instown.com.services.AppAsynchTask;


public class HomeFragment extends Fragment implements OnClickListener {
    TextView title;
    TextView food;
    static TextView tx_sorry;
    ScrollView scroll_c;
    static String city_select="";
    static String miles="6";
    String name_select="";
    static String respuestaWS=null;
    final static String TAG_NAME= "name";
    ArrayList<HashMap<String, String>> OpcionesArray = new ArrayList<HashMap<String, String>>();
    static ArrayList<HashMap<String, String>> HomeArray = new ArrayList<HashMap<String, String>>();
    ArrayAdapter<String> arrayAdapter,arrayAdapterMiles;
    AlertDialog.Builder builderSingle,builderSingleMiles;
    TextView tx_selector,tx_miles;
    BDInstown logeoBD;
    static GridView gridview_home;
    static String q="";
    private RecyclerView rvp;
    static ExploreAdapter explore_adapter;
    final static String TAG_ID = "id";
    final static String TAG_NOMBRE = "nombre";
    final static String TAG_IMAGEN = "imagen";
    final static String TAG_DESCRIPCION = "descripcion";
    static Activity actividad;
    GPSTracker gps;
    AlertDialog alert = null;
    static String Latitude="0";
    static String Longitude="0";
    int id_usuario,ciudad_actual;
    String[] valuesCiudad = new String[4];
    static RelativeLayout content_load;
    static String Next="";
    private static boolean loading = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.home_fragment, container, false);

        if( Build.VERSION.SDK_INT >= 9){
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        logeoBD = new BDInstown (getActivity());
        gps = new GPSTracker(getActivity());
        title  = (TextView) view.findViewById(R.id.title);
        tx_sorry= (TextView) view.findViewById(R.id.tx_sorry);
        gridview_home=(GridView)view.findViewById(R.id.gridview_home);
        content_load=(RelativeLayout)view.findViewById(R.id.content_load);

        try {
            logeoBD.abrir();
            id_usuario=logeoBD.obtener_datos();
            ciudad_actual=logeoBD.cityExists();
            System.out.println(ciudad_actual);
            logeoBD.cerrar();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        actividad=getActivity();
        tx_selector=(TextView) view.findViewById(R.id.tx_selector);
        tx_selector.setOnClickListener(this);
        tx_miles=(TextView) view.findViewById(R.id.tx_miles);
        tx_miles.setOnClickListener(this);


        arrayAdapter=new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1);
        arrayAdapterMiles=new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1);

        scroll_c= ((ScrollView)view.findViewById(R.id.scroll_c));

        if(arrayAdapter.getCount()==0){
            new LlenarOpciones(getActivity()).execute();
        }


        return view;
    }

    @Override
    public void onResume() {

        super.onResume();
        TimeApp.reposo(getActivity());

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tx_miles:
                builderSingleMiles.show();
            break;
            case R.id.tx_selector:
                builderSingle.show();
            break;

        }
    }



    public class LlenarOpciones extends AppAsynchTask<Void, String, String> {
        Activity actividad;
        String GetCitySelect="";


        public LlenarOpciones(Activity activity) {
            super(activity);
            // TODO Auto-generated constructor stub
            actividad=activity;
        }

        @Override
        protected String customDoInBackground(Void... params)
                throws NetworkException, ServerException, ParsingException,
                TimeOutException, IOException, JSONException {

            HttpClient httpclient = new DefaultHttpClient();
            HttpGet httppost = new HttpGet(Constants.post_cities);

            try {
                logeoBD.abrir();
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);


                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);

                HttpEntity entity = response.getEntity();
                InputStream instream = entity.getContent();
                String result = Constants.convertStreamToString(instream);
                System.out.println("result "+result);

                JSONArray resultado= null;
                JSONObject myObject = new JSONObject(result);

                HashMap<String, String> mapLo = new HashMap<String, String>();
                mapLo.put(TAG_ID, "0");
                mapLo.put(TAG_NAME,"Your current location");
                mapLo.put(TAG_NOMBRE, "Your current location");
                OpcionesArray.add(mapLo);
                arrayAdapter.add("Your current location");
                resultado = myObject.getJSONArray("results");

                GetCitySelect=logeoBD.GetCitySelect();
                logeoBD.borrar_city();

                if(ciudad_actual==0){
                    logeoBD.crear_city("0","Your current location","1");
                }else{
                    logeoBD.crear_city("0","Your current location","0");
                }

                System.out.println(GetCitySelect+" GetCitySelect");

                for(int i = 0; i < resultado.length(); i++){
                    JSONObject c = resultado.getJSONObject(i);
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put(TAG_ID, c.getString("id"));
                    map.put(TAG_NAME,c.getString("name")+", "+c.getString("region"));
                    map.put(TAG_NOMBRE, c.getString("name"));
                    OpcionesArray.add(map);
                    //aqui
                    arrayAdapter.add(c.getString("name") + ", " + c.getString("region"));
                    if(GetCitySelect.toString().equals(c.getString("id").toString())){
                        logeoBD.crear_city(c.getString("id"),c.getString("name")+", "+c.getString("region"),"1");
                    }else{
                        logeoBD.crear_city(c.getString("id"),c.getString("name")+", "+c.getString("region"),"0");
                    }

                }
                respuestaWS="si";

                logeoBD.cerrar();

            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
            } catch (IOException e) {
                // TODO Auto-generated catch block
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }


            return respuestaWS;

        }

        @Override
        protected void customOnPostExecute(String result){
            respuestaWS=null;
            HashMap<String, String> songD = new HashMap<String, String>();
            songD = OpcionesArray.get(1);

            valuesCiudad=Constants.CiudadActual(getActivity());

            Latitude=valuesCiudad[0];
            Longitude=valuesCiudad[1];
            city_select=valuesCiudad[2];
            name_select=valuesCiudad[3];
            tx_selector.setText(name_select);
            new HomeData().execute();

            //inicio
            builderSingle= new AlertDialog.Builder(getActivity());
            builderSingle.setTitle("City List");
            builderSingle.setNegativeButton("Cancelar",
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            builderSingle.setAdapter(arrayAdapter,
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {


                            if(which==0){

                                if (gps.canGetLocation()) {
                                    Latitude=gps.getLatitude()+"";
                                    Longitude=gps.getLongitude()+"";
                                    city_select="0";
                                }
                                HashMap<String, String> song = new HashMap<String, String>();
                                song = OpcionesArray.get(which);
                                tx_selector.setText(song.get(TAG_NAME));
                                Constants.ActualizarCiudad(getActivity(),city_select);


                            }else{
                                Latitude="0";
                                Longitude="0";
                                HashMap<String, String> song = new HashMap<String, String>();
                                song = OpcionesArray.get(which);
                                tx_selector.setText(song.get(TAG_NAME));
                                city_select=song.get(TAG_ID);
                                System.out.println(city_select);
                                Constants.ActualizarCiudad(getActivity(), city_select);
                            }


                            new HomeData().execute();
                        }
                    });

            //miles
            arrayAdapterMiles.add("1 Mile");
            arrayAdapterMiles.add("2 Miles");
            arrayAdapterMiles.add("5 Miles");
            arrayAdapterMiles.add("10 Miles");
            arrayAdapterMiles.add("15 Miles");
            arrayAdapterMiles.add("20 Miles");
            arrayAdapterMiles.add("30 Miles");



            builderSingleMiles= new AlertDialog.Builder(getActivity());
            builderSingleMiles.setTitle("Miles List");
            builderSingleMiles.setNegativeButton("Cancelar",
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            builderSingleMiles.setAdapter(arrayAdapterMiles,
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {



                            tx_miles.setText(arrayAdapterMiles.getItem(which));
                            miles=which+"";
                           // String[] separated = miles.split(" ");
                            //miles=separated[0];


                            new HomeData().execute();

                        }
                    });
        }


    }



    public static class HomeData extends AppAsynchTask<Void, String, String> {

        public HomeData() {
            super(actividad);
            // TODO Auto-generated constructor stub
        }

        @Override
        protected String customDoInBackground(Void... params)
                throws NetworkException, ServerException, ParsingException,
                TimeOutException, IOException, JSONException {

            HttpClient httpclient = new DefaultHttpClient();
            HttpGet httppost = new HttpGet(String.format(Constants.home,"false","0","0",miles,city_select,q,Latitude,Longitude,"20"));
            //System.out.println(String.format(Constants.home,"false","0","0",miles,city_select,q,Latitude,Longitude));
            q="";
            HomeArray.clear();
            try {

                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);

                HttpEntity entity = response.getEntity();
                InputStream instream = entity.getContent();
                String result = Constants.convertStreamToString(instream);
                System.out.println("result "+result);

                JSONArray resultado= null;
                JSONObject myObject = new JSONObject(result);
                Next=myObject.getString("next");
                // email_valido=myObject.getString("email");
                // String results=myObject.getString("results");
                //JSONObject resultsJ = myObject.getJSONObject(results);
                //System.out.println(resultsJ.length()+"");

                resultado = myObject.getJSONArray("results");

                for(int i = 0; i < resultado.length(); i++){
                    JSONObject c = resultado.getJSONObject(i);

                    String merchant=c.getString("merchant");
                    JSONObject myObjectMerchant  = new JSONObject(merchant);
                    String business_name=myObjectMerchant.getString("business_name");


                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put(TAG_ID, c.getString("id"));
                    map.put(TAG_NOMBRE,business_name);
                    map.put(TAG_DESCRIPCION,c.getString("title"));
                    map.put(TAG_IMAGEN,c.getString("image"));
                    HomeArray.add(map);

                }

                respuestaWS="si";


            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
            } catch (IOException e) {
                // TODO Auto-generated catch block
            } catch (JSONException e) {
                e.printStackTrace();
            }


            return respuestaWS;

        }

        @Override
        protected void customOnPostExecute(String result){
            respuestaWS=null;
            if(HomeArray.size()==0){
                tx_sorry.setVisibility(View.VISIBLE);
            }else{
                tx_sorry.setVisibility(View.GONE);
            }

            explore_adapter=new ExploreAdapter(actividad, HomeArray);
            gridview_home.setAdapter(explore_adapter);
            gridview_home.setOnScrollListener(new GVOnScrollListener());
            gridview_home.setOnItemClickListener(new AdapterView.OnItemClickListener(){
                public void onItemClick(AdapterView<?> parent, View v, int position, long ids)
                {
                    HashMap<String, String> song = new HashMap<String, String>();
                    song = HomeArray.get(position);

                    Intent ventana_post= new Intent(actividad, InfoFragment.class);
                    new Bundle();
                    ventana_post.putExtra("post",song.get(TAG_ID));
                    actividad.startActivity(ventana_post);
                }

            });


        }


    }

    public static void HomeDataReload(){
        AsyncHttpClient myClient = new AsyncHttpClient();
        myClient.get(Next, null, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                String txtResponse = String.valueOf(response);


                try {
                    JSONArray resultado = null;
                    JSONObject myObject = new JSONObject(txtResponse);
                    Next=myObject.getString("next");
                    // next=myObject.getString("next");

                    resultado = myObject.getJSONArray("results");

                    for(int i = 0; i < resultado.length(); i++){
                        JSONObject c = resultado.getJSONObject(i);

                        String merchant=c.getString("merchant");
                        JSONObject myObjectMerchant  = new JSONObject(merchant);
                        String business_name=myObjectMerchant.getString("business_name");


                        HashMap<String, String> map = new HashMap<String, String>();
                        map.put(TAG_ID, c.getString("id"));
                        map.put(TAG_NOMBRE,business_name);
                        map.put(TAG_DESCRIPCION,c.getString("title"));
                        map.put(TAG_IMAGEN,c.getString("image"));
                        HomeArray.add(map);

                    }

                    loading = false;
                    content_load.setVisibility(View.GONE);
                    explore_adapter.notifyDataSetChanged();


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }


            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                System.out.println(statusCode);
                Log.d("THROW", throwable.toString());
            }

        });
    }


    private void AlertNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Es necesario activar su GPS, ¿Desea activarlo?")
                .setCancelable(false)
                .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        alert = builder.create();
        if(!alert.isShowing()){
            alert.show();
        }

    }

    public static final class GVOnScrollListener implements AbsListView.OnScrollListener {
        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {

        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem,
                             int visibleItemCount, int totalItemCount) {

            Log.v("first visible count", firstVisibleItem + "");
            Log.v("visible count",visibleItemCount+"");
            Log.v("total item count", totalItemCount + "");

            if (!loading && (totalItemCount - visibleItemCount)<=(firstVisibleItem)) {

                if(!Next.toString().equals("null")){
                    loading = true;
                    content_load.setVisibility(View.VISIBLE);
                    HomeDataReload();
                }

            }



        }
    }


}