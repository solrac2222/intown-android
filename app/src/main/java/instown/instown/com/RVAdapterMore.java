package instown.instown.com;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import instown.instown.com.exception.NetworkException;
import instown.instown.com.exception.ParsingException;
import instown.instown.com.exception.ServerException;
import instown.instown.com.exception.TimeOutException;
import instown.instown.com.services.AppAsynchTask;

public class RVAdapterMore extends RecyclerView.Adapter<RVAdapterMore.PersonViewHolder> {
    Activity tabOvasN;

    public class PersonViewHolder extends RecyclerView.ViewHolder {

        CardView cv;
        TextView tx_i,tx_fecha,tx_d;
        ImageView caja;


        PersonViewHolder(View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.cv);

            tx_i = (TextView)itemView.findViewById(R.id.tx_i);
            tx_d = (TextView)itemView.findViewById(R.id.tx_d);
            tx_fecha= (TextView)itemView.findViewById(R.id.tx_fecha);
            caja = (ImageView)itemView.findViewById(R.id.caja);


            tx_i.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    // TODO Auto-generated method stub
                    int posicion=getPosition();
                    MoreFragment.PopEliminar(Integer.parseInt(persons.get(getPosition()).id),posicion);
                    return false;
                }
            });


            tx_i.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    int posicion = getPosition();

                    Intent VentanaDetalle = new Intent(v.getContext(), InfoFragment.class);
                    VentanaDetalle.putExtra("post", persons.get(posicion).id);
                    v.getContext().startActivity(VentanaDetalle);

                }
            });

            tx_d.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    int posicion=getPosition();

                    Intent VentanaDetalle = new Intent(v.getContext(),InfoFragment.class);
                    VentanaDetalle.putExtra("post",persons.get(posicion).id);
                    v.getContext().startActivity(VentanaDetalle);

                }
            });

            tx_d.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    // TODO Auto-generated method stub
                    int posicion = getPosition();
                    MoreFragment.PopEliminar(Integer.parseInt(persons.get(getPosition()).id),posicion);
                    return false;
                }
            });

            tx_fecha.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    int posicion = getPosition();

                    Intent VentanaDetalle = new Intent(v.getContext(), InfoFragment.class);
                    VentanaDetalle.putExtra("post", persons.get(posicion).id);
                    v.getContext().startActivity(VentanaDetalle);

                }
            });

            tx_fecha.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    // TODO Auto-generated method stub
                    int posicion = getPosition();
                    MoreFragment.PopEliminar(Integer.parseInt(persons.get(getPosition()).id),posicion);
                    return false;
                }
            });



            caja.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    int posicion = getPosition();

                    Intent VentanaDetalle = new Intent(v.getContext(), InfoFragment.class);
                    VentanaDetalle.putExtra("post", persons.get(posicion).id);
                    v.getContext().startActivity(VentanaDetalle);

                }
            });

            caja.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    // TODO Auto-generated method stub
                    int posicion = getPosition();
                    MoreFragment.PopEliminar(Integer.parseInt(persons.get(getPosition()).id),posicion);
                    return false;
                }
            });


        }



    }

    static List<CMore> persons;

    RVAdapterMore(Activity tabOvas, List<CMore> persons){
        tabOvasN=tabOvas;
        this.persons = persons;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }



    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_more, viewGroup, false);
        PersonViewHolder pvh = new PersonViewHolder(v);
        return pvh;
    }


    @Override
    public void onBindViewHolder(PersonViewHolder personViewHolder, int i) {
        personViewHolder.tx_i.setText(persons.get(i).descri_small);
        personViewHolder.tx_d.setText(persons.get(i).name);
        personViewHolder.tx_fecha.setText(persons.get(i).expire);

        Picasso.with(tabOvasN.getApplication())
                .load(persons.get(i).photoId)
                .skipMemoryCache()
                .into(personViewHolder.caja);


    }


    @Override
    public int getItemCount() {
        return persons.size();
    }


}
