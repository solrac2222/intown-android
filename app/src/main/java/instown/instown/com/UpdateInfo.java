package instown.instown.com;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


public class UpdateInfo extends ActionBarActivity implements View.OnClickListener{
    EditText ed_name,ed_email,ed_first_name,ed_last_name,ed_city,ed_state;
    ImageView man,woman;
    Button bt_save;
    Toolbar toolbar;
    BDInstown logeoBD;
    String emailW,first_name,username,email,last_name,customer,notifications_enabled,state,email_frequency,gender,city;
    ProgressDialog dialog;
    RequestParams params = new RequestParams();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.update_info);

        if( Build.VERSION.SDK_INT >= 9){
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        toolbar = (Toolbar)findViewById(R.id.my_awesome_toolbar);


        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        logeoBD = new BDInstown (UpdateInfo.this);
        try {
            logeoBD.abrir();
            Cursor Cusuarios=logeoBD.getUser();
            for(Cusuarios.moveToFirst();!Cusuarios.isAfterLast();Cusuarios.moveToNext()){
                emailW=Cusuarios.getString(Cusuarios.getColumnIndex(BDInstown.EMAIL));
                first_name=Cusuarios.getString(Cusuarios.getColumnIndex(BDInstown.FIRSTNAME));
                username=Cusuarios.getString(Cusuarios.getColumnIndex(BDInstown.USERNAME));
                last_name=Cusuarios.getString(Cusuarios.getColumnIndex(BDInstown.LASTNAME));
                notifications_enabled=Cusuarios.getString(Cusuarios.getColumnIndex(BDInstown.NOTIFICATIONS));
                state=Cusuarios.getString(Cusuarios.getColumnIndex(BDInstown.STATE));
                email_frequency=Cusuarios.getString(Cusuarios.getColumnIndex(BDInstown.EMAIL_FREQUENCY));
                gender=Cusuarios.getString(Cusuarios.getColumnIndex(BDInstown.GENDER));
                city=Cusuarios.getString(Cusuarios.getColumnIndex(BDInstown.CITY));
            }
            logeoBD.cerrar();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        init();
        events();


    }

    @Override
    public void onResume() {

        super.onResume();
        TimeApp.reposo(UpdateInfo.this);

    }

    private void init() {
        ed_name=(EditText)findViewById(R.id.ed_name);
        ed_email=(EditText)findViewById(R.id.ed_email);
        ed_first_name=(EditText)findViewById(R.id.ed_first_name);
        ed_last_name=(EditText)findViewById(R.id.ed_last_name);
        ed_city=(EditText)findViewById(R.id.ed_city);
        ed_state=(EditText)findViewById(R.id.ed_state);

        man=(ImageView)findViewById(R.id.man);
        woman=(ImageView)findViewById(R.id.woman);

        bt_save=(Button)findViewById(R.id.bt_save);

    }

    private void events() {


        if (!username.toString().equals("null")) {
            ed_name.setText(username);
        }

        if (!emailW.toString().equals("null")) {
            ed_email.setText(emailW);
        }

        if (!first_name.toString().equals("null")) {
            ed_first_name.setText(first_name);
        }

        if (!last_name.toString().equals("null")) {
            ed_last_name.setText(last_name);
        }




        if (!city.toString().equals("null")) {
            ed_city.setText(city);
        }

        if (!state.toString().equals("null")) {
           ed_state.setText(state);
        }


        if(gender.toString().equals("0")){
            man.setImageResource(R.drawable.maleselected);
        }else{
            woman.setImageResource(R.drawable.felameselected);
        }


        woman.setOnClickListener(this);
        man.setOnClickListener(this);
        bt_save.setOnClickListener(this);
    }



    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.man:
                man.setImageResource(R.drawable.maleselected);
                woman.setImageResource(R.drawable.femaleunselected);
                gender="0";
                break;
            case R.id.woman:
                woman.setImageResource(R.drawable.felameselected);
                man.setImageResource(R.drawable.maleunselected);
                gender="1";
                break;
            case R.id.bt_save:
                UpdateInfoUser();
                break;
        }
    }


    public void  UpdateInfoUser(){
        dialog = new ProgressDialog(UpdateInfo.this);
        dialog.setMessage("Please Wait...");
        dialog.show();
        params.put("first_name", ed_first_name.getText().toString());
        params.put("last_name", ed_last_name.getText().toString());

        if (!ed_city.getText().toString().equals("")) {
            params.put("city", ed_city.getText().toString());
        }

        if (!ed_state.getText().toString().equals("")) {
            params.put("state", ed_state.getText().toString());
        }
        params.put("gender", gender);

        AsyncHttpClient myClient = new AsyncHttpClient();
        PersistentCookieStore cookieStore = new PersistentCookieStore(UpdateInfo.this);
        myClient.setCookieStore(cookieStore);

        myClient.put(Constants.user, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                String txtResponse = String.valueOf(response);
                Log.e("", txtResponse);


                Toast toast1 =Toast.makeText(getApplicationContext(),"Datos actualizados", Toast.LENGTH_SHORT);
                toast1.setGravity(Gravity.CENTER, 0, 0);
                toast1.show();
                try {
                    logeoBD.abrir();
                    logeoBD.update_info(ed_first_name.getText().toString(),ed_last_name.getText().toString(),ed_city.getText().toString(),gender,ed_state.getText().toString());
                    logeoBD.cerrar();

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                dialog.hide();

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);


                dialog.hide();


            }
        });
    }

}