package instown.instown.com;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.Time;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;


public class MoreFragment extends Fragment implements View.OnClickListener {
    BDInstown logeoBD;
    int id_usuario;
    static ProgressDialog dialog;
    final static String TAG_NAME= "name";
    final static String TAG_ID = "id";
    final static String TAG_NOMBRE = "nombre";
    final static String TAG_IMAGEN = "imagen";
    final static String TAG_DESCRIPCION = "descripcion";
    static ArrayList<HashMap<String, String>> HomeArray = new ArrayList<HashMap<String, String>>();
    static String city_select="";
    static String miles="6";
    static String categorie="0";
    static String type="0";
    String name_select="";
    String respuestaWS=null;
    TextView tx_selector,tx_miles,tx_filter;
    ArrayList<HashMap<String, String>> OpcionesArray = new ArrayList<HashMap<String, String>>();
    ArrayAdapter<String> arrayAdapter,arrayAdapterMiles,arrayAdapterFilter;
    AlertDialog.Builder builderSingle,builderSingleMiles,builderSinglefilter;
    static Activity actividad;
    private static List<CMore> list_more = new ArrayList<>();
    private static RecyclerView rvp;
    GPSTracker gps;
    AlertDialog alert = null;
    static String Latitude="0";
    static String Longitude="0";
    static TextView tx_sorry;
    static RVAdapterMore adapter=null;
    String[] valuesCiudad = new String[4];
    private static boolean loading = false;
    static int pastVisiblesItems;
    static int visibleItemCount;
    static int totalItemCount;
    static LinearLayoutManager llm;
    static RelativeLayout content_load;
    static String Next="";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.more_fragment, container, false);

        if( Build.VERSION.SDK_INT >= 9){
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        actividad=getActivity();
        gps = new GPSTracker(getActivity());
        logeoBD = new BDInstown (getActivity());

        init(view);
        events();

        try {
            logeoBD.abrir();
            try {
                logeoBD.abrir();
                int veces=0;

                Cursor cT =logeoBD.getCity();
                for(cT.moveToFirst();!cT.isAfterLast();cT.moveToNext()){
                    veces++;
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put(TAG_ID,  cT.getString(cT.getColumnIndex("id")));
                    map.put(TAG_NAME,cT.getString(cT.getColumnIndex("name")));
                    if(veces==1){
                        tx_selector.setText(cT.getString(cT.getColumnIndex("name")));
                        city_select=cT.getString(cT.getColumnIndex("id"));
                    }


                    OpcionesArray.add(map);
                    arrayAdapter.add(cT.getString(cT.getColumnIndex("name")));

                }

                valuesCiudad=Constants.CiudadActual(getActivity());
                Latitude=valuesCiudad[0];
                Longitude=valuesCiudad[1];
                city_select=valuesCiudad[2];
                name_select=valuesCiudad[3];
                tx_selector.setText(name_select);


                //inicio
                builderSingle= new AlertDialog.Builder(getActivity());
                builderSingle.setTitle("City List");
                builderSingle.setNegativeButton("Cancelar",
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                builderSingle.setAdapter(arrayAdapter,
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {


                                if(which==0){
                                    if (gps.canGetLocation()) {
                                        Latitude=gps.getLatitude()+"";
                                        Longitude=gps.getLongitude()+"";
                                        city_select="0";
                                    }
                                    HashMap<String, String> song = new HashMap<String, String>();
                                    song = OpcionesArray.get(which);
                                    tx_selector.setText(song.get(TAG_NAME));
                                    Constants.ActualizarCiudad(getActivity(), city_select);
                                }else{
                                    Latitude="0";
                                    Longitude="0";
                                    HashMap<String, String> song = new HashMap<String, String>();
                                    song = OpcionesArray.get(which);
                                    tx_selector.setText(song.get(TAG_NAME));
                                    city_select=song.get(TAG_ID);
                                    Constants.ActualizarCiudad(getActivity(),city_select);
                                }

                                MiTareaStuff();

                                // new HomeData(getActivity()).execute();
                            }
                        });

                //miles
                arrayAdapterMiles.add("1 Mile");
                arrayAdapterMiles.add("2 Miles");
                arrayAdapterMiles.add("5 Miles");
                arrayAdapterMiles.add("10 Miles");
                arrayAdapterMiles.add("15 Miles");
                arrayAdapterMiles.add("20 Miles");
                arrayAdapterMiles.add("30 Miles");

                //filter
                arrayAdapterFilter.add("VIEW ALL");
                arrayAdapterFilter.add("Expiring Soon - Next 72 hours");
                arrayAdapterFilter.add("Remind Me");
                arrayAdapterFilter.add("Sales/Specials/Updates");
                arrayAdapterFilter.add("Events");
                arrayAdapterFilter.add("Gift Card/Offers");
                arrayAdapterFilter.add("My Rewards");


                /*-10: "VIEW All"
                        -2: "Expiring Soon - Next 72 hours"
                        -1: "Remind Me"
                0: "Sales/Specials/Updates"
                1: "Events"
                2: "Gift Card/Offers"
                3: "My Rewards"*/





                builderSingleMiles= new AlertDialog.Builder(getActivity());
                builderSingleMiles.setTitle("Miles List");
                builderSingleMiles.setNegativeButton("Cancelar",
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                builderSingleMiles.setAdapter(arrayAdapterMiles,
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            tx_miles.setText(arrayAdapterMiles.getItem(which));
                            miles=which+"";
                            MiTareaStuff();
                        }
                });

                builderSinglefilter= new AlertDialog.Builder(getActivity());
                builderSinglefilter.setTitle("Filter");
                builderSinglefilter.setNegativeButton("Cancelar",
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                builderSinglefilter.setAdapter(arrayAdapterFilter,
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                if(which==0){
                                    type="10";
                                }else if(which==1){
                                    type="2";
                                }else if(which==2){
                                    type="1";
                                }else if(which==3){
                                    type="0";
                                }else if(which==4){
                                    type="1";
                                }else if(which==5){
                                    type="2";
                                }else if(which==6){
                                    type="3";
                                }
                                MiTareaStuff();
                            }
                        });


                logeoBD.cerrar();

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            logeoBD.cerrar();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        tx_sorry= (TextView) view.findViewById(R.id.tx_sorry);
        MiTareaStuff();

        return view;
    }




    private void init(View view) {
        arrayAdapter=new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1);
        arrayAdapterMiles=new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1);
        arrayAdapterFilter=new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1);
        content_load=(RelativeLayout)view.findViewById(R.id.content_load);


        rvp=(RecyclerView)view.findViewById(R.id.rvp);

        llm = new LinearLayoutManager(getActivity());
        rvp.setLayoutManager(llm);


        tx_selector=(TextView) view.findViewById(R.id.tx_selector);
        tx_selector.setOnClickListener(this);
        tx_miles=(TextView) view.findViewById(R.id.tx_miles);
        tx_miles.setOnClickListener(this);
        tx_filter=(TextView) view.findViewById(R.id.tx_filter);
        tx_filter.setOnClickListener(this);

    }

    private void events() {

    }


    @Override
    public void onResume() {

        super.onResume();
        TimeApp.reposo(getActivity());

    }


    private void enviar(String[] to, String[] cc,String asunto, String mensaje) {
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setData(Uri.parse("mailto:"));
        //String[] to = direccionesEmail;
        //String[] cc = copias;
        emailIntent.putExtra(Intent.EXTRA_EMAIL, to);
        emailIntent.putExtra(Intent.EXTRA_CC, cc);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, asunto);
        emailIntent.putExtra(Intent.EXTRA_TEXT, mensaje);
        emailIntent.setType("message/rfc822");
        startActivity(Intent.createChooser(emailIntent, "Email "));
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.tx_miles:
                builderSingleMiles.show();
                break;
            case R.id.tx_selector:
                builderSingle.show();
                break;
            case R.id.tx_filter:
                builderSinglefilter.show();
                break;

        }

    }

    public static void  MiTareaStuff(){
        dialog = new ProgressDialog(actividad);
        dialog.setMessage("Please Wait...");
        dialog.show();
        AsyncHttpClient myClient = new AsyncHttpClient();
        PersistentCookieStore cookieStore = new PersistentCookieStore(actividad);
        myClient.setCookieStore(cookieStore);

//        System.out.println(String.format(Constants.my_stuff,categorie,miles,city_select,type,Latitude,Longitude));

        myClient.get(String.format(Constants.my_stuff, categorie, miles, city_select,type,Latitude,Longitude,"20"), null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                String txtResponse = String.valueOf(response);
                System.out.println(txtResponse);
                list_more.clear();

                try {
                    JSONObject myObject = new JSONObject(txtResponse);
                    Next=myObject.getString("next");

                    // next=myObject.getString("next");
                    JSONArray resultado = new JSONArray(myObject.getString("results"));

                    for (int i = 0; i < resultado.length(); i++) {
                        JSONObject c = resultado.getJSONObject(i);

                        String merchant = c.getString("merchant");
                        JSONObject myObjectMerchant = new JSONObject(merchant);
                        String business_name = myObjectMerchant.getString("business_name");


                        HashMap<String, String> map = new HashMap<String, String>();
                        map.put(TAG_ID, c.getString("id"));
                        map.put(TAG_NOMBRE, business_name);
                        map.put(TAG_DESCRIPCION, c.getString("title"));
                        map.put(TAG_IMAGEN, c.getString("image"));
                        HomeArray.add(map);
                        String fecha=(c.getString("expiration_date_humanized"));
                        list_more.add(new CMore(c.getString("id"),c.getString("title"),business_name,c.getString("image"),fecha));


                    }

                    if(list_more.size()==0){
                        tx_sorry.setVisibility(View.VISIBLE);
                    }else{
                        tx_sorry.setVisibility(View.GONE);
                    }

                    adapter = new RVAdapterMore(actividad,list_more);
                    rvp.setAdapter(adapter);

                    //setting up our OnScrollListener
                    rvp.setOnScrollListener(new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                            System.out.println("1");
                                visibleItemCount = llm.getChildCount();
                                totalItemCount = llm.getItemCount();
                                pastVisiblesItems = llm.findFirstVisibleItemPosition();

                                if (!loading) {
                                    System.out.println("2");
                                    if ( (visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                                        System.out.println("3");
                                        if(!Next.toString().equals("null")){
                                            System.out.println("4");
                                            loading = true;
                                            MiTareaStuffReload();
                                            content_load.setVisibility(View.VISIBLE);
                                        }
                                    }
                                }

                        }
                    });


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                dialog.hide();


            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                dialog.hide();

            }
        });
    }


    public static void  MiTareaStuffReload(){
        AsyncHttpClient myClient = new AsyncHttpClient();
        PersistentCookieStore cookieStore = new PersistentCookieStore(actividad);
        myClient.setCookieStore(cookieStore);

        myClient.get(Next, null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                String txtResponse = String.valueOf(response);
                System.out.println(txtResponse);


                try {
                    JSONObject myObject = new JSONObject(txtResponse);
                    Next=myObject.getString("next");
                    // next=myObject.getString("next");
                    JSONArray resultado = new JSONArray(myObject.getString("results"));

                    for (int i = 0; i < resultado.length(); i++) {
                        JSONObject c = resultado.getJSONObject(i);

                        String merchant = c.getString("merchant");
                        JSONObject myObjectMerchant = new JSONObject(merchant);
                        String business_name = myObjectMerchant.getString("business_name");


                        HashMap<String, String> map = new HashMap<String, String>();
                        map.put(TAG_ID, c.getString("id"));
                        map.put(TAG_NOMBRE, business_name);
                        map.put(TAG_DESCRIPCION, c.getString("title"));
                        map.put(TAG_IMAGEN, c.getString("image"));
                        HomeArray.add(map);
                        String fecha=(c.getString("expiration_date_humanized"));
                        list_more.add(new CMore(c.getString("id"),c.getString("title"),business_name,c.getString("image"),fecha));


                    }


                    loading = false;
                    content_load.setVisibility(View.GONE);
                    adapter.notifyDataSetChanged();


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);

            }
        });
    }


    public static void PopEliminar(final int posicion,final int pos){
        new AlertDialog.Builder(actividad)
                .setTitle("InTown")
                .setMessage("Are you sure to delete it?")
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {


                        }
                    })
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            DeleteStuff(posicion,pos);

                        }
                    })
                .show();

    }

    public static void  DeleteStuff(final int id_delete, final int pos){
        dialog = new ProgressDialog(actividad);
        dialog.setMessage("Please Wait...");
        dialog.show();
        AsyncHttpClient myClient = new AsyncHttpClient();
        PersistentCookieStore cookieStore = new PersistentCookieStore(actividad);
        myClient.setCookieStore(cookieStore);

        System.out.println(String.format(Constants.remove_post,id_delete+""));

        myClient.post(String.format(Constants.remove_post, id_delete + ""), null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                String txtResponse = String.valueOf(response);
                System.out.println(pos);

                list_more.remove(pos);
                rvp.getAdapter().notifyDataSetChanged();
                // adapter.notifyDataSetChanged();

                dialog.hide();


            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                dialog.hide();

            }
        });
    }

}