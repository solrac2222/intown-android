package instown.instown.com;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.widget.MediaController;
import android.widget.VideoView;



public class Video extends Activity {

    VideoView vv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.video);

        if( Build.VERSION.SDK_INT >= 9){
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        vv = (VideoView)findViewById(R.id.videoView);

        String uri = "android.resource://" + getPackageName() + "/" + R.raw.merchantvideo;
        vv.setVideoURI(Uri.parse(uri));


        MediaController mc=  new MediaController(this);
        mc.setAnchorView(vv);
        vv.setMediaController(mc);
        vv.requestFocus();
        vv.start();


        vv.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                finish();
            }
        });



    }

    @Override
    public void onResume() {

        super.onResume();
        TimeApp.reposo(Video.this);

    }



}