package instown.instown.com;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.media.MediaPlayer.OnCompletionListener;
import android.util.Log;
import android.view.Gravity;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.FrameLayout;
import android.widget.MediaController;

import java.io.IOException;

/**
 * Reproductor de vídeo.
 */
public class Player implements OnBufferingUpdateListener, OnCompletionListener,
        MediaPlayer.OnPreparedListener, SurfaceHolder.Callback, MediaController.MediaPlayerControl {

    // all possible internal states
    private static final int STATE_ERROR = -1;
    private static final int STATE_IDLE = 0;
    private static final int STATE_PREPARING = 1;
    private static final int STATE_PREPARED = 2;
    private static final int STATE_PLAYING = 3;
    private static final int STATE_PAUSED = 4;
    private static final int STATE_PLAYBACK_COMPLETED = 5;

    private int mCurrentState = STATE_IDLE;

    private final String videoURL;
    public MediaPlayer mMediaPlayer;
    private int videoWidth;
    private int videoHeight;
    private MediaController videoControl;
    private SurfaceHolder surfaceHolder;
    private SurfaceView surfaceView;
    private Context context;
    private int mCurrentBufferPercentage;
    private int mAudioSession;

    public Player(SurfaceView surfaceView, String videoURL) {
        surfaceHolder = surfaceView.getHolder();
        this.surfaceView = surfaceView;
        context = surfaceView.getContext();
        surfaceHolder.addCallback(this);
        surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        this.videoURL = videoURL;
    }

    public void hideControl() {
        videoControl.hide();
    }

    public void showControl() {
        videoControl.show();
    }

    public void play() {
        mMediaPlayer.start();
    }

    public void playUrl(String videoUrl) {
        try {
            mCurrentBufferPercentage = 0;
            mMediaPlayer.reset();
            mMediaPlayer.setScreenOnWhilePlaying(true);
            mMediaPlayer.setDataSource(videoUrl);
            mMediaPlayer.prepareAsync();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private boolean isInPlaybackState() {
        return (mMediaPlayer != null &&
                mCurrentState != STATE_ERROR &&
                mCurrentState != STATE_IDLE &&
                mCurrentState != STATE_PREPARING);
    }

    @Override
    public void start() {
        // No hacemos nada
    }

    public void pause() {
        if (isInPlaybackState()) {
            if (mMediaPlayer.isPlaying()) {
                mMediaPlayer.pause();
                mCurrentState = STATE_PAUSED;
            }
        }
    }

    @Override
    public int getDuration() {
        if (isInPlaybackState()) {
            return mMediaPlayer.getDuration();
        }

        return -1;
    }

    @Override
    public int getCurrentPosition() {
        if (isInPlaybackState()) {
            return mMediaPlayer.getCurrentPosition();
        }
        return 0;
    }

    @Override
    public void seekTo(int msec) {
        if (isInPlaybackState()) {
            mMediaPlayer.seekTo(msec);
        }
    }

    @Override
    public boolean isPlaying() {
        return isInPlaybackState() && mMediaPlayer.isPlaying();
    }

    @Override
    public int getBufferPercentage() {
        if (mMediaPlayer != null) {
            return mCurrentBufferPercentage;
        }
        return 0;
    }

    @Override
    public boolean canPause() {
        return true;
    }

    @Override
    public boolean canSeekBackward() {
        return true;
    }

    @Override
    public boolean canSeekForward() {
        return true;
    }

    @Override
    public int getAudioSessionId() {
        if (mAudioSession == 0) {
            MediaPlayer foo = new MediaPlayer();
            mAudioSession = foo.getAudioSessionId();
            foo.release();
        }
        return mAudioSession;
    }

    public void stop() {
        if (mMediaPlayer != null) {
            mMediaPlayer.stop();
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
    }

    private void initVideoControll() {
        videoControl = new MediaController(context);
        videoControl.setAnchorView((android.view.View) surfaceView.getParent());
        videoControl.setMediaPlayer(this);
    }

    @Override
    public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
        Log.e("mMediaPlayer", "surface changed");
    }

    @Override
    public void surfaceCreated(SurfaceHolder arg0) {
        try {
            mMediaPlayer = new MediaPlayer();
            mMediaPlayer.setDisplay(surfaceHolder);
            mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mMediaPlayer.setOnBufferingUpdateListener(this);
            mMediaPlayer.setOnPreparedListener(this);
            initVideoControll();
            playUrl(videoURL);
        } catch (Exception e) {

        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder arg0) {

    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        mCurrentState = STATE_PREPARED;
        videoWidth = mMediaPlayer.getVideoWidth();
        videoHeight = mMediaPlayer.getVideoHeight();
        int height = surfaceView.getHeight();
        int width = surfaceView.getWidth();
        if (videoHeight != 0 && videoWidth != 0) {
            int surfaceView_Width = surfaceView.getWidth();
            int surfaceView_Height = surfaceView.getHeight();

            float video_Width = mp.getVideoWidth();
            float video_Height = mp.getVideoHeight();

            float ratio_width = surfaceView_Width / video_Width;
            float ratio_height = surfaceView_Height / video_Height;
            float aspectratio = video_Width / video_Height;

            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) surfaceView.getLayoutParams();

            if (ratio_width > ratio_height) {
                layoutParams.width = (int) (surfaceView_Height * aspectratio);
                layoutParams.height = surfaceView_Height;
            } else {
                layoutParams.width = surfaceView_Width;
                layoutParams.height = (int) (surfaceView_Width / aspectratio);
            }
            layoutParams.gravity = Gravity.CENTER;
            surfaceView.setLayoutParams(layoutParams);
            mp.start();
            mCurrentState = STATE_PLAYING;
        }
    }

    @Override
    public void onCompletion(MediaPlayer arg0) {
        mCurrentState = STATE_PLAYBACK_COMPLETED;
    }

    @Override
    public void onBufferingUpdate(MediaPlayer arg0, int bufferingProgress) {
        mCurrentBufferPercentage = bufferingProgress;
    }

}