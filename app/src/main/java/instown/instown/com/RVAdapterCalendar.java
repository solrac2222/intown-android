package instown.instown.com;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;
import com.squareup.picasso.Picasso;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import instown.instown.com.exception.NetworkException;
import instown.instown.com.exception.ParsingException;
import instown.instown.com.exception.ServerException;
import instown.instown.com.exception.TimeOutException;
import instown.instown.com.services.AppAsynchTask;

public class RVAdapterCalendar extends RecyclerView.Adapter<RVAdapterCalendar.PersonViewHolder> {
    Activity tabOvasN;
    ProgressDialog dialog;
    Dialog dialog_pop;
    BDInstown logeoBD;

    public class PersonViewHolder extends RecyclerView.ViewHolder {

        CardView cv;
        TextView tx_i,tx_fecha,tx_d,tx_title,tx_fecha_top;
        ImageView caja,img_check;
        RelativeLayout content_frame_list;


        PersonViewHolder(View itemView,int position) {
            super(itemView);

                cv = (CardView)itemView.findViewById(R.id.cv);

                tx_i = (TextView)itemView.findViewById(R.id.tx_i);
                tx_fecha_top = (TextView)itemView.findViewById(R.id.tx_fecha_top);
                tx_d = (TextView)itemView.findViewById(R.id.tx_d);
                tx_title= (TextView)itemView.findViewById(R.id.tx_title);
                tx_fecha= (TextView)itemView.findViewById(R.id.tx_fecha);
                caja = (ImageView)itemView.findViewById(R.id.caja);
                img_check= (ImageView)itemView.findViewById(R.id.img_check);
                content_frame_list=(RelativeLayout)itemView.findViewById(R.id.content_frame_list);


                tx_i.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        int posicion=getPosition();

                        Intent VentanaDetalle = new Intent(v.getContext(),InfoFragment.class);
                        VentanaDetalle.putExtra("post",persons.get(posicion).id);
                        v.getContext().startActivity(VentanaDetalle);
                    }
                });



                tx_fecha.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        int posicion=getPosition();

                        Intent VentanaDetalle = new Intent(v.getContext(),InfoFragment.class);
                        VentanaDetalle.putExtra("post",persons.get(posicion).id);
                        v.getContext().startActivity(VentanaDetalle);
                    }
                });


                img_check.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        logeoBD = new BDInstown (tabOvasN);
                        int posicion=getPosition();
                        try {
                            logeoBD.abrir();
                            if(logeoBD.obtener_datos()>0){
                                System.out.println(persons.get(posicion).id);
                                MiTareaRemind(tabOvasN, persons.get(posicion).id + "",img_check);
                            }else{
                                Intent menu = new Intent(tabOvasN, LoginPop.class);
                                tabOvasN.startActivity(menu);
                            }
                            logeoBD.cerrar();

                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }



                    }
                });

                caja.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        int posicion=getPosition();

                        Intent VentanaDetalle = new Intent(v.getContext(),InfoFragment.class);
                        VentanaDetalle.putExtra("post",persons.get(posicion).id);
                        v.getContext().startActivity(VentanaDetalle);

                    }
                });


            tx_d.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        int posicion=getPosition();

                        Intent VentanaDetalle = new Intent(v.getContext(),InfoFragment.class);
                        VentanaDetalle.putExtra("post",persons.get(posicion).id);
                        v.getContext().startActivity(VentanaDetalle);

                    }
                });




        }



    }

    static List<CCalendar> persons;

    RVAdapterCalendar(Activity tabOvas, List<CCalendar> persons){
        tabOvasN=tabOvas;
        this.persons = persons;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v=null;

        v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_calendar, viewGroup, false);

        PersonViewHolder pvh = new PersonViewHolder(v,i);
        return pvh;
    }


    @Override
    public void onBindViewHolder(PersonViewHolder personViewHolder, int i) {
           System.out.println("i "+i);

            if(persons.get(i).id.toString().equals("0")){
                personViewHolder.content_frame_list.setVisibility(View.GONE);
                personViewHolder.tx_fecha_top.setVisibility(View.VISIBLE);
                personViewHolder.tx_fecha_top.setText(SwitchDateTop(persons.get(i).name));
            }else{
                personViewHolder.content_frame_list.setVisibility(View.VISIBLE);
                personViewHolder.tx_fecha_top.setVisibility(View.GONE);
                if(!persons.get(i).name.toString().equals("")){
                    personViewHolder.tx_fecha.setText(persons.get(i).fecha);
                }
                personViewHolder.tx_i.setText(persons.get(i).descri_small);
                personViewHolder.tx_d.setText(persons.get(i).name);
                Picasso.with(tabOvasN.getApplication())
                        .load(persons.get(i).photoId)
                        .skipMemoryCache()
                        .into(personViewHolder.caja);


                System.out.println("aqui "+persons.get(i).has_reminder.toString());
                if(persons.get(i).has_reminder.toString().equals("false")){
                    personViewHolder.img_check.setVisibility(View.VISIBLE);
                }else{
                    personViewHolder.img_check.setVisibility(View.INVISIBLE);
                }
            }







    }


    @Override
    public int getItemCount() {
        return persons.size();
    }

    public void  MiTareaRemind(final Activity activity, String idR, final ImageView img_checkN){
        dialog = new ProgressDialog(activity);
        dialog.setMessage("Please Wait...");
        dialog.show();
        AsyncHttpClient myClient = new AsyncHttpClient();
        PersistentCookieStore cookieStore = new PersistentCookieStore(activity);
        myClient.setCookieStore(cookieStore);

        System.out.println(Constants.post+idR+"/remind/");
        myClient.post(Constants.post + idR + "/remind/", null, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                String txtResponse = String.valueOf(response);
                System.out.println(txtResponse);


                try {
                    JSONObject myObject = new JSONObject(txtResponse);

                    img_checkN.setVisibility(View.INVISIBLE);
                    pop_mensaje(myObject.getString("detail").toString());

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                dialog.hide();


            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                System.out.println(statusCode);
                System.out.println(errorResponse);
                Log.d("THROW", throwable.toString());
                dialog.dismiss();
            }
        });
    }

    public static String SwitchDateTop(String dateS) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(dateS);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        String[] datetime = dateS.toString().split("-");
        String day=datetime[2];
        System.out.println(datetime[1]);
        String mo=num_mes(datetime[1]);



        String dayL = (String) android.text.format.DateFormat.format("EEEE", convertedDate);//Thursday
        String stringMonth = (String) android.text.format.DateFormat.format("MMM", convertedDate); //Jun




        // String dayL=new SimpleDateFormat("DD").format(datetime[2]);
        ///*String mo=new SimpleDateFormat("MMM").format(datetime[1]);


        //System.out.println(new SimpleDateFormat("MMM").format(datetime[1]));
        String finalS=dayL+", "+stringMonth+"."+day;
        return finalS;
    }

    public static String num_mes(String mes) {
        String mes_select="";
        switch (mes){
            case "01":
                mes_select="January";
                break;
            case "02":
                mes_select="February";
                break;
            case "03":
                mes_select="March";
                break;
            case "04":
                mes_select="April";
                break;
            case "05":
                mes_select="May";
                break;
            case "06":
                mes_select="June";
                break;
            case "07":
                mes_select="July";
                break;
            case "08":
                mes_select="August";
                break;
            case "09":
                mes_select="September";
                break;
            case "10":
                mes_select="October";
                break;
            case "11":
                mes_select="November";
                break;
            case "12":
                mes_select="December";
                break;
        }
        return mes_select;

    }

    private void pop_mensaje(String mensaje) {
        dialog_pop = new Dialog(tabOvasN);
        dialog_pop.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_pop.setContentView(R.layout.pop_info_calendar);

        Button bt_saved=(Button) dialog_pop.findViewById(R.id.bt_saved);
        Button bt_close=(Button) dialog_pop.findViewById(R.id.bt_close);
        TextView textView3=(TextView) dialog_pop.findViewById(R.id.textView3);
        TextView textView4=(TextView) dialog_pop.findViewById(R.id.textView4);
        textView4.setText(mensaje);

        bt_saved.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_pop.hide();
                tabOvasN.finish();
                Home.selectItem(6);

            }
        });



        bt_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_pop.hide();
            }
        });

        dialog_pop.show();
    }

}
