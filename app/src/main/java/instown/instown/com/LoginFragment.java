package instown.instown.com;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphRequestAsyncTask;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import instown.instown.com.exception.NetworkException;
import instown.instown.com.exception.ParsingException;
import instown.instown.com.exception.ServerException;
import instown.instown.com.exception.TimeOutException;
import instown.instown.com.services.AppAsynchTask;


public class LoginFragment extends Activity implements View.OnClickListener {
    ImageView img_fb,img_tw;
    Button bt_log,bt_registro;
    String respuestaWS=null;
    LoginButton loginButton;
    CallbackManager callbackManager;
    AccessToken accesstoken;
    String non_field,id_usuario;
    EditText ed_usuario,ed_pass;
    final BDInstown logeobd = new BDInstown (LoginFragment.this);
    ProgressDialog dialog;
    RequestParams params = new RequestParams();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.login_fragment);

        init();
        ortogarClick();



        accesstoken= AccessToken.getCurrentAccessToken();

        loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions("email", "user_likes", "user_friends");

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                String accessToken= loginResult.getAccessToken().getToken();
                System.out.println(accessToken);
                LoginServicioFB(accessToken);
            }

            @Override
            public void onCancel() {
            }

            @Override
            public void onError(FacebookException exception) {
            }
        });


        bt_log=(Button)findViewById(R.id.bt_log);
        bt_log.setOnClickListener(this);
        bt_registro=(Button)findViewById(R.id.bt_registro);
        bt_registro.setOnClickListener(this);


    }

    @Override
    public void onResume() {

        super.onResume();
        TimeApp.reposo(LoginFragment.this);

    }

    private void ortogarClick() {
        img_fb.setOnClickListener(this);
        img_tw.setOnClickListener(this);
    }


    private void init() {
        img_fb=(ImageView) findViewById(R.id.img_fb);
        img_tw=(ImageView) findViewById(R.id.img_tw);
        ed_usuario=(EditText)findViewById(R.id.ed_usuario);
        ed_pass=(EditText)findViewById(R.id.ed_pass);
    }

    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }


    private void login() {
        String usuario=ed_usuario.getText()+"";
        int pasword=ed_pass.length();




        boolean isEmailEntered=true,isPasswordEntered=true;


        if(usuario.equals("")){
            isEmailEntered=false;
        }else if (!isEmailValid(ed_usuario.getText()+"")){
            isEmailEntered=false;
        }

        if(pasword<5){
            isPasswordEntered=false;
        }


        if (isEmailEntered && isPasswordEntered) {

            //String email,String pass,String gender,String city,String send_emails
            LoginServicio(ed_usuario.getText()+"",ed_pass.getText()+"");

        }else if (!isEmailEntered) {
            ed_usuario.setError("Email Invalido");
            ed_usuario.requestFocus();
        }else if (!isPasswordEntered) {
            ed_pass.setError("Contraseña muy corta");
            ed_pass.requestFocus();
        }
    }


    private class EnviarLogeo extends AsyncTask<Void, Integer, Boolean> {
        private ProgressDialog dialog;
        String email,pass;

        public EnviarLogeo(String emailE, String passE) {
            email=emailE;
            pass=passE;
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(Constants.login);

            try {
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
                nameValuePairs.add(new BasicNameValuePair("email", email));
                nameValuePairs.add(new BasicNameValuePair("password", pass));
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);

                HttpEntity entity = response.getEntity();
                InputStream instream = entity.getContent();
                String result = Constants.convertStreamToString(instream);
                System.out.println("result "+result);


                JSONObject myObject = new JSONObject(result);

                if (!myObject.isNull("non_field_errors")){
                    non_field=myObject.getString("non_field_errors");
                }

                if (!myObject.isNull("email")){
                    non_field=myObject.getString("email");
                }

                if (!myObject.isNull("id")){
                    id_usuario=myObject.getString("id");
                }




            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
            } catch (IOException e) {
                // TODO Auto-generated catch block
            } catch (JSONException e) {
                e.printStackTrace();
            }


            return true;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            int progreso = values[0].intValue();

        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(LoginFragment.this);
            dialog.setMessage("Enviado datos. Por favor espere...");
            dialog.show();
        }



        @Override
        protected void onPostExecute(Boolean result) {
            dialog.hide();

            if(non_field.equals("[\"Invalid username or password.\"]")){

                new AlertDialog.Builder(LoginFragment.this)
                        .setIcon(R.mipmap.ic_launcher)
                        .setTitle(R.string.app_name)
                        .setMessage("Invalid username or password.")
                        .setNegativeButton(android.R.string.ok, null)
                        .show();

            }else if(non_field.equals("[\"Enter a valid email address.\"]")){

                new AlertDialog.Builder(LoginFragment.this)
                        .setIcon(R.mipmap.ic_launcher)
                        .setTitle(R.string.app_name)
                        .setMessage("Enter a valid email address.")
                        .setNegativeButton(android.R.string.ok, null)
                        .show();

            }else{


                try {
                    logeobd.abrir();
                  //  logeobd.crear_entrada("1",id_usuario);
                    logeobd.cerrar();
                    Intent menu = new Intent(getApplicationContext(), Home.class);
                    finishAffinity();
                    startActivity(menu);
                } catch (Exception e) {

                    e.printStackTrace();
                }
            }



        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

       public class LoginFB extends AppAsynchTask<Void, String, String> {
        Activity actividad;
        String tk;


        public LoginFB(Activity activity,String tok) {
            super(activity);
            // TODO Auto-generated constructor stub
            actividad=activity;
            tk=tok;
        }

        @Override
        protected String customDoInBackground(Void... params)
                throws NetworkException, ServerException, ParsingException,
                TimeOutException, IOException, JSONException {


            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(Constants.loginFB);
            System.out.println(Constants.loginFB);
            try {
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
                nameValuePairs.add(new BasicNameValuePair("access_token", tk));
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));


                // Execute HTTP Post Request
                HttpResponse response = httpclient.execute(httppost);

                HttpEntity entity = response.getEntity();
                InputStream instream = entity.getContent();
                String result = Constants.convertStreamToString(instream);

                JSONObject myObject = new JSONObject(result);

                if (!myObject.isNull("non_field_errors")){
                    non_field=myObject.getString("non_field_errors");
                }

                if (!myObject.isNull("email")){
                    non_field=myObject.getString("email");
                }

                if (!myObject.isNull("id")){
                    id_usuario=myObject.getString("id");
                }

                respuestaWS="si";


            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
            } catch (IOException e) {
                // TODO Auto-generated catch block
            }/*catch (JSONException e) {
                e.printStackTrace();
            }*/

            return respuestaWS;

        }

        @Override
        protected void customOnPostExecute(String result){

            try {
                logeobd.abrir();
                //logeobd.crear_entrada("1",id_usuario);
                logeobd.cerrar();
                Intent menu = new Intent(getApplicationContext(), Home.class);
                finishAffinity();
                startActivity(menu);
            } catch (Exception e) {

                e.printStackTrace();
            }
        }


    }




    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.img_fb:
                loginButton.performClick();
            break;
            case R.id.bt_log:
                login();
               /* Intent home = new Intent(getApplicationContext(), Home.class);
                startActivity(home);
                finish();*/
             break;
            case R.id.bt_registro:
                Intent registro = new Intent(getApplicationContext(), SignFragment.class);
                startActivity(registro);
                break;
        }
    }


    public void  LoginServicioFB(String tkR){
        dialog = new ProgressDialog(LoginFragment.this);
        dialog.setMessage("Please Wait...");
        dialog.show();
        params.put("access_token", tkR);
        AsyncHttpClient myClient = new AsyncHttpClient();
        PersistentCookieStore cookieStore = new PersistentCookieStore(LoginFragment.this);
        myClient.setCookieStore(cookieStore);

        myClient.post(Constants.loginFB, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                String txtResponse = String.valueOf(response);
                Log.e("", txtResponse);


                try {
                    JSONObject myObject = new JSONObject(txtResponse);
                    //JSONObject myObject = response;

                    if (!myObject.isNull("non_field_errors")){
                        non_field=myObject.getString("non_field_errors");
                    }

                    if (!myObject.isNull("email")){
                        non_field=myObject.getString("email");
                    }


                    if (!myObject.isNull("id")){
                        id_usuario=myObject.getString("id");
                        try {
                            logeobd.abrir();
                            //logeobd.crear_entrada("1",id_usuario);
                            logeobd.cerrar();
                            finish();
                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                dialog.hide();

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);


                dialog.hide();
                System.out.println(id_usuario);




            }
        });
    }

    public void  LoginServicio(String email, String pass){
        dialog = new ProgressDialog(LoginFragment.this);
        dialog.setMessage("Please Wait...");
        dialog.show();

        params.put("email", email);
        params.put("password", pass);

        System.out.println(email);
        System.out.println(pass);

        AsyncHttpClient myClient = new AsyncHttpClient();
        PersistentCookieStore cookieStore = new PersistentCookieStore(LoginFragment.this);
        myClient.setCookieStore(cookieStore);

        myClient.post(Constants.login, params, new JsonHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                dialog.dismiss();

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                String txtResponse = String.valueOf(response);
                Log.e("", txtResponse);


                try {
                    JSONObject myObject = response;

                    if (!myObject.isNull("non_field_errors")){
                        non_field=myObject.getString("non_field_errors");
                    }

                    if (!myObject.isNull("email")){
                        non_field=myObject.getString("email");
                    }

                    if (!myObject.isNull("id")){
                        id_usuario=myObject.getString("id");
                    }

                    if(non_field.equals("[\"Invalid username or password.\"]")){

                        new AlertDialog.Builder(LoginFragment.this)
                                .setIcon(R.mipmap.ic_launcher)
                                .setTitle(R.string.app_name)
                                .setMessage("Invalid username or password.")
                                .setNegativeButton(android.R.string.ok, null)
                                .show();

                    }else if(non_field.equals("[\"Enter a valid email address.\"]")){

                        new AlertDialog.Builder(LoginFragment.this)
                                .setIcon(R.mipmap.ic_launcher)
                                .setTitle(R.string.app_name)
                                .setMessage("Enter a valid email address.")
                                .setNegativeButton(android.R.string.ok, null)
                                .show();

                    }else{

                        try {
                            logeobd.abrir();
                            //logeobd.crear_entrada("1",id_usuario);
                            logeobd.cerrar();
                            finish();
                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);


                dialog.hide();

            }
        });
    }

}