package instown.instown.com;

/**
 * Created by Francisco on 26/5/15.
 */

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import android.os.StrictMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;


public class ExploreAdapter extends BaseAdapter {

    private Activity activity;
    private ArrayList<HashMap<String, String>> data;
    private static LayoutInflater inflater=null;



    public ExploreAdapter(Activity a, ArrayList<HashMap<String, String>> d) {
        activity = a;
        data=d;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View v=convertView;



        if( Build.VERSION.SDK_INT >= 9){
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }


        ImageView picture;
        TextView titulo,descripcion;

        if(v == null)
        {
            v = inflater.inflate(R.layout.explore_item, parent, false);
            v.setTag(R.id.caja, v.findViewById(R.id.caja));
            v.setTag(R.id.tx_intown, v.findViewById(R.id.tx_intown));
            v.setTag(R.id.tx_free, v.findViewById(R.id.tx_free));
        }



        //if(contador<items.size()){
        picture = (ImageView)v.getTag(R.id.caja);
        titulo=(TextView)v.getTag(R.id.tx_intown);
        descripcion=(TextView)v.getTag(R.id.tx_free);

        HashMap<String, String> song = new HashMap<String, String>();
        song = data.get(position);

        //  System.out.println(i+" otro");


        final String url_imagen=song.get(ExploreFragment.TAG_IMAGEN);
        final String nombre=song.get(ExploreFragment.TAG_NOMBRE);
        final String descr=song.get(ExploreFragment.TAG_DESCRIPCION);

        // titulo_categoria.setText(nombre_categoria);

        Picasso.with(inflater.getContext())
                .load(url_imagen)
                .skipMemoryCache()
                .placeholder(R.drawable.caja)
                .error(R.drawable.caja)
                .into(picture);

        titulo.setText(nombre);
        descripcion.setText(descr);

        return v;

    }



}